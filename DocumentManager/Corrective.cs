﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DocumentManager
{
    public class Corrective
    {

        public Corrective()
        {

        }

        public void InputCorrectiveAction()
        {
            string filename = @"C:\Users\Farai\source\repos\Management System\Management System\Environment Management\4. Checking and Corrective Actions\CAR LOG.xlsx";
            string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=YES'", filename);
            string sql = "Insert into [Log_Application$] (Date_Submitted, Type, Source, Process, Name_of_Person_Submitting_CAR, Brief_Description, Assigned_To, Date_Assigned, Requested_Action_Due_Date, Action_Complete_Date, Closed)" +  
                
                "values(@submittedDate, @type, @source, @process, @nameOfSubmitter, @brief, @assigned, @date_assigned, @dueDate, @actionCompleteDate, @closed)";
                

            OleDbConnection con = new OleDbConnection(conString);
            con.Open();
            OleDbCommand cmd = new OleDbCommand(sql, con);

            cmd.ExecuteNonQuery();

            con.Close();

            MessageBox.Show("Done");
        }

        public void NumberOfCars(TextBox textBox, TextBox textBox2, TextBox textBox4, TextBox textBox5)
        {
            try
            {
                string filename = @"C:\Users\Farai\source\repos\Management System\Management System\Environment Management\4. Checking and Corrective Actions\SHE FRM 001 EMS CAR Log.xlsx";
                string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=NO'", filename);
                string sql = "SELECT * FROM [" + "Report" + "$]";

                OleDbConnection con = new OleDbConnection(conString);
                con.Open();
                OleDbCommand cmd = new OleDbCommand(sql, con);
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adaptor.Fill(dataTable);

                textBox.Text = dataTable.Rows[8]["F3"].ToString();
                textBox5.Text = dataTable.Rows[9]["F3"].ToString();
                textBox4.Text = dataTable.Rows[11]["F3"].ToString();
                textBox2.Text = dataTable.Rows[12]["F3"].ToString();
              
                con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void EditCorrectiveAction(TextBox textBox)
        {
            try
            {
                string filename = @"C:\Users\Farai\source\repos\Management System\Management System\Environment Management\4. Checking and Corrective Actions\SHE FRM 001 EMS CAR Log.xlsx";
                string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=NO'", filename);
                string sql = "UPDATE [" + "Customization Tab" + "$] SET CAR_Process = 'MEH' WHERE id = 1";
                
                OleDbConnection con = new OleDbConnection(conString);
                con.Open();
                OleDbCommand cmd = new OleDbCommand(sql, con);
                
                cmd.ExecuteNonQuery();
        
                con.Close();

                MessageBox.Show("Done");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void ShowProcesses(ListBox lstItem)
        {
            try
            { 
                string filename = @"C:\Users\Farai\source\repos\Management System\Management System\Environment Management\4. Checking and Corrective Actions\SHE FRM 001 EMS CAR Log.xlsx";
                string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=NO'", filename);
                string sql = "SELECT F2 FROM [" + "Customization Tab" + "$] WHERE F2 <> ''";

                OleDbConnection con = new OleDbConnection(conString);
                OleDbCommand cmd = new OleDbCommand(sql, con);
                con.Open();
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adaptor.Fill(dataTable);
                

                foreach (DataRow dataRow in dataTable.Rows)
                {
                    foreach (var item in dataRow.ItemArray)
                    {
                        lstItem.Items.Add((string)item);
                    }
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void DisplayExcelDocsEnv(ListBox lstItem)
        {       
            string filename = @"C:\Users\Farai\source\repos\Management System\Management System\Environment Management\4. Checking and Corrective Actions\SHE FRM 001 EMS CAR Log.xlsx";
            string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=NOw'", filename);
      
            using (OleDbConnection con = new OleDbConnection(conString))
            {
                try
                {
                    var dataTable = new DataTable();
                    con.Open();

                    //var tableschema = con.GetSchema("Tables");

                    //// To get the first sheet name you use the first row and the column named TABLE_NAME
                    //var firstsheet = tableschema.Rows[5]["TABLE_NAME"].ToString();

                    string name_query = "SELECT F2 FROM [" + "Customization Tab"  + "] WHERE F2 <> ''";
                    OleDbDataAdapter da = new OleDbDataAdapter(name_query, con);
                    da.Fill(dataTable);

                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        foreach (var item in dataRow.ItemArray)
                        {
                            lstItem.Items.Add((string)item);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
}
            }
    }
}
