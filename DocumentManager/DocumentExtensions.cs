﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.Office.Interop.Excel;
namespace DocumentManager
{
    public class DocumentExtensions
    {

        public DocumentExtensions()
        {

        }

        public List<string> LoadAllDocs()
        {
            List<string> documentList = new List<string>();

            documentList.Add("c-092-22 eng data of the organization iso 9001 2015(14817).doc");
            documentList.Add("FRM-004 Management Review Agenda for 2018(R1).doc");
            documentList.Add("FRM-005 Document Change Request Form (R1).doc");
            documentList.Add("FRM-011 Management Review Minutes 18 September 2018.doc");
            documentList.Add("MIN-001 Management Review Meeting Minutes 18 September 2018(001).doc");
            documentList.Add("PRC-023 Procedure for Design and Development (R1)(001).doc");
            documentList.Add("Sample Data of the Organization Checklist ISO 9001 2015.doc");
            documentList.Add("SCP-001 Scope of the Quality Management System (R2.00).doc");
            documentList.Add("CKL-001 Internal QMS Audit Checklist (004).docx");
            documentList.Add("CKL-002 QMS Management Review checklist(R1)(001).docx");
            documentList.Add("CKL-003 ISO Requirements for SHEQ Policy(R3.00)(001).docx");
            documentList.Add("DAT-001 QMS Data Analysis August 2018(R1).docx");
            documentList.Add("FRM-004 SHEQ Management Review Agenda for 2018(R1).docx");
            documentList.Add("FRM-007 Client Satisfaction Questionnaire (R2)(002).docx");

            documentList.Add("MAN-001 Quality Manual (R2)(002).docx");
            documentList.Add("OBJ-001 Quality Objectives (R2).docx");
            documentList.Add("PLN-001 QMS Transition Project Plan (R3).docx");
            documentList.Add("POL-001 Quality Policy Statement (R3).dox");
            documentList.Add("POL-002 Quality Policy (R4)(001).docx");
            documentList.Add("PRC-001 Procedure for Control of Documented Information (R2).docx");
            documentList.Add("PRC-003 Management Review Procedure (R2).docx");
            documentList.Add("PRC-004 Planning of Changes Procedure (R1)(001).docx");
            documentList.Add("PRC-005 Client Satisfaction Monitoring (R2).docx");
            documentList.Add("PRC-006 Internal Audit Procedure (R2).docx");
            documentList.Add("PRC-008 Procedure for Change Control of Documented Information (R1).docx");
            documentList.Add("PRC-009 Procedure for Managing 3rd party property (R1).docx");
            documentList.Add("PRC-011 Procedure for Determining Context and Interested Parties(R1).docx");
            documentList.Add("PRC-012 Control of Outsourced Processes Procedure (R3)(001).docx");
            documentList.Add("PRC-013 Complaints Management Procedure (R2).docx");
            documentList.Add("PRC-015 Procedure for Addressing Risks and Opportunities(R3).docx");
            documentList.Add("PRC-016 Procedure for External and Internal Communication (R2).docx");
            documentList.Add("PRC-020 Clause 8.1 Planning and Control(R01)(001).docx");
            documentList.Add("PRC-021 Management Responsibility in ISO 9001 2015 (R1).docx");
            documentList.Add("PRC-022 Supplier Assessment(1).docx");
            documentList.Add("SANS9001 2015 Ed5.docx");

            documentList.Add("REG-001 Document Register QMS.xls");
            documentList.Add("REG-002 Non-Conformance Register (R3).xls");
            documentList.Add("SCH-002 Internal Audit Schedule (R3).xls");

            documentList.Add("Data Analysis (R1).xlsx");
            documentList.Add("MTX-001 Form - Employee Competence Training Matrix(R5).xlsx");
            documentList.Add("REG-003 Change Control Register (R1).xlsx");
            documentList.Add("REG-007 Context of the Organisation Records (R10).xlsx");
            documentList.Add("REG-009 Equip Calibration Register(1)(2).xlsx");


            return documentList;
        }

        //public void DocumentInfo(System.Windows.Forms.ListBox lstItem, System.Windows.Forms.Label lblDoc, System.Windows.Forms.Label lblCode, System.Windows.Forms.Label lblFormat, PictureBox picBar)
        //{
        //    if(lstItem.SelectedItem != null)
        //    {
        //        if (lstItem.SelectedItem.ToString().Contains("FRM"))
        //        {
        //            lblDoc.Text = "FORM";
        //            lblCode.Text = "FRM";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;

        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("PRC") || lstItem.SelectedItem.ToString().Contains("PR"))
        //        {
        //            lblDoc.Text = "PROCESS";
        //            lblCode.Text = "PRC";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("POL") || lstItem.SelectedItem.ToString().Contains("PO"))
        //        {
        //            lblDoc.Text = "POLICY";
        //            lblCode.Text = "POL";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("CKL") || lstItem.SelectedItem.ToString().Contains("CH") && (lstItem.SelectedItem.ToString().Contains("xlsx") || lstItem.SelectedItem.ToString().Contains("xls") || lstItem.SelectedItem.ToString().Contains("docx")))
        //        {
        //            lblDoc.Text = "CHECKLIST";
        //            lblCode.Text = "CKL";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("MIN"))
        //        {
        //            lblDoc.Text = "MINUTES";
        //            lblCode.Text = "MIN";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("MAN"))
        //        {
        //            lblDoc.Text = "MANUAL";
        //            lblCode.Text = "MAN";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("OBJ"))
        //        {
        //            lblDoc.Text = "OBJECTIVES";
        //            lblCode.Text = "OBJ";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("PLN"))
        //        {
        //            lblDoc.Text = "PLAN";
        //            lblCode.Text = "PLN";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("DAT"))
        //        {
        //            lblDoc.Text = "DATA";
        //            lblCode.Text = "DAT";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("SCP"))
        //        {
        //            lblDoc.Text = "SCOPE";
        //            lblCode.Text = "SCP";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("MI"))
        //        {
        //            lblDoc.Text = "MATRIX";
        //            lblCode.Text = "MI";
        //            lblFormat.Text = "Word Document (.doc/.docx)";
        //            picBar.BackColor = Color.MidnightBlue;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("Documentation") || lstItem.SelectedItem == "")
        //        {
        //            lblDoc.Text = "[NAME]";
        //            lblCode.Text = "[CODE]";
        //            lblFormat.Text = "[DOCUMENT EXTENISON]";
        //            picBar.BackColor = Color.DarkGray;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("REG") || lstItem.SelectedItem.ToString().Contains("PO-REG"))
        //        {
        //            lblDoc.Text = "REGISTER";
        //            lblCode.Text = "REG";
        //            lblFormat.Text = "Excel Document (.xls/.xlsx)";
        //            picBar.BackColor = Color.SeaGreen;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("SCH"))
        //        {
        //            lblDoc.Text = "SCHEDULE";
        //            lblCode.Text = "SCH";
        //            lblFormat.Text = "Excel Document (.xls/.xlsx)";
        //            picBar.BackColor = Color.SeaGreen;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("MTX") || lstItem.SelectedItem.ToString().Contains("TD"))
        //        {
        //            lblDoc.Text = "MATRIX";
        //            lblCode.Text = "MTX";
        //            lblFormat.Text = "Excel Document (.xls/.xlsx)";
        //            picBar.BackColor = Color.SeaGreen;
        //        }

        //        if (lstItem.SelectedItem.ToString().Contains("PRS"))
        //        {
        //            lblDoc.Text = "AWARNESS TRAINING";
        //            lblCode.Text = "PRS";
        //            lblFormat.Text = "Powerpoint Document (.ppt/.pptx)";
        //            picBar.BackColor = Color.OrangeRed;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("PFD"))
        //        {
        //            lblDoc.Text = "PROCESS FLOW";
        //            lblCode.Text = "PFD";
        //            lblFormat.Text = "Visio Document (.vsd/.vsdx)";
        //            picBar.BackColor = Color.DarkBlue;
        //        }
        //        if (lstItem.SelectedItem.ToString().Contains("ORG"))
        //        {
        //            lblDoc.Text = "ORGANOGRAM";
        //            lblCode.Text = "ORG";
        //            lblFormat.Text = "Visio Document (.vsd/.vsdx)";
        //            picBar.BackColor = Color.DarkBlue;
        //        }

        //    }
          
        //}

        public string LocationChangerE(string filepath, System.Windows.Forms.ListBox lstItem)
        {
            string path = "";

            if (lstItem.SelectedItem.ToString().Contains("517") || lstItem.SelectedItem.ToString().Contains("518"))
            {
                path = filepath + @"\1. Management Model\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("500") || lstItem.SelectedItem.ToString().Contains("501") || 
                lstItem.SelectedItem.ToString().Contains("502") || lstItem.SelectedItem.ToString().Contains("503") ||
                 lstItem.SelectedItem.ToString().Contains("519")
                )
            {
                path = filepath + @"\2. Planning\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("506") || lstItem.SelectedItem.ToString().Contains("507") ||
                lstItem.SelectedItem.ToString().Contains("508") || lstItem.SelectedItem.ToString().Contains("509") ||
                lstItem.SelectedItem.ToString().Contains("510") || lstItem.SelectedItem.ToString().Contains("511")
               )
            {
                path = filepath + @"\3. Implementation and Operation\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("512") || lstItem.SelectedItem.ToString().Contains("514") ||
                lstItem.SelectedItem.ToString().Contains("515") || lstItem.SelectedItem.ToString().Contains("520") ||
                lstItem.SelectedItem.ToString().Contains("001"))
            {
                path = filepath + @"\4. Checking and Corrective Actions\" + lstItem.SelectedItem.ToString();
            }

            if (lstItem.SelectedItem.ToString().Contains("516"))
            {
                path = filepath + @"\5. Management Review\" + lstItem.SelectedItem.ToString();
            }

            if (lstItem.SelectedItem.ToString().Contains("462") || lstItem.SelectedItem.ToString().Contains("524") ||
                lstItem.SelectedItem.ToString().Contains("525") || lstItem.SelectedItem.ToString().Contains("526"))
            {
                path = filepath + @"\6. Documentation\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("460") || lstItem.SelectedItem.ToString().Contains("557") ||
                lstItem.SelectedItem.ToString().Contains("Storage"))
            {
                path = filepath + @"\7. SOP\" + lstItem.SelectedItem.ToString();
            }



            return path;
        }

        public string LocationChangerSafety(string filepath, System.Windows.Forms.ListView lstItem)
        {

            string path = "";
            try
            {

                if (lstItem.SelectedItems[0].SubItems[0].Text.Contains("IMS-PO-001") || lstItem.SelectedItems[0].SubItems[0].Text.Contains("IMS-REG-PO-001"))
                {
                    path = filepath + @"\4.1 General Requirements, Context of the Organisation\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PO-SHEQ"))
                {
                    path = filepath + @"\5.2 SHE Policy\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-012") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-013") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("OR-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("PR-004"))
                {
                    path = filepath + @"\5.3 Resources, Roles, Responsibilty, Accountability and Authority\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-002") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-003") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-004") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-005") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-006") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-007") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-008") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-009") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-010") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AP-011"))
                {
                    path = filepath + @"\5.3 Resources, Roles, Responsibilty, Accountability and Authority\2020 Appointment letters\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("AG-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("FO-002") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("PL") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("PR-006") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-008") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-009"))
                {
                    path = filepath + @"\5.4 Communication, Participation and Consultation\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("PR-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("FO-001") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("PR-002") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-001") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-002") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-003") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-004") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-005") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-005") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-006") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("REG-007"))
                {
                    path = filepath + @"\6.1.2 Hazard identification, Risk Assessments and Determining Controls\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PO-002"))
                {
                    path = filepath + @"\6.2 Objectives, Targets and Programmes\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-005") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-TD")
                    )
                {
                    path = filepath + @"\7.3 Competance, Training and Awareness\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-MI-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-008"))
                {
                    path = filepath + @"\7.5 Documented Information\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-027") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-028"))
                {
                    path = filepath + @"\7.5.3 Control of Records\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-035"))
                {
                    path = filepath + @"\8.1 Operational Control\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-009"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-010"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-011"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-012"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-013"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-014"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-015"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-016"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-017"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-018"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-019"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-020"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-021"))
                {
                    path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-037"))
                {
                    path = filepath + @"\8.3 Outsourcing\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-036"))
                {
                    path = filepath + @"\8.4 Procurement\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-31"))
                {
                    path = filepath + @"\8.5 Contractors\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-002") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-003") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-022") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-023") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-010") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-011") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-012") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-013") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-014") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REP-001"))
                {
                    path = filepath + @"\8.6 Emergency Preparedness and  Response\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-024") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-019") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REP-002"))
                {
                    path = filepath + @"\9.1 Performance, Measurement and Monitoring\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-025") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-015"))
                {
                    path = filepath + @"\9.1.2 Evaluation of Compliance\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-003"))
                {
                    path = filepath + @"\9.1.2 Legal and Other Requirements\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-004") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-010") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-CH-011") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-030") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-029") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-034") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-019"))
                {
                    path = filepath + @"\9.2 Internal Audit\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-007") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-021"))
                {
                    path = filepath + @"\9.3 Management Review\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-FRM-001") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-026") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-016") || lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-017") ||
                    lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-REG-018"))
                {
                    path = filepath + @"\10.1 Incident Investigation\Incident investigation\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }

                if (lstItem.SelectedItems[0].SubItems[0].Text.ToString().Contains("IMS-PR-033"))
                {
                    path = filepath + @"\10.1 Incident Investigation\NCR & Preventative Action\" + lstItem.SelectedItems[0].SubItems[0].Text.ToString();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        


            return path;
        }

        public string LocationChanger(string filepath, System.Windows.Forms.ListBox lstItem)
        {
            string path = "";

            if(lstItem.SelectedItem.ToString().Contains("IMS-PO-001") || lstItem.SelectedItem.ToString().Contains("IMS-REG-PO-001"))
            {
                    path = filepath + @"\4.1 General Requirements, Context of the Organisation\" + lstItem.SelectedItem.ToString();                        
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PO-SHEQ"))
            {
                path = filepath + @"\5.2 SHE Policy\" + lstItem.SelectedItem.ToString();
            }
            if(lstItem.SelectedItem.ToString().Contains("AP-012") || lstItem.SelectedItem.ToString().Contains("AP-013"))
            {
                path = filepath + @"\5.3 Resources, Roles, Responsibilty, Accountability and Authority\" + lstItem.SelectedItem.ToString();
            }
               
            if(lstItem.SelectedItem.ToString().Contains("AP-001") || lstItem.SelectedItem.ToString().Contains("AP-002") ||
                lstItem.SelectedItem.ToString().Contains("AP-003") || lstItem.SelectedItem.ToString().Contains("AP-004") ||
                lstItem.SelectedItem.ToString().Contains("AP-005") || lstItem.SelectedItem.ToString().Contains("AP-006") ||
                lstItem.SelectedItem.ToString().Contains("AP-007") || lstItem.SelectedItem.ToString().Contains("AP-008") ||
                lstItem.SelectedItem.ToString().Contains("AP-009") || lstItem.SelectedItem.ToString().Contains("AP-010") ||
                lstItem.SelectedItem.ToString().Contains("AP-011"))
            {
                path = filepath + @"\5.3 Resources, Roles, Responsibilty, Accountability and Authority\2020 Appointment letters\" + lstItem.SelectedItem.ToString();
            }
            
            if (lstItem.SelectedItem.ToString().Contains("AG-001") || lstItem.SelectedItem.ToString().Contains("FO-002")||
                lstItem.SelectedItem.ToString().Contains("PL") || lstItem.SelectedItem.ToString().Contains("PR-006") ||
                lstItem.SelectedItem.ToString().Contains("REG-008") || lstItem.SelectedItem.ToString().Contains("REG-009"))
            {
                path = filepath + @"\5.4 Communication, Participation and Consultation\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("PR-001") || lstItem.SelectedItem.ToString().Contains("FO-001") ||
                lstItem.SelectedItem.ToString().Contains("PR-002") || lstItem.SelectedItem.ToString().Contains("REG-001") ||
                lstItem.SelectedItem.ToString().Contains("REG-002") || lstItem.SelectedItem.ToString().Contains("REG-003") ||
                lstItem.SelectedItem.ToString().Contains("REG-004") || lstItem.SelectedItem.ToString().Contains("REG-005") ||
                lstItem.SelectedItem.ToString().Contains("REG-005") || lstItem.SelectedItem.ToString().Contains("REG-006") ||
                lstItem.SelectedItem.ToString().Contains("REG-007"))
            {
                path = filepath + @"\6.1.2 Hazard identification, Risk Assessments and Determining Controls\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PO-002"))
            {
                path = filepath + @"\6.2 Objectives, Targets and Programmes\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-005") || lstItem.SelectedItem.ToString().Contains("IMS-TD")
                ) 
            {
                path = filepath + @"\7.3 Competance, Training and Awareness\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-MI-001") || lstItem.SelectedItem.ToString().Contains("IMS-PR-008"))
            {
                path = filepath + @"\7.5 Documented Information\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-027") || lstItem.SelectedItem.ToString().Contains("IMS-PR-028"))
            {
                path = filepath + @"\7.5.3 Control of Records\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-035"))
            {
                path = filepath + @"\8.1 Operational Control\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-009"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-010"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-011"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-012"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-013"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-014"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-015"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-016"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-017"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-018"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-019"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-020"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-021"))
            {
                path = filepath + @"\8.1 Operational Control\Procedures\" + lstItem.SelectedItem.ToString();
            }

            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-037"))
            {
                path = filepath + @"\8.3 Outsourcing\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-036"))
            {
                path = filepath + @"\8.4 Procurement\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-31"))
            {
                path = filepath + @"\8.5 Contractors\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-CH-001") || lstItem.SelectedItem.ToString().Contains("IMS-CH-002") ||
                lstItem.SelectedItem.ToString().Contains("IMS-CH-003") || lstItem.SelectedItem.ToString().Contains("IMS-PR-022") ||
                lstItem.SelectedItem.ToString().Contains("IMS-PR-023") || lstItem.SelectedItem.ToString().Contains("IMS-REG-010") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REG-011") || lstItem.SelectedItem.ToString().Contains("IMS-REG-012") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REG-013") || lstItem.SelectedItem.ToString().Contains("IMS-REG-014") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REP-001"))
            {
                path = filepath + @"\8.6 Emergency Preparedness and  Response\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-024") || lstItem.SelectedItem.ToString().Contains("IMS-REG-019") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REP-002"))
            {
                path = filepath + @"\9.1 Performance, Measurement and Monitoring\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-025") || lstItem.SelectedItem.ToString().Contains("IMS-REG-015"))
            {
                path = filepath + @"\9.1.2 Evaluation of Compliance\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-003"))
            {
                path = filepath + @"\9.1.2 Legal and Other Requirements\" + lstItem.SelectedItem.ToString();
            }
            if (lstItem.SelectedItem.ToString().Contains("IMS-CH-004") || lstItem.SelectedItem.ToString().Contains("IMS-CH-010") ||
                lstItem.SelectedItem.ToString().Contains("IMS-CH-011") || lstItem.SelectedItem.ToString().Contains("IMS-PR-030") ||
                lstItem.SelectedItem.ToString().Contains("IMS-PR-029") || lstItem.SelectedItem.ToString().Contains("IMS-PR-034") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REG-019"))
            {
                path = filepath + @"\9.2 Internal Audit\" + lstItem.SelectedItem.ToString();
            }

            if(lstItem.SelectedItem.ToString().Contains("IMS-PR-007") || lstItem.SelectedItem.ToString().Contains("IMS-REG-021"))
            {
                path = filepath + @"\9.3 Management Review\" + lstItem.SelectedItem.ToString();
            }

            if(lstItem.SelectedItem.ToString().Contains("IMS-FRM-001") || lstItem.SelectedItem.ToString().Contains("IMS-PR-026") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REG-016") || lstItem.SelectedItem.ToString().Contains("IMS-REG-017") ||
                lstItem.SelectedItem.ToString().Contains("IMS-REG-018"))
            {
                path = filepath + @"\10.1 Incident Investigation\Incident investigation\" + lstItem.SelectedItem.ToString();
            }

            if (lstItem.SelectedItem.ToString().Contains("IMS-PR-033"))
            {
                path = filepath + @"\10.1 Incident Investigation\NCR & Preventative Action\" + lstItem.SelectedItem.ToString();
            }

            return path;
        }

        public void SwitchView(System.Windows.Forms.ListBox lstItem, RichTextBox richText, DataGridView dataGrid)
        {
            if(lstItem.SelectedItem.ToString().Contains("xls") || lstItem.SelectedItem.ToString().Contains("xlsx"))
            {
                richText.Visible = false;
                dataGrid.Visible = true;
            }
            else
            {
                richText.Visible = true;
                dataGrid.Visible = false;
            }
        }

        public void GetAllWorkSheetName(string filepath, ComboBox combo)
        {
            Microsoft.Office.Interop.Excel.Application excelAPP = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook workBooks = excelAPP.Workbooks.Open(filepath);
           
            
            foreach (Worksheet worksheet in workBooks.Worksheets)
            {
                combo.Items.Add(worksheet.Name);
            }

            workBooks.Close();
            excelAPP.Quit();
            
        }
    }

    
}
