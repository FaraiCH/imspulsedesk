﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aspose.Cells;
namespace DocumentManager
{
    /* Most of the features on this class are depreciated and are no longer used */
    public static class ServerPath
    {

        

        public static string PathLocation()
        {
            //Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            //Workbook workbook = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            //Worksheet sheet = workbook.Worksheets[2];
            //string path = sheet.Cells[0, 0].StringValue;
            //workbook.Settings.Password = "@Paradice1";
            //workbook.Save(@"C:\Reader\Loader.xlsx");

            //NetworkCredential theNetworkCredential = new NetworkCredential(@"SERVER-PC\Cinfratec", "Cinfratecadmin");
            //CredentialCache theNetCache = new CredentialCache();
            //theNetCache.Add(new Uri(@"\\SERVER-PC"), "Basic", theNetworkCredential);
            //string[] theFolders = Directory.GetDirectories(@"\\SERVER-PC\ISO 9001");

            

                return PathLocation(Application.StartupPath + @"\IMS Pulse\");
          
        }

        static Dictionary<string, string> _docs = new Dictionary<string, string>();
        static string PathLocation(string value)
        {
            string lookup;
            if (_docs.TryGetValue(value, out lookup))
                return lookup;

            lookup = value;

            _docs[value] = lookup;
            return lookup;
        }

    }



    public class DocumentMaster
    {

        static NetworkCredential networkCredential = new NetworkCredential("Cinfratec", "@Paradice1");
        public DocumentMaster()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";
            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));

            stream2.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream2);

        }



        public void activateList(ListView lstDepartment)
        {

          
                lstDepartment.View = View.Details;


                lstDepartment.Columns.Add("Document List", 150);
                lstDepartment.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.HeaderSize);
          
        }

        public void OfficeProduct(ImageList imageIcon)
        {

         

                try
                {

                    imageIcon.ImageSize = new Size(25, 25);

                    string[] paths = { };
                    paths = Directory.GetFiles(ServerPath.PathLocation() + @"Office Assets");

                    try
                    {
                        foreach (var path in paths)
                        {
                            imageIcon.Images.Add(Image.FromFile(path));
                        }
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }


        }


        public List<string> LoadDocuments(string primeFolder, string code, string documentExtension)
        {

            List<string> documentList = new List<string>();
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*." + documentExtension, SearchOption.AllDirectories);

            for (int i = 0; i < fileArray.Length; i++)
            {
                if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(code))
                {
                    string path = Path.GetFileName(fileArray[i]);
                    documentList.Add(path);
                }
            }
            return documentList;
        }


        public void LoadDocuments(string primeFolder, string name1, string name2,
                                       string name3, string name4, string name5, string name6,
                                       string name7, string name8, string name9, string name10, ListView lst, ImageList imageIcon)
        {
          
           
         
            try
            {

          
        
                    



                List<string> documentList = new List<string>();
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                lst.SmallImageList = imageIcon;
                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (name1 != null)
                    {

                        if (fileArray[i].Contains(name1) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".vsd"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name1) && fileArray[i].Contains(".pdf"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 5);
                        }


                    }

                    if (name2 != null)
                    {
                        if (fileArray[i].Contains(name2) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name2) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }

                    if (name3 != null)
                    {
                        if (fileArray[i].Contains(name3) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name3) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }

                    if (name4 != null)
                    {
                        if (fileArray[i].Contains(name4) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name4) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }
                    if (name5 != null)
                    {
                        if (fileArray[i].Contains(name5) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name5) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }

                    if (name6 != null)
                    {
                        if (fileArray[i].Contains(name6) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name6) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }


                    if (name7 != null)
                    {
                        if (fileArray[i].Contains(name7) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name7) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }

                    if (name8 != null)
                    {
                        if (fileArray[i].Contains(name8) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name8) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }

                    if (name9 != null)
                    {
                        if (fileArray[i].Contains(name9) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name9) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                    }


                    if (name10 != null)
                    {
                        if (fileArray[i].Contains(name10) && fileArray[i].Contains(".docx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".xlsx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".ppt"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".doc"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 1);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".xls"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 2);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".pptx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 3);
                        }
                        else if (fileArray[i].Contains(name10) && fileArray[i].Contains(".vsdx"))
                        {
                            string path = Path.GetFileName(fileArray[i]);
                            lst.Items.Add(path, 4);
                        }

                    }

                }
            
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        public List<string> LoadDocuments(string primeFolder, string name1, string name2,
                                          string name3, string name4, string name5, string name6,
                                          string name7, string name8, string name9, string name10, 
                                          string documentExtension)
        {
           
            List<string> documentList = new List<string>();
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder +@"\", "*." + documentExtension, SearchOption.AllDirectories);

            for (int i = 0; i < fileArray.Length; i++)
            {
                if(name1 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name1))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }

                if(name2 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name2))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
                
                if(name3 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name3))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
                
                if(name4 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name4))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
                if(name5 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name5))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
                
                if(name6 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name6))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }

               
                if(name7 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name7))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
              
                if(name8 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name8))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
                
                if(name9 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name9))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
               

                if (name10 != null)
                {
                    if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(name10))
                    {
                        string path = Path.GetFileName(fileArray[i]);
                        documentList.Add(path);
                    }
                }
               


            }
            return documentList;
        }

        public List<string> LoadPaths(string primeFolder, string code, string documentExtension)
        {

            List<string> documentList = new List<string>();
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*." + documentExtension, SearchOption.AllDirectories);

            for (int i = 0; i < fileArray.Length; i++)
            {
                if (fileArray[i].Contains(documentExtension) && fileArray[i].Contains(code))
                {
                    string path = Path.GetFullPath(fileArray[i]);
                    documentList.Add(path);
                }

            }
            return documentList;
        }


        public string LoadSelectedWithNAme(string primeFolder, ListView listView)
        {
            string pathandfiles = "";
            string name = "";
            try
            {

                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (fileArray[i].Contains(listView.SelectedItems[0].SubItems[0].Text.ToString()))
                    {
                        pathandfiles = fileArray[i];

                        name = Path.GetFileNameWithoutExtension(pathandfiles);
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            return name;

        }

        public string LoadSelectedExtensions(string primeFolder, ListView listView)
        {
            string pathandfiles = "";
            string name = "";
            try
            {

                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (fileArray[i].Contains(listView.SelectedItems[0].SubItems[0].Text.ToString()))
                    {
                        pathandfiles = fileArray[i];
                        name = Path.GetExtension(pathandfiles);
                    }
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            return name;

        }

        public string LoadSelected(string primeFolder, ListView listView)
        {
            string pathandfiles = "";
            try
            {
                
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (fileArray[i].Contains(listView.SelectedItems[0].SubItems[0].Text.ToString()))
                    {
                        pathandfiles = fileArray[i];                 
                    }
                }

                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            return pathandfiles;

        }

        public List<string> RunSelectedFile(string primeFolder, ListView listView)
        {
            List<string> documentList = new List<string>();
          

             
                try
                {
                    string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                    for (int i = 0; i < fileArray.Length; i++)
                    {
                    if (fileArray[i].Contains(listView.SelectedItems[0].SubItems[0].Text.ToString()))
                    {
                        string pathandfiles = fileArray[i];

                        //using (new NetworkConnection(@"\\SERVER-PC\ISO 9001", networkCredential))
                        //{
                            Process.Start(pathandfiles);
                        //}
                    }
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            
            return documentList;
        }

        public string SelectedPrintDoc(ListView lstView, string primeFolder)
        {
            string chosen = "";
           

            if (lstView.SelectedItems[0].SubItems[0].Text.ToString() != null )
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {

                    if (fileArray[i].Contains(lstView.SelectedItems[0].SubItems[0].Text.ToString()))
                    {
                        string pathandfiles = fileArray[i];
                        chosen = pathandfiles;
                    }
                }
               
            }
     
            return chosen;
        }



    }
}
