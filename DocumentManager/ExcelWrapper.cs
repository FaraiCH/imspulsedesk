﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExcelLibrary = Microsoft.Office.Interop.Excel;

namespace DocumentManager
{
    public class ExcelWrapper
    {
        public ExcelWrapper()
        {

        }

        /// <summary>
        /// Load All Documents
        /// </summary>
        /// <returns></returns>
        public List<string> LoadExcelDocs()
        {
            
            List<string> documentList = new List<string>();

                documentList.Add("REG-001 Document Register QMS.xls");
                documentList.Add("REG-002 Non-Conformance Register (R3).xls");
                documentList.Add("SCH-002 Internal Audit Schedule (R3).xls");
          
                documentList.Add("Data Analysis (R1).xlsx");
                documentList.Add("MTX-001 Form - Employee Competence Training Matrix(R5).xlsx");
                documentList.Add("REG-003 Change Control Register (R1).xlsx");
                documentList.Add("REG-007 Context of the Organisation Records (R10).xlsx");
                documentList.Add("REG-009 Equip Calibration Register(1)(2).xlsx");
            documentList.Add("RISK REGISTER.xlsx");

            return documentList;
        }


        public void DisplayExcelDocsEnv(ListBox lstItem, DataGridView dataCell, string sheetName)
        {
            DocumentExtensions extensions = new DocumentExtensions();
            string filename = extensions.LocationChangerE(@"C:\Environment Management", lstItem);
            string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=YES'", filename);
            string sql = "Select * from [" + sheetName + "$]";

            OleDbConnection con = new OleDbConnection(conString);
            OleDbCommand cmd = new OleDbCommand(sql, con);
            con.Open();
            OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
            DataTable data = new DataTable();
            adaptor.Fill(data);
            dataCell.DataSource = data;
            dataCell.GridColor = Color.SeaGreen;

            con.Close();
        }

        public void DisplayExcelDocsNew(ListBox lstItem, DataGridView dataCell, string sheetName)
        {
            string conString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + @"C:\Quality Management\QMS\" + lstItem.SelectedItem.ToString() + ";Extended Properties = 'Excel 8.0;HDR=YES'";
            string sql = "Select * from [" + sheetName + "$]";

            OleDbConnection con = new OleDbConnection(conString);
            OleDbCommand cmd = new OleDbCommand(sql, con);
            con.Open();
            OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
            DataTable data = new DataTable();
            adaptor.Fill(data);
            dataCell.DataSource = data;
            dataCell.GridColor = Color.SeaGreen;

            con.Close();
        }

        public void DisplayExcelDocs(ListBox lstItem, DataGridView dataCell, string sheetName)
        {      
                string conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + @"C:\Quality Management\QMS\" + lstItem.SelectedItem.ToString() + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1;'";
                string sql = "Select * from [" + sheetName + "$]";

                OleDbConnection con = new OleDbConnection(conString);
                OleDbCommand cmd = new OleDbCommand(sql, con);
                con.Open();
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable data = new DataTable();
                adaptor.Fill(data);
                dataCell.DataSource = data;
                dataCell.GridColor = Color.SeaGreen; 
            
                con.Close();   
        }

        public void DisplayExcelDocsEnvironment(ListBox lstItem, DataGridView dataCell, string sheetName)
        {
            try
            {
                DocumentExtensions extensions = new DocumentExtensions();

                string filename = extensions.LocationChangerE(@"C:\Environment Management", lstItem);
                string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=YES'", filename);
                string sql = "Select * from [" + sheetName + "$]";

                OleDbConnection con = new OleDbConnection(conString);
                OleDbCommand cmd = new OleDbCommand(sql, con);
                con.Open();
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable data = new DataTable();
                adaptor.Fill(data);
                dataCell.DataSource = data;
                dataCell.GridColor = Color.SeaGreen;
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void DisplayExcelDocsSafety(ListBox lstItem, DataGridView dataCell, string sheetName)
        {
            try
            {
                DocumentExtensions extensions = new DocumentExtensions();

                string filename = extensions.LocationChanger(@"C:\Safety Management\IMS Global", lstItem);
                string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=YES'", filename);
                string sql = "Select * from [" + sheetName + "$]";

                OleDbConnection con = new OleDbConnection(conString);
                OleDbCommand cmd = new OleDbCommand(sql, con);
                con.Open();
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable data = new DataTable();
                adaptor.Fill(data);
                dataCell.DataSource = data;
                dataCell.GridColor = Color.SeaGreen;
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        public List<string> LoadEnvironmentDocs(string heading)
        {
            List<string> documentList = new List<string>();

            if (heading == "Checking")
            {
                documentList.Add("SHE FRM 001 EMS CAR Log.xlsx");
            }
            else if (heading == "SOP")
            {
                documentList.Add("SHE 479-1 Storage and disposal of glass.xls");
                documentList.Add("SHE 479-10 Storage and disposal of waste water.xls");
                documentList.Add("SHE 479-13 Storage and disposal of grease trap content.xls");
                documentList.Add("SHE 479-15 Storage and disposal of waste cloths.xls");
                documentList.Add("SHE 479-19 Storage and disposal of oil filters.xls");
                documentList.Add("SHE 479-2 Storage and disposal of fluorescent tubes.xls");
                documentList.Add("SHE 479-20 Storage and disposal of computer electronic omponents.xls");
                documentList.Add("SHE 479-21 Storage and disposal of plastic.xls");
                documentList.Add("SHE 479-22 Storage and disposal of paint.xls");
                documentList.Add("SHE 479-23 Storage and disposal of acid.xls");
                documentList.Add("SHE 479-24 Sorage and disposal of wood.xls");
                documentList.Add("SHE 479-27 Storage and disposal of poison.xls");
                documentList.Add("SHE 479-5 Storage and disposal of paper.xls");
                documentList.Add("SHE 479-6 Storage and disposal of mineral oils.xls");
                documentList.Add("SHE 479-7 Stroage and disposal of metal.xls");
                documentList.Add("SHE 479-8 Storage and disposal of lead-acid batteries.xls");
                documentList.Add("SHE 479-9 Storage and disposal of sealed batteries .xls");
                

            }

            return documentList;
        }

        public List<string> LoadSafetyDocs(string heading, string subHeading, string subheading2)
        {
            List<string> documentList = new List<string>();

            
            if (heading == "General" && subHeading == null)
            {
                documentList.Add("IMS-REG-PO-001 Context of the Organisation Records (R10).xlsx");
            }
            else if (heading == "Communicate" && subHeading == null)
            {
                documentList.Add("IMS-PL-001-Annual Communication Plan 2020 1.xlsx");
                documentList.Add("IMS-PL-002-Annual Communication Plan 2020 2.xlsx");
                documentList.Add("IMS-REG-008-Visitors register.xlsx");
                documentList.Add("IMS-REG-009 - Staff movement register.xlsx");

            }
            else if (heading == "Hazard" && subHeading == null)
            {
                documentList.Add("IMS-FO-001-Non Routine Risk Assessment Evaluation Template.xlsx");
                documentList.Add("IMS-REG-001-Risk Appraisal-Office.xlsx");
                documentList.Add("IMS-REG-002-Risk Appraisal-Structures.xlsx");
                documentList.Add("IMS-REG-003-Risk Appraisal-Econ Dev.xlsx");
                documentList.Add("IMS-REG-004-Risk Appraisal-Geometrics.xlsx");
                documentList.Add("IMS-REG-005-Risk Appraisal-Rehab  Reseals.xlsx");
                documentList.Add("IMS-REG-006-Risk Appraisal-Water.xlsx");
                documentList.Add("IMS-REG-007-Risk Appraisal-Environ.xlsx");
            }
        
            else if (heading == "Competence" && subHeading == null)
            {
                documentList.Add("IMS-TD-001-Training Matrix 2020 1.xls");
                documentList.Add("IMS-TD-002-Training Matrix 2020 2.xls");
            }
         
            else if (heading == "Emergency" && subHeading == null)
            {
                documentList.Add("IMS-CH-003 - First aid box checklist.xlsx");
                documentList.Add("IMS-REG-010 - Next of kin list.xlsx");
                documentList.Add("IMS-REG-011 - Emergency Numbers.xlsx");
                documentList.Add("IMS-REG-012 Extinguisher register.xlsx");
                documentList.Add("IMS-REG-013 - fire Equipment Register.xlsx");
                documentList.Add("IMS-CH-003 - First aid box checklist.xlsx");
                documentList.Add("IMS-CH-003 - First aid box checklist.xlsx");
                documentList.Add("IMS-REG-014 - List for unaccounted for persons.xlsx");
                documentList.Add("IMS-REP-001-Drill report.xlsx");

            }
            else if (heading == "Performance" && subHeading == null)
            {
                documentList.Add("IMS-REG-019-Water and Electricty Monthly Consumption.xlsx");
                documentList.Add("IMS-REP-002 - Incident data.xlsx");
            }
            else if (heading == "Evaluation" && subHeading == null)
            {
                documentList.Add("IMS-REG-015 - General Administrative Regs.xlsx");
            }
           
            else if (heading == "Internal" && subHeading == null)
            {
                documentList.Add("IMS-CH-004-Office Checklist rev1.xlsx");      
            }
            else if (heading == "Incident")
            {
                if (subHeading == "Incident")
                {
                    documentList.Add("IMS-REG-016 - Log of incidents.xlsx");
                    documentList.Add("IMS-REG-017 - Incident record.xlsx");
                    documentList.Add("IMS-REG-018 - Incident data.xlsx");
                }
            }

            return documentList;
        }


    }
}
