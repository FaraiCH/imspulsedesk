﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PowerPointLibrary = Microsoft.Office.Interop.PowerPoint;

namespace DocumentManager
{
    public class PowerWrapper
    {
        public PowerWrapper()
        {

        }
        public List<string> LoadPowerDocs()
        {
            List<string> documentList = new List<string>();

            documentList.Add("PRS-001 ISO 9001 2015 Awareness (R2).pptx");

            return documentList;
        }

        public List<string> LoadPowerDocsSafety()
        {
            List<string> documentList = new List<string>();

            documentList.Add("IMS-OR-001-IMS orginisational chart rev 5.pptx");

            return documentList;
        }

    }
}
