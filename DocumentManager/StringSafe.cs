﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentManager
{
    public static class StringSafe
    {
        public static string SafeSubstring(this string orig, int length)
        {
            return orig.Substring(0, orig.Length >= length ? length : orig.Length);
        }

    }
}
