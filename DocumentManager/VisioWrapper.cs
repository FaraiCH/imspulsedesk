﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DocumentManager
{
    public class VisioWrapper
    {
        public List<string> LoadVisioDocs()
        {
            List<string> documentList = new List<string>();

            documentList.Add("PFD-001 Control of Documented Information Process (R4).vsd");
            documentList.Add("PFD-002 Management Review Process (R1).vsd");
            documentList.Add("PFD-003 Control of Non-conformance Process Flow(R1).vsd");

            documentList.Add("PFD-008_Risk & Opportunity Process _(R1).vsd");
            documentList.Add("PFD-010 Change Control Process Flow Diagram (R1).vsd");
            documentList.Add("PFD-011 Determine Requirements for Products Services (R2).vsd");

            documentList.Add("ORG-001 Organogram (R2).vsdx");
            documentList.Add("PFD-005 End-to-end Process Flow Diagram (R2).vsdx");
            documentList.Add("PFD-006 Complaints Management Process (R1).vsdx");
            documentList.Add("PFD-007 Operational Planning and Control (R1).vsdx");
            documentList.Add("PFD-007 Planning for Realization of Products or Services Process (R1).vsdx");
            documentList.Add("PFD-009 QMS and its Processes (R3).vsdx");
            documentList.Add("PROCESS+RISKS+PDCA.vsdx");

            return documentList;
        }

        public void DisplayVisio(RichTextBox richTextBox1, ListBox lstItem)
        {
     
        }
    }
}
