﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WordLibrary = Microsoft.Office.Interop.Word;
 

namespace DocumentManager
{
    public class WordWrapper
    {
        //Constructor for instatition
        public WordWrapper()
        {

        }
        
        /// Copy contents on the word document (.doc, .docx) to a richtextbox
        /// </summary>
        /// <param name="wordApp">The Actual Application for the Word Document</param>
        /// <param name="wordDocument">This is the specific document that will be focused (Reuires Application to open)</param>
        /// <param name="richTextBox">Comopent required to attach</param>
        /// <param name="item">Combox filled with documents</param>
        public void DisplayFullDocument(string filePathName, WordLibrary.Application wordApp, WordLibrary.Document wordDocument, RichTextBox richTextBox)
        {
            wordApp = new WordLibrary.Application();
            object nullobject = System.Reflection.Missing.Value;
            wordApp.DisplayAlerts = WordLibrary.WdAlertLevel.wdAlertsNone; 
            wordDocument = wordApp.Documents.Open(filePathName);
            wordDocument.ActiveWindow.Selection.WholeStory();
            wordDocument.ActiveWindow.Selection.Copy();
            richTextBox.Paste();
            wordDocument.Close(ref nullobject, ref nullobject, ref nullobject);
            wordApp.Quit(ref nullobject, ref nullobject, ref nullobject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="findText"></param>
        /// <param name="replaceWithText"></param>
        private void FindAndReplace(WordLibrary.Application doc, object findText, object replaceWithText)
        {
            //options
            object matchCase = false;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;
            //execute find and replace
            doc.Selection.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);
        }


        public void DisplayOnePageSafety(RichTextBox richTextBox, ListBox lstItem, GroupBox groupBox)
        {
            //TODO: Must make Document Libraray work for one page
            groupBox.Visible = true;
            DocumentExtensions extensions = new DocumentExtensions();

            string filename = extensions.LocationChanger(@"C:\Safety Management\IMS Global", lstItem);

            Microsoft.Office.Interop.Word.Application oWord = new Microsoft.Office.Interop.Word.Application();



            for (int i = 1; i < 2; i++)
            {

                Microsoft.Office.Interop.Word.Document oDoc1 = oWord.Documents.Open(filename);
                object matchCase = false;
                object matchWholeWord = true;
                object matchWildCards = false;
                object matchSoundsLike = false;
                object matchAllWordForms = false;
                object forward = true;
                object format = false;
                object matchKashida = false;
                object matchDiacritics = false;
                object matchAlefHamza = false;
                object matchControl = false;
                object read_only = false;
                object visible = true;
                object replace = 2;
                object wrap = 1;
                object findText = "#";
                object replaceWithText = i.ToString();
                //execute find and replace
                oDoc1.Content.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                    ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                    ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);

                Microsoft.Office.Interop.Word.Range oRange = oDoc1.Content;
                oRange.Copy();
                
                //I need to paste this range into the ith page. so if i=1 paste into first page, i=2 paste into second page
                richTextBox.Paste();

                groupBox.Visible = false;

                richTextBox.SelectAll();
               
           
                richTextBox.SelectionStart = 0;
                richTextBox.SelectionLength = 1;
                richTextBox.ScrollToCaret();

                oDoc1.Close();
                oWord.Quit();
            }
        }


        public void DisplayOnePageEnvironment(RichTextBox richTextBox, ListBox lstItem, GroupBox groupBox)
        {
            groupBox.Visible = true;
            
            //TODO: Must make Document Library work for one page
            DocumentExtensions extensions = new DocumentExtensions();

            string filename = extensions.LocationChangerE(@"C:\Environment Management", lstItem);

            Microsoft.Office.Interop.Word.Application oWord = new Microsoft.Office.Interop.Word.Application();

           

                for (int i = 1; i < 2; i++)
                {

                    Microsoft.Office.Interop.Word.Document oDoc1 = oWord.Documents.Open(filename);
                    object matchCase = false;
                    object matchWholeWord = true;
                    object matchWildCards = false;
                    object matchSoundsLike = false;
                    object matchAllWordForms = false;
                    object forward = true;
                    object format = false;
                    object matchKashida = false;
                    object matchDiacritics = false;
                    object matchAlefHamza = false;
                    object matchControl = false;
                    object read_only = false;
                    object visible = true;
                    object replace = 2;
                    object wrap = 1;
                    object findText = "#";
                    object replaceWithText = i.ToString();
                    //execute find and replace
                    oDoc1.Content.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                        ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                        ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);

                    Microsoft.Office.Interop.Word.Range oRange = oDoc1.Content;
                  
                    oRange.Copy();

                    //I need to paste this range into the fifth page. so if i=1 paste into first page, i=2 paste into second page
                    richTextBox.Paste();
                    groupBox.Visible = false;

                    richTextBox.SelectionStart = 0;
                    richTextBox.SelectionLength = 1;
                    richTextBox.ScrollToCaret();

                    oDoc1.Close();
                    oWord.Quit();
                
            }

             
               
          
      



        }





        public void DisplayOnePage(RichTextBox richTextBox, ListBox lstItem, GroupBox groupBox)
        {
            //TODO: Must make Document Libraray work for one page

            groupBox.Visible = true;

            string filename = @"C:\Quality Management\QMS\" + lstItem.SelectedItem.ToString();

            Microsoft.Office.Interop.Word.Application oWord = new Microsoft.Office.Interop.Word.Application();


           
            for (int i = 1; i < 2; i++)
            {
               
                Microsoft.Office.Interop.Word.Document oDoc1 = oWord.Documents.Open(filename);
                object matchCase = false;
                object matchWholeWord = true;
                object matchWildCards = false;
                object matchSoundsLike = false;
                object matchAllWordForms = false;
                object forward = true;
                object format = false;
                object matchKashida = false;
                object matchDiacritics = false;
                object matchAlefHamza = false;
                object matchControl = false;
                object read_only = false;
                object visible = true;
                object replace = 2;
                object wrap = 1;
                object findText = "#";
                object replaceWithText = i.ToString();
                //execute find and replace
                oDoc1.Content.Find.Execute(ref findText, ref matchCase, ref matchWholeWord,
                    ref matchWildCards, ref matchSoundsLike, ref matchAllWordForms, ref forward, ref wrap, ref format, ref replaceWithText, ref replace,
                    ref matchKashida, ref matchDiacritics, ref matchAlefHamza, ref matchControl);

                Microsoft.Office.Interop.Word.Range oRange = oDoc1.Content;
                oRange.Copy();

                
                //I need to paste this range into the ith page. so if i=1 paste into first page, i=2 paste into second page
                richTextBox.Paste();
                groupBox.Visible = false;
                richTextBox.SelectionStart = 0;
                richTextBox.SelectionLength = 1;
                richTextBox.ScrollToCaret();
                
                oDoc1.Close();
                oWord.Quit();
            }
        }


        /// <summary>
        /// All the word documentation in the Quality Management System
        /// </summary>
        /// <returns></returns>
        public List<string> LoadWOrdDocs()
        {
            
            List<string> documentList = new List<string>();


                documentList.Add("c-092-22 eng data of the organization iso 9001 2015(14817).doc");
                documentList.Add("FRM-004 Management Review Agenda for 2018(R1).doc");
                documentList.Add("FRM-005 Document Change Request Form (R1).doc");
                documentList.Add("FRM-011 Management Review Minutes 18 September 2018.doc");
                documentList.Add("MIN-001 Management Review Meeting Minutes 18 September 2018(001).doc");
                documentList.Add("PRC-023 Procedure for Design and Development (R1)(001).doc");
                documentList.Add("Sample Data of the Organization Checklist ISO 9001 2015.doc");
                documentList.Add("SCP-001 Scope of the Quality Management System (R2.00).doc");
                documentList.Add("CKL-001 Internal QMS Audit Checklist (004).docx");
                documentList.Add("CKL-002 QMS Management Review checklist(R1)(001).docx");
                documentList.Add("CKL-003 ISO Requirements for SHEQ Policy(R3.00)(001).docx");
                documentList.Add("DAT-001 QMS Data Analysis August 2018(R1).docx");
                documentList.Add("FRM-004 SHEQ Management Review Agenda for 2018(R1).docx");
                documentList.Add("FRM-007 Client Satisfaction Questionnaire (R2)(002).docx");
                
                documentList.Add("MAN-001 Quality Manual.docx");
                documentList.Add("OBJ-001 Quality Objectives (R2).docx");
                documentList.Add("PLN-001 QMS Transition Project Plan (R3).docx");
                documentList.Add("POL-001 Quality Policy Statement.docx");
                documentList.Add("POL-002 Quality Policy (R4)(001).docx");
                documentList.Add("PRC-001 Procedure for Control of Documented Information (R2).docx");
                documentList.Add("PRC-003 Management Review Procedure (R2).docx");
                documentList.Add("PRC-004 Planning of Changes Procedure (R1)(001).docx");
                documentList.Add("PRC-005 Client Satisfaction Monitoring (R2).docx");
                documentList.Add("PRC-006 Internal Audit Procedure (R2).docx");
                documentList.Add("PRC-008 Procedure for Change Control of Documented Information (R1).docx");
                documentList.Add("PRC-009 Procedure for Managing 3rd party property (R1).docx");
                documentList.Add("PRC-011 Procedure for Determining Context and Interested Parties(R1).docx");
                documentList.Add("PRC-012 Control of Outsourced Processes Procedure (R3)(001).docx");
                documentList.Add("PRC-013 Complaints Management Procedure (R2).docx");
                documentList.Add("PRC-015 Procedure for Addressing Risks and Opportunities(R3).docx");
                documentList.Add("PRC-016 Procedure for External and Internal Communication (R2).docx");
                documentList.Add("PRC-020 Clause 8.1 Planning and Control(R01)(001).docx");
                documentList.Add("PRC-021 Management Responsibility in ISO 9001 2015 (R1).docx");
                documentList.Add("PRC-022 Supplier Assessment(1).docx");
                documentList.Add("SANS9001 2015 Ed5.docx");

           
            return documentList;
        }


        public List<string> LoadSafetyDocs(string heading, string subHeading, string subheading2)
        {
            List<string> documentList = new List<string>();
                
            if(heading == "General" && subHeading == null)
            {
                documentList.Add("IMS-PO-001 - General Requirements.docx");
            }
            else if(heading == "SHE" && subHeading == null)
            {
                documentList.Add("IMS-PO-SHEQ Policy.docx");
            }
            
            else if (heading == "Resources" && subHeading == null)
            {
                documentList.Add("IMS-AP-012-Appointment of 7.doc");
                documentList.Add("IMS-AP-013-Appointment of 8.doc");
                documentList.Add("IMS-PR-004-Resources Roles Responsibility and Authority1.docx");
                documentList.Add("IMS-OR-001-IMS orginisational chart rev 5.pptx");

               
            }
            else if (subHeading == "Appointment" && heading == null)
            {
                documentList.Add("IMS-AP-001-Appointment - Divisional Manager - Master 1.docx");
                documentList.Add("IMS-AP-002-Appointment - MD - Master 1.docx");
                documentList.Add("IMS-AP-003-MRIMS Appointment - Master 1.docx");
                documentList.Add("IMS-AP-004-IMS Manager Appointment.docx");
                documentList.Add("IMS-AP-005-Safety Rep Appointment - Master 1.docx");
                documentList.Add("IMS-AP-006-First Aider Appointment - Master 1.docx");
                documentList.Add("IMS-AP-007-Fire Team Member Appointment - Master 1.doc");
                documentList.Add("IMS-AP-008-Incident Investigator Appointment - Master 1.docx");
                documentList.Add("IMS-AP-009-Risk Assessor Appointment - Master 1.docx");
                documentList.Add("IMS-AP-010-Safety Committee Chairman Appointment - Master 1.docx");
                documentList.Add("IMS-AP-011-Safety Committee Employer Member Appointment.docx");
            }
            else if (heading == "Communicate" && subHeading == null)
            {
                documentList.Add("IMS-AG-001 - Mandatary agreement.docx");
                documentList.Add("IMS-FO-002 - Agenda Safety Committee.docx");
                documentList.Add("IMS-PR-006-Communication, Participation and Consultation.docx");
              
            }
            else if (heading == "Hazard" && subHeading == null)
            {
                documentList.Add("IMS-PR-001-Methodology for Risk Rating.docx");
                documentList.Add("IMS-PR-002-IMS Hazard Identification, Risk Assessment.docx");
            }
            else if (heading == "Objectives" && subHeading == null)
            {
                documentList.Add("IMS-PO-002 - Objectives and Targets.docx");
            }
            else if (heading == "Competence" && subHeading == null)
            {
                documentList.Add("IMS-PR-005-Competence, Training and Awareness.docx");
            }
            else if (heading == "Documented" && subHeading == null)
            {
                documentList.Add("IMS-MI-001 - Master index 2018.docx");
                documentList.Add("IMS-PR-008 - Documentation and Control of Documents.docx");
            }
            else if (heading == "Control" && subHeading == null)
            {
                documentList.Add("IMS-PR-027 Control of Documents.docx");
                documentList.Add("IMS-PR-028 - Retention of IMS Documents.docx");
            }
            else if (heading == "Operational")
            {
                documentList.Add("IMS-PR-035 - Operational Control.docx");

                if(subHeading == "Procedures")
                {
                    documentList.Add("IMS-PR-009-AIDS_and_HIV.docx");
                    documentList.Add("IMS-PR-010-Appointing and managing contracotrs.docx");
                    documentList.Add("IMS-PR-011-Excavations.docx");
                    documentList.Add("IMS-PR-012-Health_Surveillance.docx");
                    documentList.Add("IMS-PR-013-Inspecting works.docx");
                    documentList.Add("IMS-PR-014-Legal compliance safety audits rev01.docx");
                    documentList.Add("IMS-PR-015-PPE issue slip.docx");
                    documentList.Add("IMS-PR-016-PPE.docx");
                    documentList.Add("IMS-PR-017-Pre-design inspections.docx");
                    documentList.Add("IMS-PR-018-Recycling.docx");
                    documentList.Add("IMS-PR-019-Travel.docx");
                    documentList.Add("IMS-PR-020-Waste reduction.docx");
                    documentList.Add("IMS-PR-021-working at night.docx");
                }
            }
            else if (heading == "Outsourcing" && subHeading == null)
            {
                documentList.Add("IMS-PR-037 - Control of Outsourced Processes.docx");
            }
            else if (heading == "Procurement" && subHeading == null)
            {
                documentList.Add("IMS-PR-036 - Procurement for Suppliers.docx");
            }
            else if (heading == "Cotractors" && subHeading == null)
            {
                documentList.Add("IMS-PR-031 - Planing, Management and Developing of Services.docx");
            }
            else if (heading == "Emergency" && subHeading == null)
            {
                documentList.Add("IMS-CH-001 - Bomb Threat Checklist.docx");
                documentList.Add("IMS-CH-002- Fire Drill Record.docx");
                
                documentList.Add("IMS-PR-022 IMS Emergency Preparedeness and Response.docx");
                documentList.Add("IMS-PR-023- Evacuation process.docx");
                
            }
            else if (heading == "Performance" && subHeading == null)
            {
                documentList.Add("IMS-PR-024 Performance, Measurement and Monitoring.docx");
            }
            else if (heading == "Evaluation" && subHeading == null)
            {
                documentList.Add("IMS-PR-025 - Evaluation of Compliance.docx");
            }
            else if (heading == "Legal" && subHeading == null)
            {
                documentList.Add("IMS-PR-003 - Legal and Other Requirements.docx");
            }
            else if (heading == "Internal" && subHeading == null)
            {
                documentList.Add("IMS-CH-010 Audit Checklist.docx");
                documentList.Add("IMS-CH-011 - Project Audit Checklist.docx");
                documentList.Add("IMS-CH-012 Internal Legal Compliance Auditing Review.docx");
                documentList.Add("IMS-PR-029 - Internal Audit.doc");
                documentList.Add("IMS-PR-030 - Internal IMS System Audits Guidelines.doc");
                documentList.Add("IMS-PR-034 - Control of Internal Audits.docx");
                documentList.Add("IMS-REG-019  Hygiene Survey Register.docx");
            }
            else if (heading == "Management" && subHeading == null)
            {
                documentList.Add("IMS-PR-007_Process_for_Management_Review.docx");
                documentList.Add("IMS-REG-021 - Attendance Register.docx");
            }
            else if (heading == "Incident")
            {
                if(subHeading == "Incident")
                {
                    documentList.Add("IMS-FRM-001 - Incident report form-Master.docx");
                    documentList.Add("IMS-PR-026 Incident Investigation & Reporting.docx");
                }
                if(subheading2 == "NCR")
                {
                    documentList.Add("IMS-PR-033 - Control of NC and CAR.docx");
                }
            }

            return documentList;
        }

        public List<string> LoadEnvironmentDocs(string heading)
        {
            List<string> documentList = new List<string>();

            if (heading == "Model")
            {
                documentList.Add("SHE 517 EMS Environmental Management System Model.doc");
                documentList.Add("SHE 518  EMS Environmental General Intention.doc");
            }
            else if (heading == "Planning")
            {
                documentList.Add("SHE PRC 500 EMS Environmental Policy Procedure.doc");
                documentList.Add("SHE PRC 501 EMS Environmental Aspects and Risk Assessment Procedure.doc");
                documentList.Add("SHE PRC 502 EMS Legal and Other Requirement Procedure.doc");
                documentList.Add("SHE PRC 503 EMS Environmental Management System Objective and Targets Procedure.doc");
                documentList.Add("SHE PRC 519 EMS Planning Procedure.doc");
              
            }
            else if (heading == "Implementation")
            {
                documentList.Add("SHE PRC 506 EMS Competence, Training and Awareness Procedure.doc");
                documentList.Add("SHE PRC 507 EMS Communication Procedure.doc");
                documentList.Add("SHE PRC 508 EMS Documentation Procedure.doc");
                documentList.Add("SHE PRC 509 EMS Control of Documents Procedure.doc");
                documentList.Add("SHE PRC 510 EMS Operational Control Procedure.doc");
                documentList.Add("SHE PRC 511 EMS Emergency Preparedness and Response Procedure.doc");
               
            }
            else if (heading == "Checking")
            {
                documentList.Add("SHE PRC 512  EMS Monitoring and Measurement Procedure.doc");
                documentList.Add("SHE PRC 514 EMS Control of Records Procedure.doc");
                documentList.Add("SHE PRC 515 EMS Internal Audit Procedure.doc");
                documentList.Add("SHE PRC 520 EMS Evaluation of Compliance Procedure.doc");
              
              
            }
            else if (heading == "Review")
            {
                documentList.Add("SHE PRC 516 EMS Management Review Procedure.doc");
            }
            else if (heading == "Documentation")
            {
                documentList.Add("SHE PRC 462 EMS Waste Management.doc");
                documentList.Add("SHE REG 524 EMS Communication  Register for Environmental System.docx");
                documentList.Add("SHE REG 525 EMS Environmental Customer Complaints Register  (1).doc");
                documentList.Add("SHE REG 525 EMS Environmental Customer Complaints Register .doc");
                documentList.Add("SHE REG 525 EMS Environmental Customer Complaints Register.doc");
                documentList.Add("SHE REG 526 EMS Environmental Customer Complaint Form.doc");
                
            }
            else if (heading == "SOP")
            {
                documentList.Add("SHE PRC 460 EMS Safe use of Paint.doc");
                documentList.Add("SHE PRC 557 EMS HOW to USE a Spill Kit.doc");
            }

            return documentList;
        }

        public void TurnToPDF(/*string filename,*/ WordLibrary.Application wordApp, WordLibrary.Document wordDoc, WebBrowser webBrowser)
        {
            wordApp = new WordLibrary.Application();
            wordDoc = wordApp.Documents.Open(@"C:\Certificate.docx");
            wordDoc.Activate();
            wordDoc.SaveAs2(@"C:\document.pdf", WdSaveFormat.wdFormatPDF);
            wordDoc.Close();
            wordApp.Quit();

            webBrowser.Navigate(@"C:\document.pdf");
        }
    }
}
