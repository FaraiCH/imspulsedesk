﻿
namespace IMSPulseCloud
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.documentManager1 = new DevExpress.XtraBars.Docking2010.DocumentManager(this.components);
            this.windowsUIView = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView(this.components);
            this.flyout1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Flyout(this.components);
            this.groupItemDetailPageDocument = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.GroupItemDetailPageTile = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.tileContainer1 = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer(this.components);
            this.groupDetailPageDocument = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.GroupDetailPageTile = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            this.itemDetailPageDocument = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document(this.components);
            this.ItemDetailPageTile = new DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flyout1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupItemDetailPageDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupItemDetailPageTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDetailPageDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDetailPageTile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemDetailPageDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailPageTile)).BeginInit();
            this.SuspendLayout();
            // 
            // documentManager1
            // 
            this.documentManager1.ContainerControl = this;
            this.documentManager1.View = this.windowsUIView;
            this.documentManager1.ViewCollection.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseView[] {
            this.windowsUIView});
            // 
            // windowsUIView
            // 
            this.windowsUIView.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI Light", 36F);
            this.windowsUIView.AppearanceCaption.Options.UseFont = true;
            this.windowsUIView.Caption = "Header";
            this.windowsUIView.ContentContainers.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.IContentContainer[] {
            this.flyout1,
            this.tileContainer1});
            this.windowsUIView.Documents.AddRange(new DevExpress.XtraBars.Docking2010.Views.BaseDocument[] {
            this.groupItemDetailPageDocument,
            this.groupDetailPageDocument,
            this.itemDetailPageDocument});
            this.windowsUIView.Tiles.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.GroupItemDetailPageTile,
            this.GroupDetailPageTile,
            this.ItemDetailPageTile});
            // 
            // flyout1
            // 
            this.flyout1.Name = "flyout1";
            // 
            // groupItemDetailPageDocument
            // 
            this.groupItemDetailPageDocument.Caption = "GroupItemDetailPage";
            this.groupItemDetailPageDocument.ControlName = "GroupItemDetailPage";
            this.groupItemDetailPageDocument.ControlTypeName = "IMSPulseCloud.GroupItemDetailPage";
            // 
            // GroupItemDetailPageTile
            // 
            this.GroupItemDetailPageTile.ActivationTarget = this.flyout1;
            this.GroupItemDetailPageTile.Document = this.groupItemDetailPageDocument;
            this.GroupItemDetailPageTile.Name = "GroupItemDetailPageTile";
            this.GroupItemDetailPageTile.Tag = ((short)(1));
            // 
            // tileContainer1
            // 
            this.tileContainer1.Items.AddRange(new DevExpress.XtraBars.Docking2010.Views.WindowsUI.BaseTile[] {
            this.GroupItemDetailPageTile,
            this.GroupDetailPageTile,
            this.ItemDetailPageTile});
            this.tileContainer1.Name = "tileContainer1";
            // 
            // groupDetailPageDocument
            // 
            this.groupDetailPageDocument.Caption = "GroupDetailPage";
            this.groupDetailPageDocument.ControlName = "GroupDetailPage";
            this.groupDetailPageDocument.ControlTypeName = "IMSPulseCloud.GroupDetailPage";
            // 
            // GroupDetailPageTile
            // 
            this.GroupDetailPageTile.Document = this.groupDetailPageDocument;
            this.tileContainer1.SetID(this.GroupDetailPageTile, 1);
            this.GroupDetailPageTile.Name = "GroupDetailPageTile";
            // 
            // itemDetailPageDocument
            // 
            this.itemDetailPageDocument.Caption = "ItemDetailPage";
            this.itemDetailPageDocument.ControlName = "ItemDetailPage";
            this.itemDetailPageDocument.ControlTypeName = "IMSPulseCloud.ItemDetailPage";
            // 
            // ItemDetailPageTile
            // 
            this.ItemDetailPageTile.Document = this.itemDetailPageDocument;
            this.tileContainer1.SetID(this.ItemDetailPageTile, 2);
            this.ItemDetailPageTile.Name = "ItemDetailPageTile";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1090, 611);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Glow;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IconOptions.Image = ((System.Drawing.Image)(resources.GetObject("MainForm.IconOptions.Image")));
            this.Name = "MainForm";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.documentManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowsUIView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flyout1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupItemDetailPageDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupItemDetailPageTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileContainer1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupDetailPageDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupDetailPageTile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemDetailPageDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailPageTile)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking2010.DocumentManager documentManager1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.WindowsUIView windowsUIView;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer tileContainer;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Flyout flyout1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.TileContainer tileContainer1;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile GroupItemDetailPageTile;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document groupItemDetailPageDocument;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile GroupDetailPageTile;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document groupDetailPageDocument;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Tile ItemDetailPageTile;
        private DevExpress.XtraBars.Docking2010.Views.WindowsUI.Document itemDetailPageDocument;
    }
}

