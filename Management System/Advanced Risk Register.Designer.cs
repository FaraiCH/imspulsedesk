﻿namespace Management_System
{
    partial class Advanced_Risk_Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Advanced_Risk_Register));
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRating = new System.Windows.Forms.TextBox();
            this.cboCost = new System.Windows.Forms.ComboBox();
            this.cboReputation = new System.Windows.Forms.ComboBox();
            this.cboRegulation = new System.Windows.Forms.ComboBox();
            this.cboContractTerms = new System.Windows.Forms.ComboBox();
            this.cboHumanHealth = new System.Windows.Forms.ComboBox();
            this.cboLossContracts = new System.Windows.Forms.ComboBox();
            this.cboPrevious = new System.Windows.Forms.ComboBox();
            this.cboLike = new System.Windows.Forms.ComboBox();
            this.cboPro = new System.Windows.Forms.ComboBox();
            this.txtProb = new System.Windows.Forms.TextBox();
            this.txtRisk = new System.Windows.Forms.TextBox();
            this.txtEntry = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 5;
            this.bunifuFlatButton2.ButtonText = "Confirm Changes";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(445, 606);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(216, 48);
            this.bunifuFlatButton2.TabIndex = 56;
            this.bunifuFlatButton2.Text = "Confirm Changes";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.bunifuFlatButton2_Click);
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 5;
            this.bunifuFlatButton1.ButtonText = "Open Risk Register";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(151, 606);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(216, 48);
            this.bunifuFlatButton1.TabIndex = 55;
            this.bunifuFlatButton1.Text = "Open Risk Register";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(151, 12);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(530, 82);
            this.listBox1.TabIndex = 54;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtRating);
            this.groupBox1.Controls.Add(this.cboCost);
            this.groupBox1.Controls.Add(this.cboReputation);
            this.groupBox1.Controls.Add(this.cboRegulation);
            this.groupBox1.Controls.Add(this.cboContractTerms);
            this.groupBox1.Controls.Add(this.cboHumanHealth);
            this.groupBox1.Controls.Add(this.cboLossContracts);
            this.groupBox1.Controls.Add(this.cboPrevious);
            this.groupBox1.Controls.Add(this.cboLike);
            this.groupBox1.Controls.Add(this.cboPro);
            this.groupBox1.Controls.Add(this.txtProb);
            this.groupBox1.Controls.Add(this.txtRisk);
            this.groupBox1.Controls.Add(this.txtEntry);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Location = new System.Drawing.Point(43, 106);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(739, 494);
            this.groupBox1.TabIndex = 60;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Risk Entry";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label4.Location = new System.Drawing.Point(476, 444);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 20);
            this.label4.TabIndex = 61;
            this.label4.Text = "label4";
            this.c1ThemeController1.SetTheme(this.label4, "(default)");
            // 
            // txtRating
            // 
            this.txtRating.Location = new System.Drawing.Point(168, 444);
            this.txtRating.Name = "txtRating";
            this.txtRating.Size = new System.Drawing.Size(156, 20);
            this.txtRating.TabIndex = 44;
            // 
            // cboCost
            // 
            this.cboCost.FormattingEnabled = true;
            this.cboCost.Location = new System.Drawing.Point(549, 360);
            this.cboCost.Name = "cboCost";
            this.cboCost.Size = new System.Drawing.Size(169, 21);
            this.cboCost.TabIndex = 43;
            this.cboCost.SelectedIndexChanged += new System.EventHandler(this.cboCost_SelectedIndexChanged);
            // 
            // cboReputation
            // 
            this.cboReputation.FormattingEnabled = true;
            this.cboReputation.Location = new System.Drawing.Point(168, 360);
            this.cboReputation.Name = "cboReputation";
            this.cboReputation.Size = new System.Drawing.Size(156, 21);
            this.cboReputation.TabIndex = 42;
            this.cboReputation.SelectedIndexChanged += new System.EventHandler(this.cboReputation_SelectedIndexChanged);
            // 
            // cboRegulation
            // 
            this.cboRegulation.FormattingEnabled = true;
            this.cboRegulation.Location = new System.Drawing.Point(549, 283);
            this.cboRegulation.Name = "cboRegulation";
            this.cboRegulation.Size = new System.Drawing.Size(169, 21);
            this.cboRegulation.TabIndex = 41;
            this.cboRegulation.SelectedIndexChanged += new System.EventHandler(this.cboRegulation_SelectedIndexChanged);
            // 
            // cboContractTerms
            // 
            this.cboContractTerms.FormattingEnabled = true;
            this.cboContractTerms.Location = new System.Drawing.Point(168, 286);
            this.cboContractTerms.Name = "cboContractTerms";
            this.cboContractTerms.Size = new System.Drawing.Size(156, 21);
            this.cboContractTerms.TabIndex = 40;
            this.cboContractTerms.SelectedIndexChanged += new System.EventHandler(this.cboContractTerms_SelectedIndexChanged);
            // 
            // cboHumanHealth
            // 
            this.cboHumanHealth.FormattingEnabled = true;
            this.cboHumanHealth.Location = new System.Drawing.Point(549, 213);
            this.cboHumanHealth.Name = "cboHumanHealth";
            this.cboHumanHealth.Size = new System.Drawing.Size(169, 21);
            this.cboHumanHealth.TabIndex = 39;
            this.cboHumanHealth.SelectedIndexChanged += new System.EventHandler(this.cboHumanHealth_SelectedIndexChanged);
            // 
            // cboLossContracts
            // 
            this.cboLossContracts.FormattingEnabled = true;
            this.cboLossContracts.Location = new System.Drawing.Point(168, 213);
            this.cboLossContracts.Name = "cboLossContracts";
            this.cboLossContracts.Size = new System.Drawing.Size(156, 21);
            this.cboLossContracts.TabIndex = 38;
            this.cboLossContracts.SelectedIndexChanged += new System.EventHandler(this.cboLossContracts_SelectedIndexChanged);
            // 
            // cboPrevious
            // 
            this.cboPrevious.FormattingEnabled = true;
            this.cboPrevious.Location = new System.Drawing.Point(168, 139);
            this.cboPrevious.Name = "cboPrevious";
            this.cboPrevious.Size = new System.Drawing.Size(156, 21);
            this.cboPrevious.TabIndex = 37;
            this.cboPrevious.SelectedIndexChanged += new System.EventHandler(this.cboPrevious_SelectedIndexChanged);
            // 
            // cboLike
            // 
            this.cboLike.FormattingEnabled = true;
            this.cboLike.Location = new System.Drawing.Point(549, 75);
            this.cboLike.Name = "cboLike";
            this.cboLike.Size = new System.Drawing.Size(169, 21);
            this.cboLike.TabIndex = 36;
            this.cboLike.SelectedIndexChanged += new System.EventHandler(this.cboLike_SelectedIndexChanged);
            // 
            // cboPro
            // 
            this.cboPro.FormattingEnabled = true;
            this.cboPro.Location = new System.Drawing.Point(549, 19);
            this.cboPro.Name = "cboPro";
            this.cboPro.Size = new System.Drawing.Size(169, 21);
            this.cboPro.TabIndex = 35;
            // 
            // txtProb
            // 
            this.txtProb.Location = new System.Drawing.Point(549, 139);
            this.txtProb.Name = "txtProb";
            this.txtProb.Size = new System.Drawing.Size(169, 20);
            this.txtProb.TabIndex = 34;
            // 
            // txtRisk
            // 
            this.txtRisk.Location = new System.Drawing.Point(168, 76);
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.Size = new System.Drawing.Size(156, 20);
            this.txtRisk.TabIndex = 33;
            // 
            // txtEntry
            // 
            this.txtEntry.Location = new System.Drawing.Point(168, 19);
            this.txtEntry.Name = "txtEntry";
            this.txtEntry.Size = new System.Drawing.Size(156, 20);
            this.txtEntry.TabIndex = 32;
            this.txtEntry.TextChanged += new System.EventHandler(this.txtEntry_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 451);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Cons. Rating";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(399, 368);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Est. Cost of Correction";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 363);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Impact on Company Reputation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(354, 291);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(157, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Potential Violation of Regulation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Entry #";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(423, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Process";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 83);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Risk";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 286);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(156, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Inability to Meet Contract Terms";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(423, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Likelihood";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(358, 216);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(155, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Potential Risk to Human Health";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Previous Occurence";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 216);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Potential Loss of Contracts";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(423, 142);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(105, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Prob Rating (Approx)";
            // 
            // Advanced_Risk_Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(820, 666);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bunifuFlatButton2);
            this.Controls.Add(this.bunifuFlatButton1);
            this.Controls.Add(this.listBox1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Advanced_Risk_Register";
            this.Text = "Advanced Risk Register";
            this.c1ThemeController1.SetTheme(this, "MacBlue");
            this.Load += new System.EventHandler(this.Advanced_Risk_Register_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRating;
        private System.Windows.Forms.ComboBox cboCost;
        private System.Windows.Forms.ComboBox cboReputation;
        private System.Windows.Forms.ComboBox cboRegulation;
        private System.Windows.Forms.ComboBox cboContractTerms;
        private System.Windows.Forms.ComboBox cboHumanHealth;
        private System.Windows.Forms.ComboBox cboLossContracts;
        private System.Windows.Forms.ComboBox cboPrevious;
        private System.Windows.Forms.ComboBox cboLike;
        private System.Windows.Forms.ComboBox cboPro;
        private System.Windows.Forms.TextBox txtProb;
        private System.Windows.Forms.TextBox txtRisk;
        private System.Windows.Forms.TextBox txtEntry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private System.Windows.Forms.Label label4;
    }
}