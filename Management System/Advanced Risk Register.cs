﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XlApp = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.Diagnostics;
using Aspose.Cells;
using DocumentManager;
using Renci.SshNet;

namespace Management_System
{
    public partial class Advanced_Risk_Register : Form
    {
        string path = ServerPath.PathLocation() + @"Shared\RISK REGISTER.xlsx";
        public Advanced_Risk_Register()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);

            InitializeComponent();
        }

        private void Advanced_Risk_Register_Load(object sender, EventArgs e)
        {
            try
            {
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";

                //try
                //{
                using (var client = new SftpClient(host, port, username, password))
                {

                    client.Connect();

                    if (client.IsConnected)
                    {

                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {


                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                            Workbook works = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                            Worksheet sheets = works.Worksheets[1];

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue);
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared");
                            }

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                            {
                                using (var fileStream = new FileStream(path, FileMode.Open))
                                {

                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }
                            }
                            else
                            {
                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/"))
                                {
                                    if (!item.IsDirectory)
                                    {
                                        using (Stream fileStreams = File.Create(ServerPath.PathLocation() + @"/Shared/" + item.Name))
                                        {
                                            client.DownloadFile(item.FullName, fileStreams);
                                        }

                                    }

                                }

                                label4.Visible = false;
                                txtProb.ReadOnly = true;
                                txtRating.ReadOnly = true;
                                ViewEntries();



                                Workbook work = new Workbook(path);
                                Worksheet sheet = work.Worksheets[4];

                                int row = sheet.Cells.MaxDataRow;

                                for (int i = 2; i < row; i++)
                                {
                                    if (sheet.Cells["I" + i].Type != CellValueType.IsNull && sheet.Cells["I" + i].Value.ToString() != "All Processes")
                                    {
                                        cboPro.Items.Add(sheet.Cells["I" + i].Value);
                                    }

                                    if (sheet.Cells["K" + i].Type != CellValueType.IsNull)
                                    {
                                        cboLike.Items.Add(sheet.Cells["K" + i].Value);
                                    }

                                    if (sheet.Cells["L" + i].Type != CellValueType.IsNull)
                                    {
                                        cboPrevious.Items.Add(sheet.Cells["L" + i].Value);
                                    }

                                    if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                                    {
                                        cboLossContracts.Items.Add(sheet.Cells["M" + i].Value);
                                    }

                                    if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                                    {
                                        cboHumanHealth.Items.Add(sheet.Cells["M" + i].Value);
                                    }
                                    if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                                    {
                                        cboContractTerms.Items.Add(sheet.Cells["M" + i].Value);
                                    }
                                    if (sheet.Cells["N" + i].Type != CellValueType.IsNull)
                                    {
                                        cboRegulation.Items.Add(sheet.Cells["N" + i].Value);
                                    }
                                    if (sheet.Cells["P" + i].Type != CellValueType.IsNull)
                                    {
                                        cboReputation.Items.Add(sheet.Cells["P" + i].Value);
                                    }
                                    if (sheet.Cells["O" + i].Type != CellValueType.IsNull)
                                    {
                                        cboCost.Items.Add(sheet.Cells["O" + i].Value);
                                    }


                                }

                                ViewEntries();
                            }
                        }
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }
            
           
        }


        public void EditCar()
        {
          

                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[2];
            

                for (int i = 3; i < sheet.Cells.Rows.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString().Contains(sheet.Cells[i, 16].Value.ToString()))
                    {
                        if (sheet.Cells[i, 0].Value.ToString() == txtEntry.Text)
                        {
                        sheet.Cells[i, 1].PutValue(cboPro.SelectedItem.ToString());
                        sheet.Cells[i, 2].PutValue(txtRisk.Text);
                        sheet.Cells[i, 3].PutValue(cboLike.SelectedItem.ToString());
                        sheet.Cells[i, 4].PutValue(cboPrevious.SelectedItem.ToString());
                        sheet.Cells[i, 6].PutValue(cboLossContracts.SelectedItem.ToString());
                        sheet.Cells[i, 7].PutValue(cboHumanHealth.SelectedItem.ToString());
                        sheet.Cells[i, 8].PutValue(cboContractTerms.SelectedItem.ToString());
                        sheet.Cells[i, 9].PutValue(cboRegulation.SelectedItem.ToString());
                        sheet.Cells[i, 10].PutValue(cboReputation.SelectedItem.ToString());
                        sheet.Cells[i, 11].PutValue(cboCost.SelectedItem.ToString());

                    
                        }
                        break;
                    }
                }

                work.CalculateFormula();
                work.Save(path);
                MessageBox.Show("Changes Complete. Please view the CAR LOG for detailed analysis", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        
        }


        public void ProbRating()
        {
            float like = 0, prev = 0, proRating = 0;

            switch (cboLike.SelectedIndex)
            {
                case 0:
                    like = 1;
                    break;
                case 1:
                    like = 2;
                    break;
                case 2:
                    like = 3;
                    break;
                case 3:
                    like = 4;
                    break;
                case 4:
                    like = 5;
                    break;
            }

            switch (cboPrevious.SelectedIndex)
            {
                case 0:
                    prev = 1;
                    break;
                case 1:
                    prev = 2;
                    break;
                case 2:
                    prev = 3;
                    break;
                case 3:
                    prev = 4;
                    break;
                case 4:
                    prev = 5;
                    break;
            }

            proRating += (like + prev) / 2;

            if (proRating >= 3)
            {
                txtProb.ReadOnly = false;
                txtProb.Text = proRating.ToString();
                txtProb.BackColor = Color.Red;
                txtProb.ForeColor = Color.White;
                txtProb.ReadOnly = true;
            }
            else if (proRating < 2)
            {

                txtProb.ReadOnly = false;
                txtProb.Text = proRating.ToString();
                txtProb.BackColor = Color.LightGreen;
                txtProb.ForeColor = Color.Black;
                txtProb.ReadOnly = true;
            }
            else if (proRating > 2 && proRating < 3)
            {

                txtProb.ReadOnly = false;
                txtProb.Text = proRating.ToString();
                txtProb.BackColor = Color.Yellow;
                txtProb.ForeColor = Color.Black;
                txtProb.ReadOnly = true;
            }
         
        }

        public void CalcFullRating()
        {
            float lossContracts, humanHealth, contractTerms, regulation, reputation, cost = 0;
            float rating = 0;

            switch (cboLossContracts.SelectedIndex)
            {
                case 0:
                    lossContracts = 1;
                    break;
                case 1:
                    lossContracts = 2;
                    break;
                case 2:
                    lossContracts = 3;
                    break;
                case 3:
                    lossContracts = 4;
                    break;
                case 4:
                    lossContracts = 5;
                    break;
                default:
                    lossContracts = 0;
                    break;

            }

            switch (cboHumanHealth.SelectedIndex)
            {
                case 0:
                    humanHealth = 1;
                    break;
                case 1:
                    humanHealth = 2;
                    break;
                case 2:
                    humanHealth = 3;
                    break;
                case 3:
                    humanHealth = 4;
                    break;
                case 4:
                    humanHealth = 5;
                    break;
                default:
                    humanHealth = 0;
                    break;

            }

            switch (cboContractTerms.SelectedIndex)
            {
                case 0:
                    contractTerms = 1;
                    break;
                case 1:
                    contractTerms = 2;
                    break;
                case 2:
                    contractTerms = 3;
                    break;
                case 3:
                    contractTerms = 4;
                    break;
                case 4:
                    contractTerms = 5;
                    break;
                default:
                    contractTerms = 0;
                    break;

            }

            switch (cboRegulation.SelectedIndex)
            {
                case 0:
                    regulation = 1;
                    break;
                case 1:
                    regulation = 2;
                    break;
                case 2:
                    regulation = 3;
                    break;
                case 3:
                    regulation = 4;
                    break;
                case 4:
                    regulation = 5;
                    break;
                default:
                    regulation = 0;
                    break;

            }

            switch (cboReputation.SelectedIndex)
            {
                case 0:
                    reputation = 1;
                    break;
                case 1:
                    reputation = 2;
                    break;
                case 2:
                    reputation = 3;
                    break;
                case 3:
                    reputation = 4;
                    break;
                case 4:
                    reputation = 5;
                    break;
                default:
                    reputation = 0;
                    break;

            }

            switch (cboCost.SelectedIndex)
            {
                case 0:
                    cost = 1;
                    break;
                case 1:
                    cost = 2;
                    break;
                case 2:
                    cost = 3;
                    break;
                case 3:
                    cost = 4;
                    break;
                case 4:
                    cost = 5;
                    break;

            }

            rating = (lossContracts + humanHealth + contractTerms + regulation + reputation + cost) / 6;

            if (rating >= 3)
            {
                txtRating.ReadOnly = false;
                txtRating.Text = rating.ToString();
                txtRating.BackColor = Color.Red;
                txtRating.ForeColor = Color.White;
                txtRating.ReadOnly = true;
                label4.Visible = true;
                label4.Text = "Immediate Action Needed";
                label4.BackColor = Color.DarkRed;
                label4.ForeColor = Color.White;
            }
            else if (rating < 2)
            {
                txtRating.ReadOnly = false;
                txtRating.Text = rating.ToString();
                txtRating.BackColor = Color.LightGreen;
                txtRating.ForeColor = Color.Black;
                txtRating.ReadOnly = true;
                label4.Visible = true;
                label4.Text = "No Action Needed";
                label4.BackColor = Color.Green;
                label4.ForeColor = Color.White;
            }
            else if (rating > 2 && rating < 3)
            {
                txtRating.ReadOnly = false;
                txtRating.Text = rating.ToString();
                txtRating.BackColor = Color.Yellow;
                txtRating.ForeColor = Color.Black;
                txtRating.ReadOnly = true;
                label4.Visible = true;
                label4.Text = "Action Might Be Needed";
                label4.BackColor = Color.LightGoldenrodYellow;
                label4.ForeColor = Color.Black;
            }
  

        }

        public void ViewEntries()
        {
            try
            {
          
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[2];

                int row = sheet.Cells.Rows.Count;

                for (int i = 1; i < row; i++)
                {
                    if (sheet.Cells[i, 0].Value != null)
                    {
                        listBox1.Items.Add(sheet.Cells[i, 0].Value + "\t\t\t" + sheet.Cells[i, 1].Value + "\t\t\t\t" + sheet.Cells[i,2].Value + "\t\t\t\t\t" + sheet.Cells[i, 16].Value);
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ShowCar()
        {

            try
            {
              

                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[2];

                for (int i = 3; i < sheet.Cells.Rows.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString().Contains(sheet.Cells[i, 16].Value.ToString()))
                    {
                        if (sheet.Cells[i, 0].Type != CellValueType.IsNull)
                        {
                            txtEntry.Text = sheet.Cells[i, 0].Value.ToString();

                            cboPro.SelectedItem = sheet.Cells[i, 1].Value.ToString();
                            txtRisk.Text = sheet.Cells[i, 2].Value.ToString();
                            cboLike.SelectedItem = sheet.Cells[i, 3].Value.ToString();
                            txtProb.Text = sheet.Cells[i, 5].DoubleValue.ToString();
                            cboPrevious.SelectedItem = sheet.Cells[i, 4].Value.ToString();
                            cboLossContracts.SelectedItem = sheet.Cells[i, 6].Value.ToString();
                            cboHumanHealth.SelectedItem = sheet.Cells[i, 7].Value.ToString();
                            cboContractTerms.SelectedItem = sheet.Cells[i, 8].Value.ToString();
                            cboRegulation.SelectedItem = sheet.Cells[i, 9].Value.ToString();
                            cboReputation.SelectedItem = sheet.Cells[i, 10].Value.ToString();
                            cboCost.SelectedItem = sheet.Cells[i, 11].Value.ToString();
                            txtRating.Text = sheet.Cells[i, 12].DoubleValue.ToString();

                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Select valid entry.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }


        public void ShowEntry()
        {
            try
            {
                Workbook work = new Workbook(path);

                Worksheet sheet = work.Worksheets[5];

                Cells cell = sheet.Cells;


                //var style = work.Worksheets[5].Cells[i, 12].GetStyle();



                if (txtEntry.Text == cell[0, 0].StringValue)
                {
                    txtEntry.Enabled = false;
                    cboPro.Enabled = false;
                    txtRisk.Enabled = false;
                    cboLike.Enabled = false;
                    cboPrevious.Enabled = false;
                    txtProb.Enabled = false;
                    cboLossContracts.Enabled = false;
                    cboHumanHealth.Enabled = false;
                    cboContractTerms.Enabled = false;
                    cboRegulation.Enabled = false;
                    cboReputation.Enabled = false;
                    cboCost.Enabled = false;
                    txtRating.Enabled = false;
                }
                else
                {
                    txtEntry.Enabled = true;
                    cboPro.Enabled = true;
                    txtRisk.Enabled = true;
                    cboLike.Enabled = true;
                    cboPrevious.Enabled = true;
                    txtProb.Enabled = true;
                    cboLossContracts.Enabled = true;
                    cboHumanHealth.Enabled = true;
                    cboContractTerms.Enabled = true;
                    cboRegulation.Enabled = true;
                    cboReputation.Enabled = true;
                    cboCost.Enabled = true;
                    txtRating.Enabled = true;
                }

                for (int i = 0; i < cell.Rows.Count; i++)
                {

                    if (listBox1.SelectedItem.ToString().Contains(cell[i, 0].StringValue))
                    {
                        txtEntry.Text = cell[i, 0].StringValue;
                        cboPro.Text = cell[i, 1].StringValue;
                        txtRisk.Text = cell[i, 2].StringValue;
                        cboLike.Text = cell[i, 3].StringValue;
                        cboPrevious.Text = cell[i, 4].StringValue;
                        txtProb.Text = cell[i, 5].StringValue;
                        cboLossContracts.Text = cell[i, 6].StringValue;
                        cboHumanHealth.Text = cell[i, 7].StringValue;
                        cboContractTerms.Text = cell[i, 8].StringValue;
                        cboRegulation.Text = cell[i, 9].StringValue;
                        cboReputation.Text = cell[i, 10].StringValue;
                        cboCost.Text = cell[1, 11].StringValue;
                        txtRating.Text = cell[i, 12].StringValue;

                        break;
                    }

                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception)
            {
                MessageBox.Show("You did not pick an item.", "Nothing Chosen", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowCar();
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {           
            EditCar();

            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";

            try
            {

                using (var client = new SftpClient(host, port, username, password))
                {


                    if (File.Exists(@"C:\Reader\Loader.xlsx"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                        Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                        Worksheet sheet = work.Worksheets[1];

                        client.Connect();


                        if (client.IsConnected)
                        {
                            //MessageBox.Show("I'm connected to the client");  

                            string result = new DirectoryInfo(path).Parent.Name;

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                            }


                            using (var fileStream = new FileStream(path, FileMode.Open))
                            {

                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                if (client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                                {
                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }

                                MessageBox.Show("Changes Saved");
                            }

                        }
                        else
                        {
                            MessageBox.Show("I couldn't connect");
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboLike_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProbRating();
        }

        private void cboPrevious_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProbRating();
        }

        private void cboLossContracts_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboHumanHealth_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboContractTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboRegulation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboReputation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboCost_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();

        }


        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            Process.Start(path);
        }

        private void txtEntry_TextChanged(object sender, EventArgs e)
        {
            if (txtEntry.Text.Contains("Ln"))
            {
                txtEntry.Enabled = false;
                cboPro.Enabled = false;
                txtRisk.Enabled = false;
                cboLike.Enabled = false;
                cboPrevious.Enabled = false;
                txtProb.Enabled = false;
                cboLossContracts.Enabled = false;
                cboHumanHealth.Enabled = false;
                cboContractTerms.Enabled = false;
                cboRegulation.Enabled = false;
                cboReputation.Enabled = false;
                cboCost.Enabled = false;
                txtRating.Enabled = false;
            }
            else
            {
                txtEntry.Enabled = true;
                cboPro.Enabled = true;
                txtRisk.Enabled = true;
                cboLike.Enabled = true;
                cboPrevious.Enabled = true;
                txtProb.Enabled = true;
                cboLossContracts.Enabled = true;
                cboHumanHealth.Enabled = true;
                cboContractTerms.Enabled = true;
                cboRegulation.Enabled = true;
                cboReputation.Enabled = true;
                cboCost.Enabled = true;
                txtRating.Enabled = true;
            }
        }
    }

}
