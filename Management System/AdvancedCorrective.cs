﻿
using Aspose.Cells;
using DocumentManager;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Management_System
{
    public partial class AdvancedCorrective : Form
    {

        string path = ServerPath.PathLocation() + @"Shared\CAR LOG.xlsx";

        public AdvancedCorrective()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            InitializeComponent();
        }



        private void AdvancedCorrective_Load(object sender, EventArgs e)
        {
            try
            {
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";

                //try
                //{
                using (var client = new SftpClient(host, port, username, password))
                {

                    client.Connect();

                    if (client.IsConnected)
                    {

                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {


                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                            Workbook works = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                            Worksheet sheets = works.Worksheets[1];

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue);
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared");
                            }

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                            {
                                using (var fileStream = new FileStream(path, FileMode.Open))
                                {

                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }
                            }
                            else
                            {
                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/"))
                                {
                                    if (!item.IsDirectory)
                                    {
                                        using (Stream fileStreams = File.Create(ServerPath.PathLocation() + @"/Shared/" + item.Name))
                                        {
                                            client.DownloadFile(item.FullName, fileStreams);
                                        }

                                    }

                                }

                                datAssigned.Value = DateTime.Now;
                                datDueDate.Value = DateTime.Now;
                                datActionComplete.Value = DateTime.Now;



                                Workbook work = new Workbook(path);
                                Worksheet sheet = work.Worksheets[3];

                                int row = sheet.Cells.Rows.Count;

                                for (int i = 4; i < row; i++)
                                {
                                    if (sheet.Cells[i, 0].Value != null)
                                    {
                                        cboProcess.Items.Add(sheet.Cells[i, 0].Value);
                                    }

                                }

                                cboType.Items.Add("Corrective Action");
                                cboType.Items.Add("Preventive Action");
                                cboType.Items.Add("Non-Conformity");
                                cboType.Items.Add("Suggestion");

                                cboSource.Items.Add("Customer Feedback");
                                cboSource.Items.Add("Employer/Employee Feedback");
                                cboSource.Items.Add("Supplier/Subcontractor");
                                cboSource.Items.Add("Internal Audit");
                                cboSource.Items.Add("External Audit");
                                cboSource.Items.Add("Management Review");
                                cboSource.Items.Add("Other");
                                ViewEntries();
                                ReadLogs();
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Unable to connect to the server. It could be an internet issue. Therefore, you will not be able to update or get updates pf the NCR.");
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            

  
        }

        public void ReadLogs()
        {
            string isChecked = File.ReadAllText(@"C:\Management\Management System\ID_46664\Process.txt");
            string isMula = File.ReadAllText(@"C:\Management\Management System\ID_46664\Mula.txt");

            if (isChecked.Contains("Checked"))
            {
                lblone.Text = "ACTIVATED";
                lblone.ForeColor = Color.Red;
            }
            else
            {
                lblone.Text = "DEACTIVATED";
                lblone.ForeColor = Color.Blue;
            }

            if (isMula.Contains("Checked"))
            {
                lbltwo.Text = "ACTIVATED";
                lbltwo.ForeColor = Color.Red;
            }
            else
            {
                lbltwo.Text = "DEACTIVATED";
                lbltwo.ForeColor = Color.Blue;
            }
        }

        public void ViewEntries()
        {
            try
            {
              

                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];

                
                int row = sheet.Cells.Rows.Count;

                for (int i = 2; i < row; i++)
                {
                    if(sheet.Cells[i, 0].Value != null)
                    {
                        listBox1.Items.Add(sheet.Cells[i, 0].Value + "\t\t" +  sheet.Cells[i, 1].Value + "\t\t\t\t" + sheet.Cells[i,2].Value);                      
                    }
                  
                }
                      
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
               
        }

        public void EditCar()
        {
            try
            {
                
                
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];
                work.Settings.CalcMode = CalcModeType.Automatic;

                int carNum = 0;
                int.TryParse(txtCar.Text, out carNum);

                for (int i = 2; i < sheet.Cells.Rows.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString().Contains(sheet.Cells[i, 1].Value.ToString()))
                    {
                        if (sheet.Cells[i, 0].Value.ToString() != null)
                        {
                            sheet.Cells[i, 0].PutValue(carNum);
                            sheet.Cells[i,2].PutValue(cboType.SelectedItem.ToString());
                            sheet.Cells[i,3].PutValue(cboSource.SelectedItem.ToString());
                            sheet.Cells[i,4].PutValue(cboProcess.SelectedItem.ToString());
                            sheet.Cells[i,5].PutValue(txtSumbitter.Text);
                            sheet.Cells[i,6].PutValue(txtDescription.Text);
                            sheet.Cells[i,7].PutValue(txtAssignedTo.Text);
                            sheet.Cells[i,8].PutValue(datAssigned.Value.Date);
                            sheet.Cells[i,9].PutValue(datDueDate.Value.Date);                          
                            sheet.Cells[i,11].PutValue(datActionComplete.Value.Date);                             
                        }                      
                        break;
                    }
                }

                work.CalculateFormula();
                work.Save(path);
                MessageBox.Show("Changes Complete. Please view the CAR LOG for detailed analysis", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        //public void EditCorrectiveAction()
        //{
        //    try
        //    {
        //        DateTime dateTime = DateTime.Now;

        //        var myDate = dateTime;

        //        var assignedDate = DateTime.ParseExact(txtAssignedDate.Text, "dd-MM-yyyy", null);
        //        var myAssigned = assignedDate;

        //        var actualDueDate = DateTime.ParseExact(txtActualDueDate.Text, "dd-MM-yyyy", null);
        //        var myActualDueDate = actualDueDate;

        //        var actionCompleteDate = DateTime.ParseExact(textBox1.Text, "dd-MM-yyyy", null);
        //        var myAction = actionCompleteDate;

        //        string filename = @"C:\Environment Management\4. Checking and Corrective Actions\CAR LOG.xlsx";
        //        string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};" + @"Extended Properties = 'Excel 8.0;HDR=YES'", filename);
        //        string sql = @"UPDATE [" + "Log_Application" + "$] SET Type = @type, Source = @source, Process = @process, " +
        //            "Name_of_Person_Submitting_CAR = @person, Brief_Description = @brief, Assigned_To = @assigned, Date_Assigned = @dateAssigned, Requested_Action_Due_Date = @dueDate, Action_Complete_Date = @completeDate , Overdue = @overdue, Closed = @closed, Days_To_Meet_Due_Date = @daysLeft, Days_Late = @daysLate" +
        //            " WHERE CAR_No = @car";
              
        //        OleDbConnection con = new OleDbConnection(conString);

        //        con.Open();
        //        OleDbCommand cmd = new OleDbCommand(sql, con);
         
        //        cmd.Parameters.AddWithValue("@type", cboType.SelectedItem.ToString());
        //        cmd.Parameters.AddWithValue("@source", cboSource.SelectedItem.ToString());
        //        cmd.Parameters.AddWithValue("@process", cboProcess.SelectedItem.ToString());
        //        cmd.Parameters.AddWithValue("@person", txtSumbitter.Text);
        //        cmd.Parameters.AddWithValue("@brief", txtDescription.Text);
        //        cmd.Parameters.AddWithValue("@assigned", txtAssignedTo.Text);
        //        cmd.Parameters.AddWithValue("@dateAssigned", txtAssignedDate.Text);
        //        cmd.Parameters.AddWithValue("@dueDate", txtActualDueDate.Text);
        //        cmd.Parameters.AddWithValue("@completeDate", textBox1.Text);
           
        //        DateTime nothing = DateTime.MinValue;
        //        var noDate = nothing.ToString("dd-MM-yyyy");
        //        var daysLate = myAction.Day - myActualDueDate.Day;
        //        int daysLeft = myActualDueDate.Day - myAssigned.Day;

        //        if (myAction >= myActualDueDate)
        //        {
        //            string overdue = "Yes";
        //            cmd.Parameters.AddWithValue("@overdue", overdue);

        //            if (myAction.ToString("dd-MM-yyyy") == noDate)
        //            {
        //                cmd.Parameters.AddWithValue("@closed", "No");
        //                cmd.Parameters.AddWithValue("@daysLeft", (int)daysLeft);
        //                cmd.Parameters.AddWithValue("@daysLate", 0);
        //            }
        //            else
        //            {
        //                cmd.Parameters.AddWithValue("@closed", "Yes");
        //                cmd.Parameters.AddWithValue("@daysLeft", 0);
        //                cmd.Parameters.AddWithValue("@daysLate", (int)daysLate);
        //            }
        //        }
        //        else
        //        {
        //            string overdue = "No";
        //            cmd.Parameters.AddWithValue("@overdue", overdue);

        //            if (myAction.ToString("dd-MM-yyyy") == noDate)
        //            {
        //                cmd.Parameters.AddWithValue("@closed", "No");
        //                cmd.Parameters.AddWithValue("@daysLeft", (int)daysLeft);
        //                cmd.Parameters.AddWithValue("@daysLate", 0);
        //            }
        //            else
        //            {
        //                cmd.Parameters.AddWithValue("@closed", "Yes");
        //                cmd.Parameters.AddWithValue("@daysLeft", 0);
        //                cmd.Parameters.AddWithValue("@daysLate", (int)daysLate);
        //            }
        //        }

        //        cmd.Parameters.AddWithValue("@car", txtCar.Text);
        //        cmd.ExecuteNonQuery();

        //        con.Close();
           
        //        MessageBox.Show("Complete");
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            EditCar();

            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";

            try { 

                using (var client = new SftpClient(host, port, username, password))
                {


                    if (File.Exists(@"C:\Reader\Loader.xlsx"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                        Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                        Worksheet sheet = work.Worksheets[1];

                        client.Connect();


                        if (client.IsConnected)
                        {
                            //MessageBox.Show("I'm connected to the client");  

                            string result = new DirectoryInfo(path).Parent.Name;

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                            }


                            using (var fileStream = new FileStream(path, FileMode.Open))
                            {

                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                if (client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                                {
                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }

                                MessageBox.Show("Changes Saved");
                            }

                        }
                        else
                        {
                            MessageBox.Show("I couldn't connect");
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        public void ShowCar()
        {
            try
            {
              
                
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];

                for (int i = 2; i < sheet.Cells.Rows.Count; i++)
                {
                    if (listBox1.SelectedItem.ToString().Contains(sheet.Cells[i, 1].Value.ToString()))
                    {
                        if (sheet.Cells[i, 0].Value.ToString() != null)
                        {
                            txtCar.Text = sheet.Cells[i, 0].Value.ToString();

                            cboType.SelectedItem = sheet.Cells[i, 2].Value.ToString();

                            cboSource.SelectedItem = sheet.Cells[i, 3].Value.ToString();
                            cboProcess.SelectedItem = sheet.Cells[i, 4].Value.ToString();
                            txtSumbitter.Text = sheet.Cells[i, 5].Value.ToString();
                            txtDescription.Text = sheet.Cells[i, 6].Value.ToString();
                            txtAssignedTo.Text = sheet.Cells[i, 7].Value.ToString();
                            datAssigned.Value = sheet.Cells[i, 8].DateTimeValue;
                            datDueDate.Value = sheet.Cells[i, 9].DateTimeValue;

                            if (sheet.Cells[i, 11].DateTimeValue != null)
                            {
                                datActionComplete.Value = sheet.Cells[i, 11].DateTimeValue;
                            }                         
                        }
                       
                        break;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("This entry has no action complete date. Feel free to put one here", "No Action Complete Date Detected.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               
            }

        }

        private void txtCar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (listBox1.SelectedItem.ToString().Contains("CAR") || listBox1.SelectedItem.ToString().Contains("Date_Submitted") || listBox1.SelectedItem.ToString().Contains("Name"))
            {
                txtCar.Text = "";
                txtCar.Enabled = false;

                txtAssignedTo.Text = "";
                txtAssignedTo.Enabled = false;

                txtDescription.Text = "";
                txtDescription.Enabled = false;

                txtSumbitter.Text = "";
                txtSumbitter.Enabled = false;

                cboProcess.Enabled = false;
                cboSource.Enabled = false;
                cboType.Enabled = false;
           
            }
            else
            {
                
                txtCar.Enabled = true;
        
                txtAssignedTo.Enabled = true;
               
                txtDescription.Enabled = true;    
                txtSumbitter.Enabled = true;
                cboProcess.Enabled = true;
                cboSource.Enabled = true;
                cboType.Enabled = true;
  
            }
            }
            catch (Exception)
            {

                MessageBox.Show("You must pick an entry that's been made", "Cannot Add Entries", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                

                Process.Start(path);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            string text = File.ReadAllText(@"C:\Management\Management System\ID_46664\Process.txt");


            if(checkBox1.Checked == false)
            {
                if (text == string.Empty)
                {
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Process.txt", "Unchecked");
                }
                else
                {
                    text = text.Replace("Checked", "Unchecked");
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Process.txt", text);
                    MessageBox.Show("You will no longer get notifications of late dates of CAR entries", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
              
            }
            else
            {
                if (text == string.Empty)
                {
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Process.txt", "Checked");
                    MessageBox.Show("From now on you will be notified about late dates whenever IMS Pulse is turned on. You can Cancel this feature by coming here again, TICK and UNTICK this box to stop notifications", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    text = text.Replace("Unchecked", "Checked");
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Process.txt", text);
                    MessageBox.Show("From now on you will be notified about late dates whenever IMS Pulse is turned on. You can Cancel this feature by coming here again, TICK and UNTICK this box to stop notifications", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            string text = File.ReadAllText(@"C:\Management\Management System\ID_46664\Mula.txt");

            if (checkBox2.Checked == false)
            {
                if (text == string.Empty)
                {
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Mula.txt", "Unchecked");
                }
                else
                {
                    text = text.Replace("Checked", "Unchecked");
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Mula.txt", text);
                    MessageBox.Show("You will no longer get notifications of late dates of CAR entries", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                if (text == string.Empty)
                {
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Mula.txt", "Checked");
                    MessageBox.Show("From now on you will be notified about late dates whenever IMS Pulse is turned on. You can Cancel this feature by coming here again, TICK and UNTICK this box to stop notifications", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    text = text.Replace("Unchecked", "Checked");
                    File.WriteAllText(@"C:\Management\Management System\ID_46664\Mula.txt", text);
                    MessageBox.Show("From now on you will be notified about late dates whenever IMS Pulse is turned on. You can Cancel this feature by coming here again, TICK and UNTICK this box to stop notifications", "Change Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ShowCar();
        }
    }
}
