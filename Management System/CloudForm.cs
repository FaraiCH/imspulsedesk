﻿using Aspose.Cells;
using Aspose.Words;
using DevExpress.XtraBars;
using DevExpress.XtraSplashScreen;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Menu;
using DevExpress.XtraTreeList.Nodes;
using DocumentManager;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheRelease;

namespace Management_System
{
    public partial class CloudForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        Timer t;

        FluentSplashScreenOptions op = new FluentSplashScreenOptions();
        public CloudForm()
        {
            InitializeComponent();
        }

        private void CloudForm_Load(object sender, EventArgs e)
        {

            DialogResult many = MessageBox.Show("Do you want to update the documents on your PC from the Cloud Server? If you already have updated documents, it is recommended that you choose No.", "Update Documents", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (many == DialogResult.Yes)
            {

                DialogResult canny = MessageBox.Show("Do you want to Upload? If Upload, select yes. For downloading files from the Cloud, select no.", "Upload Or Download?", MessageBoxButtons.YesNo,  MessageBoxIcon.Question);

                if(canny == DialogResult.Yes)
                {
                    var host = "imspulse.com";
                    var port = 22;
                    var username = "farai";
                    var password = "@Paradice1";



                    using (var client = new SftpClient(host, port, username, password))
                    {


                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {
                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                            Worksheet sheet = work.Worksheets[1];

                            client.Connect();

                    
                            if (client.IsConnected)
                            {
                                MessageBox.Show("I'm connected to the client");

                                op.Title = "IMS Pulse Cloud" + "\u2122";
                                op.Subtitle = "Your Cloud Solution";
                                op.RightFooter = "Uploading Documents. This Can Take Several Minutes...";
                                op.LeftFooter = "Copyright © 2000 - 2021|" + "IMS Pulse" + Environment.NewLine + "All Rights reserved.";
                                op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                                op.OpacityColor = Color.Black;
                                op.Opacity = 50;
                                op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";
                                
                                DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                                    op,
                                    parentForm: this,
                                    useFadeIn: true,
                                    useFadeOut: true
                                );

                                string primeFolder = @"MC QMS\QMS";
                                string primeFolder2 = @"MC QMS\Operations";
                                string primeFolder3 = @"MC QMS\Human Resources";

                                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
                                string[] fileArray2 = Directory.GetFiles(ServerPath.PathLocation() + primeFolder2 + @"\", "*", System.IO.SearchOption.AllDirectories);
                                string[] fileArray3 = Directory.GetFiles(ServerPath.PathLocation() + primeFolder3 + @"\", "*", System.IO.SearchOption.AllDirectories);


                                for (int i = 0; i < fileArray.Length; i++)
                                {
                                    if (fileArray[i].Contains("FRM") || fileArray[i].Contains("Form") || fileArray[i].Contains("FR") || fileArray[i].Contains("CKL") ||
                                       fileArray[i].Contains("CKL") || fileArray[i].Contains("Checklist") || fileArray[i].Contains("REG") || fileArray[i].Contains("Register"))
                                    {
                                        string result = new DirectoryInfo(fileArray[i]).Parent.Name;

                                        if(!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result))
                                        {
                                            client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result);
                                        }
                                       

                                        using (var fileStream = new FileStream(fileArray[i], FileMode.Open))
                                        {

                                            client.BufferSize = 4 * 1024; // bypass Payload error large files
                                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result + "/" + Path.GetFileName(fileArray[i])))
                                            {
                                                client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result + "/");
                                                client.UploadFile(fileStream, Path.GetFileName(fileArray[i]));

                                            }
                                        }
                                    }
                              

                                }
                                for (int i = 0; i < fileArray2.Length; i++)
                                {

                                        string result = new DirectoryInfo(fileArray2[i]).Parent.Name;
                                        
                                        if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Operations/" + result))
                                        {
                                            client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Operations/" + result);
                                        }

                                        using (var fileStream = new FileStream(fileArray2[i], FileMode.Open))
                                        {

                                            client.BufferSize = 4 * 1024; // bypass Payload error large files

                                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations/" + result + "/" + Path.GetFileName(fileArray2[i])))
                                            {
                                                client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Operations/" + result + "/");
                                                client.UploadFile(fileStream, Path.GetFileName(fileArray2[i]));
                                            }

                                        }


                                }

                                for (int i = 0; i < fileArray3.Length; i++)
                                {

                                        string result = new DirectoryInfo(fileArray3[i]).Parent.Name;

                                        if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result))
                                        {
                                            client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result);
                                        }

                                        using (var fileStream = new FileStream(fileArray3[i], FileMode.Open))
                                        {

                                            client.BufferSize = 4 * 1024; // bypass Payload error large files

                                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources/" + result + "/" + Path.GetFileName(fileArray3[i])))
                                            {
                                                client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result + "/");
                                                client.UploadFile(fileStream, Path.GetFileName(fileArray3[i]));
                                            }

                                        }

                                }
                                for (int i = 0; i < fileArray3.Length; i++)
                                {

                                        string result = new DirectoryInfo(fileArray3[i]).Parent.Name;

                                        if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result))
                                        {
                                            client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result);
                                        }

                                        using (var fileStream = new FileStream(fileArray3[i], FileMode.Open))
                                        {

                                            client.BufferSize = 4 * 1024; // bypass Payload error large files

                                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources/" + result + "/" + Path.GetFileName(fileArray3[i])))
                                            {
                                                client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + result + "/");
                                                client.UploadFile(fileStream, Path.GetFileName(fileArray3[i]));
                                            }

                                        }

                                }

                                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();


                            }
                            else
                            {
                                MessageBox.Show("I couldn't connect");
                            }

                        }
                    }
                }
                else
                {
                    var host = "imspulse.com";
                    var port = 22;
                    var username = "farai";
                    var password = "@Paradice1";

                

                    using (var client = new SftpClient(host, port, username, password))
                    {
                        string directory = "";

                        client.Connect();
                        if (client.IsConnected)
                        {

                       

                            if (File.Exists(@"C:\Reader\Loader.xlsx"))
                            {
                                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                                Worksheet sheet = work.Worksheets[1];
                              

                                op.Title = "IMS Pulse Cloud" + "\u2122";
                                op.Subtitle = "Your Cloud Solution";
                                op.RightFooter = "Downloading Documents. This Can Take Several Minutes...";
                                op.LeftFooter = "Copyright © 2000 - " + DateTime.Now.Date + " Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
                                op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                                op.OpacityColor = Color.Black;
                                op.Opacity = 50;
                                op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                                DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                                    op,
                                    parentForm: this,
                                    useFadeIn: true,
                                    useFadeOut: true
                                );

                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directory = item.Name;

                                        if(!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/QMS/" + directory))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/QMS/");

                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/QMS/" + directory);
                                          
                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + directory))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {

                                            using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/QMS/" + directory + "/" + itemdoc.Name))
                                            {
                                                client.DownloadFile(itemdoc.FullName, fileStream);
                                            }

                                        }
                                    }
                                        
                                }


                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directory = item.Name;

                                        if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Resources/" + directory))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Resources");
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Resources/" + directory);
                                           
                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Resources/" + directory))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {

                                            using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Resources/" + directory + "/" + itemdoc.Name))
                                            {
                                                client.DownloadFile(itemdoc.FullName, fileStream);
                                            }

                                        }
                                    }

                                }

                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Operations/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directory = item.Name;

                                        if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Operations/" + directory))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Operations");
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Operations/" + directory);

                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Operations/" + directory))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {

                                            using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/Operations/" + directory + "/" + itemdoc.Name))
                                            {
                                                client.DownloadFile(itemdoc.FullName, fileStream);
                                            }

                                        }
                                    }

                                }

                                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                            }
                        }
                    }


                    //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                    //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim();
                }

            }
            Workbook cloudbook = new Workbook();
            cloudbook.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\Cloud\" + "Record.xlsx");

            LoadList();
        }

        private void T_Tick(object sender, EventArgs e)
        {

            t.Stop();
            //Close the splashscreen
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
        }

        public void LoadList()
        {

            t = new Timer();
            t.Interval = 3000;
            t.Tick += T_Tick; ;
            t.Start();

            op.Title = "IMS Pulse Cloud" + "\u2122";
            op.Subtitle = "Your Cloud Solution";
            op.RightFooter = "Loading and Updating Documents...";
            op.LeftFooter = "Copyright © 2000 - " + DateTime.Now.Date + " | Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
            op.LoadingIndicatorType = FluentLoadingIndicatorType.Ring;
            op.OpacityColor = Color.DodgerBlue;
            op.Opacity = 30;
            op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

            DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                op,
                parentForm: this,
                useFadeIn: true,
                useFadeOut: true
            );

            treeList1.Dock = DockStyle.None;
            treeList2.Dock = DockStyle.None;
            //Specify the fields that arrange underlying data as a hierarchy.
            treeList1.KeyFieldName = "ID";
            treeList2.KeyFieldName = "ID";
            treeList3.KeyFieldName = "ID";
            treeList1.ParentFieldName = "RegionID";
            treeList2.ParentFieldName = "RegionID";
            treeList3.ParentFieldName = "RegionID";
            //Allow the treelist to create columns bound to the fields the KeyFieldName and ParentFieldName properties specify.
            treeList1.OptionsBehavior.PopulateServiceColumns = true;
            treeList2.OptionsBehavior.PopulateServiceColumns = true;
            treeList3.OptionsBehavior.PopulateServiceColumns = true;
            //Specify the data source.
            treeList1.DataSource = CloudDataGenerator.CreateData("MC QMS");
            treeList2.DataSource = CloudDataGenerator.CreateDataHR();
            treeList3.DataSource = CloudDataGenerator.CreateDataOperation();
            //The treelist automatically creates columns for the public fields found in the data source. 
            //You do not need to call the TreeList.PopulateColumns method unless the treeList1.OptionsBehavior.AutoPopulateColumns option is disabled.

            //Change the row height.
            treeList1.RowHeight = 23;
            treeList2.RowHeight = 23;
            treeList3.RowHeight = 23;

            //Access the automatically created columns.
            TreeListColumn colRegion = treeList1.Columns["Document"];
            TreeListColumn colRegio2 = treeList2.Columns["Document"];
            TreeListColumn colRegio3 = treeList3.Columns["Document"];


            //Hide the key columns. An end-user can access them from the Customization Form.
            treeList1.Columns[treeList1.KeyFieldName].Visible = false;
            treeList2.Columns[treeList1.KeyFieldName].Visible = false;
            treeList3.Columns[treeList1.KeyFieldName].Visible = false;
            treeList1.Columns[treeList1.ParentFieldName].Visible = false;
            treeList2.Columns[treeList1.ParentFieldName].Visible = false;
            treeList3.Columns[treeList1.ParentFieldName].Visible = false;


            //Make the Region column read-only.
            colRegion.OptionsColumn.ReadOnly = true;
            colRegio2.OptionsColumn.ReadOnly = true;
            colRegio3.OptionsColumn.ReadOnly = true;

            //Sort data against the Region column
            colRegion.SortIndex = 0;
            colRegio2.SortIndex = 0;
            colRegio3.SortIndex = 0;

            //Do not stretch columns to the treelist width.



            //Calculate the optimal column widths after the treelist is shown.
            this.BeginInvoke(new MethodInvoker(delegate
            {
                treeList1.BestFitColumns();
            }));

            treeList1.StateImageList = imageCollection1;

            treeList2.StateImageList = imageCollection1;
            treeList3.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList1.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 7;

                }
                else
                {
                    node.StateImageIndex = 7;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

            foreach (TreeListNode node in treeList2.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 8;

                }
                else
                {
                    node.StateImageIndex = 8;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            foreach (TreeListNode node in treeList3.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 9;

                }
                else
                {
                    node.StateImageIndex = 9;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void tabNavigationPage2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void treeList1_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(treeList1, "QMS");
        }

        private void treeList2_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(treeList2, "Resources");
        }

        private void treeList3_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(treeList3, "Operations");
        }

        private void upload_ItemClick(object sender, EventArgs e)
        {
            saveUpload(treeList1, "QMS");
            saveUpload(treeList2, "Resources");
            saveUpload(treeList3, "Operations");

        }

        public void saveUpload(TreeList treeList, string folder)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;

            string first = string.Empty;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";


            if (selectedNodes[0].HasChildren == false)
            {

                using (var client = new SftpClient(host, port, username, password))
                {

                    if (File.Exists(@"C:\Reader\Loader.xlsx"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                        Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                        Worksheet sheet = work.Worksheets[1];

                        client.Connect();

                        if (client.IsConnected)
                        {
                            MessageBox.Show("I'm connected to the client");

                            //op.Title = "IMS Pulse Cloud" + "\u2122";
                            //op.Subtitle = "Your Cloud Solution";
                            //op.RightFooter = "Uploading Documents. This Can Take Several Minutes...";
                            //op.LeftFooter = "Copyright © 2000 - 2021|" + "IMS Pulse" + Environment.NewLine + "All Rights reserved.";
                            //op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                            //op.OpacityColor = Color.Black;
                            //op.Opacity = 50;
                            //op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                            //DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                            //    op,
                            //    parentForm: this,
                            //    useFadeIn: true,
                            //    useFadeOut: true
                            //);

                            string primeFolder = @"MC QMS\Cloud\" + folder;

                            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                            for (int i = 0; i < fileArray.Length; i++)
                            {
                                if (fileArray[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                {
                                  
                                        string result = new DirectoryInfo(fileArray[i]).Parent.Name;
                                  
                                        if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result))
                                        {
                                            client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result);
                                        }


                                        using (var fileStream = new FileStream(fileArray[i], FileMode.Open))
                                        {

                                            client.BufferSize = 4 * 1024; // bypass Payload error large files
                                      
                                            client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/QMS/" + result + "/");
                                            client.UploadFile(fileStream, Path.GetFileName(fileArray[i]));
                                            MessageBox.Show("Completed");
       
                                        }

                                    break;
                                }
                            }

                            //DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();


                        }

                        //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                        //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                    }
                }
            }
        }
    

        public void treelistDoubleClick(TreeList treeList, string folder)
        {

            TreeListMultiSelection selectedNodes = treeList.Selection;
       
            string first = string.Empty;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";


                if (selectedNodes[0].HasChildren == false)
                {

                        using (var client = new SftpClient(host, port, username, password))
                        {
                            string directory = "";

                            client.Connect();
                            if (client.IsConnected)
                            {

                                if (File.Exists(@"C:\Reader\Loader.xlsx"))
                                {
                                    Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                                    Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                                    Worksheet sheet = work.Worksheets[1];

                                    foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/" + folder +  @"/"))
                                    {
                                        if (item.IsDirectory)
                                        {
                                            directory = item.Name;

                                            if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/" + folder + "/" + directory))
                                            {
                                                Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/" + folder + "/" + directory);
                                            }
                                        }

                                        foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/"+ folder + "/" + directory))
                                        {
                                            if (!itemdoc.IsDirectory)
                                            {
                                                if(itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                                {
                                                    using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/" + folder + "/" + directory + "/" + itemdoc.Name))
                                                    {
                                                        client.DownloadFile(itemdoc.FullName, fileStream);
                                                       
                                                    }

                                                    Process.Start(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + "/Cloud/" + folder + "/" + directory + "/" + itemdoc.Name);
                                                    Workbook cloudbooker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\Cloud\" + "Record.xlsx");
                                                    Worksheet cloudsheet = cloudbooker.Worksheets[0];
                                                    
                                                    if(cloudsheet.Cells.MaxDataRow == 0)
                                                    {
                                                        cloudsheet.Cells[1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[1, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[1, 2].PutValue(folder);
                                                    }
                                                    else
                                                    {
                                                        
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow + 1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 2].PutValue(folder);
                                                    }
                                                  

                                                    
                                                    //cloudsheet.Cells["A" + row].PutValue(itemdoc.Name);

                                                    cloudbooker.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\Cloud\" + "Record.xlsx");

                                                    break;
                                                }

                                               
                                            }
                                        }

                                    }

                                }
                            }
                        }

                            //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                            //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                }

            }

        private void treeList1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu is TreeListNodeMenu)
            {
                treeList1.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                TreeListMultiSelection selectedNodes = treeList1.Selection;
                e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Upload Document", upload_ItemClick));

            }
        }

        public void saveFiles()
        {
            MessageBox.Show("Saving Your work to cloud");
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";

            //Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            //Workbook work = new Workbook("C:/Reader/Loader.xlsx", getum);
            //Worksheet sheet = work.Worksheets[0];

            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                Workbook worker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\Cloud\" + "Record.xlsx");
                Worksheet files = worker.Worksheets[0];


                for (int i = 0; i <= files.Cells.MaxDataRow; i++)
                {
                    using (var fileStream = new FileStream(Application.StartupPath + "/IMS Pulse/MC QMS/Cloud/" + files.Cells[i, 2].StringValue + "/" + files.Cells[i, 1].StringValue + "/" + files.Cells[i, 0].StringValue, FileMode.Open))
                    {

                        client.BufferSize = 4 * 1024; // bypass Payload error large files

                        client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + "2BLN" + @"/" + files.Cells[i, 2].StringValue + "/" + files.Cells[i, 1].StringValue + "/");
                        client.UploadFile(fileStream, Path.GetFileName(files.Cells[i, 0].StringValue));


                    }
                }
            }
            MessageBox.Show("complete");
        }
        private void CloudForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveFiles();
        }

        private void CloudForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
           

         


        }

        private void treeList1_FocusedNodeChanged(object sender, FocusedNodeChangedEventArgs e)
        {

        }
    }
}


    public class CloudData
    {
        static int UniqueID = 37;
        public CloudData()
        {
            ID = UniqueID++;
        }
        public CloudData(int id, int regionId, string document)
        {
            ID = id;
            RegionID = regionId;
            Document = document;

        }
        public int ID { get; set; }
        public int RegionID { get; set; }
        public string Document { get; set; }

    }


    public class CloudDataGenerator
    {
        /// <summary>
        /// This will be the new Document Master for the TreeList
        /// </summary>
        /// <returns></returns>
        public static List<CloudData> CreateData(string primFolder)
        {
         

            Random rnd = new Random();

            List<CloudData> sales = new List<CloudData>();
       

        try
        {
            int hasrows = 0; 
            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                           

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/" + directoryitem.Name))
                            {
                                
                                if (!item.IsDirectory)
                                {
                                    if (hasrows == 0)
                                    {
                                        var dbCon = DBConnection.Instance();
                                        dbCon.Server = "92.205.25.31";
                                        dbCon.DatabaseName = "imspulse";
                                        dbCon.UserName = "manny";
                                        dbCon.Password = "@Paradice1";


                                        string[] optionsForms = { };

                                        if (dbCon.IsConnect())
                                        {
                                            //Get all users and compare that user to the login
                                            string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 1 AND company_name = '" + sheet.Cells["A1"].StringValue + "';";
                                            var cmd = new MySqlCommand(query, dbCon.Connection);
                                            var reader = cmd.ExecuteReader();

                                            if (reader.HasRows)
                                            {
                                              
                                                while (reader.Read())
                                                {
                                                    string codes = reader.GetString(1);

                                                    if (item.Name.Contains(codes))
                                                    {

                                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                                        count++;
                                                    }
                                                }
                                                reader.Close();
                                            }
                                            else
                                            {
                                                reader.Close();
                                                hasrows = 1;
                                            }


                                        }                                  

                                    }
                                    else
                                    {
                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                        count++;
                                    }
                                }

                            }

                        }

                        directorycount++;


                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message);
            return sales;
        }

        }
        public static List<CloudData> CreateDataHR()
        {
            string primeFolder = @"MC QMS\Resources";
            Random rnd = new Random();

            List<CloudData> sales = new List<CloudData>();

        try
        {
            int hasrows = 0;
            int count = 100001;
            var host = "imspulse.com";
            int directorycount = 0;
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {
                                    if (hasrows == 0)
                                    {
                                        var dbCon = DBConnection.Instance();
                                        dbCon.Server = "92.205.25.31";
                                        dbCon.DatabaseName = "imspulse";
                                        dbCon.UserName = "manny";
                                        dbCon.Password = "@Paradice1";


                                        string[] optionsForms = { };

                                        if (dbCon.IsConnect())
                                        {
                                            //Get all users and compare that user to the login
                                            string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 1 AND company_name = '" + sheet.Cells["A1"].StringValue + "';";
                                            var cmd = new MySqlCommand(query, dbCon.Connection);
                                            var reader = cmd.ExecuteReader();
                                            if (reader.HasRows)
                                            {
                                                while (reader.Read())
                                                {
                                                    string codes = reader.GetString(1);

                                                    if (item.Name.Contains(codes))
                                                    {
                                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                                        count++;
                                                    }
                                                }

                                                reader.Close();
                                            }
                                            else
                                            {
                                                reader.Close();
                                                hasrows = 1;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                        count++;
                                    }

                                }
                            }

                        }

                        directorycount++;

                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }
       
    }

        public static List<CloudData> CreateDataOperation()
        {
            string primeFolder = @"MC QMS\Operations";
            Random rnd = new Random();

            List<CloudData> sales = new List<CloudData>();

        try
        {
            int hasrows = 0;
            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];

            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {
                                    if (hasrows == 0)
                                    {

                                        var dbCon = DBConnection.Instance();
                                        dbCon.Server = "92.205.25.31";
                                        dbCon.DatabaseName = "imspulse";
                                        dbCon.UserName = "manny";
                                        dbCon.Password = "@Paradice1";


                                        string[] optionsForms = { };

                                        if (dbCon.IsConnect())
                                        {
                                            //Get all users and compare that user to the login
                                            string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 1 AND company_name = '"+ sheet.Cells["A1"].StringValue + "';";
                                            var cmd = new MySqlCommand(query, dbCon.Connection);
                                            var reader = cmd.ExecuteReader();
                                            if (reader.HasRows)
                                            {
                                                while (reader.Read())
                                                {
                                                    string codes = reader.GetString(1);

                                                    if (item.Name.Contains(codes))
                                                    {
                                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                                        count++;
                                                    }
                                                }
                                                reader.Close();
                                            }
                                            else
                                            {
                                                reader.Close();
                                                hasrows = 1;
                                            }

                                        }
                                    
                                    }
                                    else
                                    {
                                        sales.Add(new CloudData(count, directorycount, item.Name));
                                        count++;
                                    }
                                }
                            }
                        }

                        directorycount++;
                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }


        }

    public static List<CloudData> CreateDataPro(string primFolder)
    {

        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();
        try
        {

            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }



                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {


                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 2;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;


                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }



    }
    public static List<CloudData> CreateDataHRPro()
    {
        string primeFolder = @"MC QMS\Resources";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {

            int count = 100001;
            var host = "imspulse.com";
            int directorycount = 0;
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {

                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 2;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;

                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }
 
    }

    public static List<CloudData> CreateDataOperationPro()
    {
        string primeFolder = @"MC QMS\Operations";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();
        try
        {
            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];

            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {
                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 2;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }
                            }
                        }

                        directorycount++;
                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }


    }

    public static List<CloudData> CreateDataPol(string primFolder)
    {

        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();


        try
        {

            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }



                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {


                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 3;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;


                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }



    }
    public static List<CloudData> CreateDataHRPol()
    {
        string primeFolder = @"MC QMS\Resources";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {
          
            int count = 100001;
            var host = "imspulse.com";
            int directorycount = 0;
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {

                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 3;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;

                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }

    }

    public static List<CloudData> CreateDataOperationPol()
    {
        string primeFolder = @"MC QMS\Operations";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {
            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];

            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {

                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 3;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }
                            }
                        }

                        directorycount++;
                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }
  

    }

    public static List<CloudData> CreateDataPfd(string primFolder)
    {

        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {

            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }



                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "QMS" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {


                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 4;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;


                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }
 


    }
    public static List<CloudData> CreateDataHRPfd()
    {
        string primeFolder = @"MC QMS\Resources";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {
 
            int count = 100001;
            var host = "imspulse.com";
            int directorycount = 0;
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];


            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Resources" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {

                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 4;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }

                            }

                        }

                        directorycount++;

                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }

    }

    public static List<CloudData> CreateDataOperationPfd()
    {
        string primeFolder = @"MC QMS\Operations";
        Random rnd = new Random();

        List<CloudData> sales = new List<CloudData>();

        try
        {
            int count = 100001;
            int directorycount = 0;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[1];

            using (var client = new SftpClient(host, port, username, password))
            {
                client.Connect();
                if (client.IsConnected)
                {

                    foreach (var directoryitem in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/"))
                    {
                        if (!directoryitem.Name.Contains("."))
                        {

                            if (directoryitem.IsDirectory)
                            {
                                sales.Add(new CloudData(directorycount, -1, directoryitem.Name));

                            }

                            foreach (var item in client.ListDirectory("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + "Operations" + "/" + directoryitem.Name))
                            {

                                if (!item.IsDirectory)
                                {

                                    var dbCon = DBConnection.Instance();
                                    dbCon.Server = "92.205.25.31";
                                    dbCon.DatabaseName = "imspulse";
                                    dbCon.UserName = "manny";
                                    dbCon.Password = "@Paradice1";


                                    string[] optionsForms = { };

                                    if (dbCon.IsConnect())
                                    {
                                        //Get all users and compare that user to the login
                                        string query = "SELECT * FROM imspulse.farai_document_codes WHERE codetype_id = 4;";
                                        var cmd = new MySqlCommand(query, dbCon.Connection);
                                        var reader = cmd.ExecuteReader();
                                        while (reader.Read())
                                        {
                                            string codes = reader.GetString(1);

                                            if (item.Name.Contains(codes))
                                            {
                                                sales.Add(new CloudData(count, directorycount, item.Name));
                                                count++;
                                            }
                                        }
                                        reader.Close();

                                    }
                                }
                            }
                        }

                        directorycount++;
                    }
                }
                return sales;
            }
        }
        catch (Exception ex)
        {

            MessageBox.Show(ex.Message);
            return sales;
        }
 

    }
}
