﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentManager;
namespace Management_System
{
    public partial class DocumentCode : Form
    {
        public DocumentCode()
        {
            InitializeComponent();
        }

        private void DocumentCode_Load(object sender, EventArgs e)
        {
            cboStandard.Items.Add("SHE");
            cboStandard.Items.Add("IMS");
            cboStandard.Items.Add("EMS");
            cboStandard.Items.Add("SMS");
            cboStandard.Items.Add("QMS");


            cboCode.Items.Add("PRC");
            cboCode.Items.Add("POL");
            cboCode.Items.Add("APP");
            cboCode.Items.Add("AGG");
            cboCode.Items.Add("CHE");
            cboCode.Items.Add("CKL");
            cboCode.Items.Add("DAT");
            cboCode.Items.Add("MIX");
            cboCode.Items.Add("TAN");
            cboCode.Items.Add("REG");
            cboCode.Items.Add("REP");
            cboCode.Items.Add("FOB");
            cboCode.Items.Add("FRM");
            cboCode.Items.Add("SCP");
            cboCode.Items.Add("MAN");
            cboCode.Items.Add("MIN");
            cboCode.Items.Add("OBJ");
     
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {

            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Excel Document 2003|*.xls|Excel Document 2007|*.xlsx|Word Document 2003|*.doc|Word Document 2007|*.docx|Powerpoint Document 2003|*.ppt|Powerpoint Document 2007|*.pptx|Visio Document 2003|*.vsd|Visio Document 2007|*.vsdx";
            saveFileDialog1.FilterIndex = 1;


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                txtPath.Text = saveFileDialog1.FileName;

                textBox2.Text = Path.GetFileName(txtPath.Text);

              

            }
        }
    
        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {   
            try
            {
                if (cboStandard.Text == " " || cboCode.Text == " " || txtNum.Text == " " || textBox2.Text == " " || txtPath.Text == " " || txtNum.MaxLength < 2 && txtNum.MaxLength > 2)
                {

                    MessageBox.Show("Missing values. Might be the Number field. Number Format is: 000");
    
                }
                else
                {
                    string[] standard = { "SHE", "IMS", "EMS", "SMS", "QMS" };
                    string[] code = { "PRC", "POL", "APP", "AGG", "CHE", "CKL", "DAT", "MIX", "TAN", "REG", "REP", "FOB", "FRM", "SCP", "MAN", "MIN", "OBJ" };
                    string myStandard = Path.GetFileName(txtPath.Text).Substring(0, 3);
                    string myCode = Path.GetFileName(txtPath.Text).Substring(4, 3);
                    string myNum = Path.GetFileName(txtPath.Text).Substring(8, 3);

                    if(cboStandard.Items.Contains(myStandard))
                    {
                        
                        if (cboCode.Items.Contains(myCode))
                        {
                            File.Move(txtPath.Text, Path.GetDirectoryName(txtPath.Text) + @"\" + cboStandard.SelectedItem.ToString() + "-" + cboCode.SelectedItem.ToString() + "-" + txtNum.Text + "-" + textBox2.Text.Substring(12));
                            MessageBox.Show("Document code changed.");
                        }                          
                                                  
                    }
                    else
                    {
                        File.Move(txtPath.Text, Path.GetDirectoryName(txtPath.Text) + @"\" + cboStandard.SelectedItem.ToString() + "-" + cboCode.SelectedItem.ToString() + "-" + txtNum.Text + "-" + textBox2.Text);
                        MessageBox.Show("Code added to document.");
                            

                    }

                }      
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void cboStandard_SelectedIndexChanged(object sender, EventArgs e)
        {

         
        }

        private void cboCode_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void txtPath_TextChanged(object sender, EventArgs e)
        {
            string[] standard = { "SHE", "IMS", "EMS", "SMS", "QMS" };
            string[] code = { "PRC", "POL", "APP", "AGG", "CHE", "CKL", "DAT", "MIX", "TAN", "REG", "REP", "FOB", "FRM", "SCP", "MAN", "MIN", "OBJ" };
            string myStandard = Path.GetFileName(txtPath.Text).Substring(0, 3);
            string myCode = Path.GetFileName(txtPath.Text).Substring(4, 3);
            string myNum = Path.GetFileName(txtPath.Text).Substring(8, 3);

            for (int i = 0; i < standard.Length; i++)
            {
                if (cboStandard.Items.Contains(standard[i]))
                {
                    cboStandard.SelectedItem = myStandard;
                }
                else
                {
                    cboStandard.Text = "";
                }

            }

            for (int i = 0; i < code.Length; i++)
            {
                if (cboCode.Items.Contains(code[i]))
                {
                    cboCode.SelectedItem = myCode;
                    txtNum.Text = myNum;
                }
                else
                {
                    cboCode.Text = "";
                    txtNum.Text = "";
                }


            }
        }
    }
}
