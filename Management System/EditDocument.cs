﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentManager;
namespace Management_System
{
    public partial class EditDocument : Form
    {
        OleDbConnection cn = new OleDbConnection();
        OleDbCommand cmd = new OleDbCommand();
        DataTable dataTable = new DataTable();
        string sql = "";
        string filename = ServerPath.PathLocation() + @"Shared\NewDocuments.accdb";

        public EditDocument()
        {
            InitializeComponent();
        }

        private void EditDocument_Load(object sender, EventArgs e)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename));
                string sql = "Select DocumentPath FROM ExternalDocument";
                con.Open();
                cmd = new OleDbCommand(sql, con);
                OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                adaptor.Fill(dataTable);

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    lstItem.Items.Add(dataTable.Rows[i]["DocumentPath"].ToString());
                }

                con.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show("You are missing the following feature from your PC: ADO.NET. Therefore, you will not be able to save the documents that you open here.", "Missing");
            } 
  
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Excel Document 2003|*.xls|Excel Document 2007|*.xlsx|Word Document 2003|*.doc|Word Document 2007|*.docx|Powerpoint Document 2003|*.ppt|Powerpoint Document 2007|*.pptx|Visio Document 2003|*.vsd|Visio Document 2007|*.vsdx";
            saveFileDialog1.FilterIndex = 1;


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = saveFileDialog1.FileName;
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPath.Text == "")
                {
                    MessageBox.Show("No path selected", "Choose path");
                }
                else
                {
                    if (lstItem.Items.Contains(txtPath.Text) == false)
                    {
                        string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename);

                        sql = "INSERT INTO ExternalDocument(DocumentPath) values(@doc)";
                        cn = new OleDbConnection(conString);
                        cmd = new OleDbCommand(sql, cn);
                        cn.Open();
                        cmd.Parameters.AddWithValue("@doc", txtPath.Text);
                        cmd.ExecuteNonQuery();
                        cn.Close();
                    }


                    Process.Start(txtPath.Text);
                    lstItem.Refresh();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }           
        }

        private void lstItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstItem.Items == null)
                {
                    MessageBox.Show("No item in list");
                }
                else
                {
                    Process.Start(lstItem.SelectedItem.ToString());
                }
            }
            catch (Exception ex)
            {
                DialogResult dialog = MessageBox.Show(ex.Message + ". " + "Do you want to delete this file from the list?", "File Error", MessageBoxButtons.YesNo, MessageBoxIcon.Error);

                if (dialog == DialogResult.Yes)
                {
                    string conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename);

                    sql = "DELETE From ExternalDocument WHERE DocumentPath = @doc";
                    cn = new OleDbConnection(conString);
                    cmd = new OleDbCommand(sql, cn);
                    cn.Open();
                    cmd.Parameters.AddWithValue("@doc", lstItem.SelectedItem.ToString());
                    cmd.ExecuteNonQuery();
                    cn.Close();
                    lstItem.Refresh();
                    MessageBox.Show("Done");
                }
                
            }
          
            
        }
    }
}
