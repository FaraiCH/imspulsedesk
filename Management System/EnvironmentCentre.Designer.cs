﻿namespace Management_System
{
    partial class EnvironmentCentre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnvironmentCentre));
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsUserName = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsSystemName = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsDate = new System.Windows.Forms.ToolStripMenuItem();
            this.mEMBERSHIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.picBar = new System.Windows.Forms.PictureBox();
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonBottomToolBar1 = new C1.Win.C1Ribbon.RibbonBottomToolBar();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.ribbonTab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton2 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup4 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton4 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup5 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton5 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTab2 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup8 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton8 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup9 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton9 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup19 = new C1.Win.C1Ribbon.RibbonGroup();
            this.rcmbThemes = new C1.Win.C1Ribbon.RibbonComboBox();
            this.ribbonTab3 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup11 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton11 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup10 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton10 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup12 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton12 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup13 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton13 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup6 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton6 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup20 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton20 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup3 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton3 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTab4 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup15 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton15 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton19 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup16 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton16 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup18 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton18 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTopToolBar1 = new C1.Win.C1Ribbon.RibbonTopToolBar();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnForm = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnProcess = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnPol = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnProcedure = new Bunifu.Framework.UI.BunifuTileButton();
            this.emsForms1 = new Management_System.EMSForms();
            this.emsProcedure1 = new Management_System.EMSProcedure();
            this.emsPolicy1 = new Management_System.EMSPolicy();
            this.emsProcessFlow1 = new Management_System.EMSProcessFlow();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.bunifuGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(45, 20);
            this.toolStripMenuItem5.Text = "User:";
            // 
            // tlsUserName
            // 
            this.tlsUserName.Name = "tlsUserName";
            this.tlsUserName.Size = new System.Drawing.Size(93, 20);
            this.tlsUserName.Text = "[USER_NAME]";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem27.Text = "PC:";
            // 
            // tlsSystemName
            // 
            this.tlsSystemName.Name = "tlsSystemName";
            this.tlsSystemName.Size = new System.Drawing.Size(103, 20);
            this.tlsSystemName.Text = "[SYTEM_NAME]";
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem28.Text = "Date:";
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(100, 20);
            this.toolStripMenuItem29.Text = "[MEMBERSHIP]";
            // 
            // tlsDate
            // 
            this.tlsDate.Name = "tlsDate";
            this.tlsDate.Size = new System.Drawing.Size(55, 20);
            this.tlsDate.Text = "[DATE]";
            // 
            // mEMBERSHIPToolStripMenuItem
            // 
            this.mEMBERSHIPToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.mEMBERSHIPToolStripMenuItem.Name = "mEMBERSHIPToolStripMenuItem";
            this.mEMBERSHIPToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.mEMBERSHIPToolStripMenuItem.Text = "Membership:";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.tlsUserName,
            this.toolStripMenuItem27,
            this.tlsSystemName,
            this.toolStripMenuItem28,
            this.toolStripMenuItem29,
            this.tlsDate,
            this.mEMBERSHIPToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 728);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1274, 24);
            this.menuStrip2.TabIndex = 25;
            this.menuStrip2.Text = "BottomStrip";
            // 
            // picBar
            // 
            this.picBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picBar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picBar.Location = new System.Drawing.Point(0, 694);
            this.picBar.Name = "picBar";
            this.picBar.Size = new System.Drawing.Size(1274, 58);
            this.picBar.TabIndex = 22;
            this.picBar.TabStop = false;
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.AutoSizeElement = C1.Framework.AutoSizeElement.Width;
            this.c1Ribbon1.BottomToolBarHolder = this.ribbonBottomToolBar1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(1274, 146);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab1);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab2);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab3);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab4);
            this.c1ThemeController1.SetTheme(this.c1Ribbon1, "Office2016White");
            this.c1Ribbon1.TopToolBarHolder = this.ribbonTopToolBar1;
            this.c1Ribbon1.RibbonEvent += new C1.Win.C1Ribbon.RibbonEventHandler(this.c1Ribbon1_RibbonEvent);
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            // 
            // ribbonBottomToolBar1
            // 
            this.ribbonBottomToolBar1.Name = "ribbonBottomToolBar1";
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Groups.Add(this.ribbonGroup1);
            this.ribbonTab1.Groups.Add(this.ribbonGroup2);
            this.ribbonTab1.Groups.Add(this.ribbonGroup4);
            this.ribbonTab1.Groups.Add(this.ribbonGroup5);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Home";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.ribbonButton1);
            this.ribbonGroup1.Name = "ribbonGroup1";
            this.ribbonGroup1.Text = "New";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "New Document";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.ribbonButton2);
            this.ribbonGroup2.Name = "ribbonGroup2";
            this.ribbonGroup2.Text = "External";
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.LargeImage")));
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "Edit External Document";
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click);
            // 
            // ribbonGroup4
            // 
            this.ribbonGroup4.Items.Add(this.ribbonButton4);
            this.ribbonGroup4.Name = "ribbonGroup4";
            this.ribbonGroup4.Text = "Main Centre";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.LargeImage")));
            this.ribbonButton4.Name = "ribbonButton4";
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Text = "Go To Main Centre";
            this.ribbonButton4.Click += new System.EventHandler(this.ribbonButton4_Click);
            // 
            // ribbonGroup5
            // 
            this.ribbonGroup5.Items.Add(this.ribbonButton5);
            this.ribbonGroup5.Name = "ribbonGroup5";
            this.ribbonGroup5.Text = "Application";
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.LargeImage")));
            this.ribbonButton5.Name = "ribbonButton5";
            this.ribbonButton5.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.SmallImage")));
            this.ribbonButton5.Text = "Exit Application";
            this.ribbonButton5.Click += new System.EventHandler(this.ribbonButton5_Click);
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Groups.Add(this.ribbonGroup8);
            this.ribbonTab2.Groups.Add(this.ribbonGroup9);
            this.ribbonTab2.Groups.Add(this.ribbonGroup19);
            this.ribbonTab2.Name = "ribbonTab2";
            this.ribbonTab2.Text = "Edit";
            // 
            // ribbonGroup8
            // 
            this.ribbonGroup8.Items.Add(this.ribbonButton8);
            this.ribbonGroup8.Name = "ribbonGroup8";
            this.ribbonGroup8.Text = "Document Location";
            // 
            // ribbonButton8
            // 
            this.ribbonButton8.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.LargeImage")));
            this.ribbonButton8.Name = "ribbonButton8";
            this.ribbonButton8.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.SmallImage")));
            this.ribbonButton8.Text = "Change Document Location";
            this.ribbonButton8.Click += new System.EventHandler(this.ribbonButton8_Click);
            // 
            // ribbonGroup9
            // 
            this.ribbonGroup9.Items.Add(this.ribbonButton9);
            this.ribbonGroup9.Name = "ribbonGroup9";
            this.ribbonGroup9.Text = "Document Code";
            // 
            // ribbonButton9
            // 
            this.ribbonButton9.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton9.LargeImage")));
            this.ribbonButton9.Name = "ribbonButton9";
            this.ribbonButton9.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton9.SmallImage")));
            this.ribbonButton9.Text = "Change Document Code";
            this.ribbonButton9.Click += new System.EventHandler(this.ribbonButton9_Click);
            // 
            // ribbonGroup19
            // 
            this.ribbonGroup19.Items.Add(this.rcmbThemes);
            this.ribbonGroup19.Name = "ribbonGroup19";
            this.ribbonGroup19.Text = "Themes [Preview]";
            // 
            // rcmbThemes
            // 
            this.rcmbThemes.Label = "Themes";
            this.rcmbThemes.Name = "rcmbThemes";
            this.rcmbThemes.ChangeCommitted += new System.EventHandler(this.rcmbThemes_ChangeCommitted);
            this.rcmbThemes.DropDown += new System.EventHandler(this.rcmbThemes_DropDown);
            // 
            // ribbonTab3
            // 
            this.ribbonTab3.Groups.Add(this.ribbonGroup11);
            this.ribbonTab3.Groups.Add(this.ribbonGroup10);
            this.ribbonTab3.Groups.Add(this.ribbonGroup12);
            this.ribbonTab3.Groups.Add(this.ribbonGroup13);
            this.ribbonTab3.Groups.Add(this.ribbonGroup6);
            this.ribbonTab3.Groups.Add(this.ribbonGroup20);
            this.ribbonTab3.Groups.Add(this.ribbonGroup3);
            this.ribbonTab3.Name = "ribbonTab3";
            this.ribbonTab3.Text = "Environmental Management";
            // 
            // ribbonGroup11
            // 
            this.ribbonGroup11.Items.Add(this.ribbonButton11);
            this.ribbonGroup11.Name = "ribbonGroup11";
            this.ribbonGroup11.Text = "NCR and CAR";
            // 
            // ribbonButton11
            // 
            this.ribbonButton11.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton11.LargeImage")));
            this.ribbonButton11.Name = "ribbonButton11";
            this.ribbonButton11.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton11.SmallImage")));
            this.ribbonButton11.Text = "Create CAR/NCR";
            this.ribbonButton11.Click += new System.EventHandler(this.ribbonButton11_Click);
            // 
            // ribbonGroup10
            // 
            this.ribbonGroup10.Items.Add(this.ribbonButton10);
            this.ribbonGroup10.Name = "ribbonGroup10";
            this.ribbonGroup10.Text = "Advanced Corrective Action";
            // 
            // ribbonButton10
            // 
            this.ribbonButton10.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton10.LargeImage")));
            this.ribbonButton10.Name = "ribbonButton10";
            this.ribbonButton10.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton10.SmallImage")));
            this.ribbonButton10.Text = "Edit NCR/CAR";
            this.ribbonButton10.Click += new System.EventHandler(this.ribbonButton10_Click);
            // 
            // ribbonGroup12
            // 
            this.ribbonGroup12.Items.Add(this.ribbonButton12);
            this.ribbonGroup12.Name = "ribbonGroup12";
            this.ribbonGroup12.Text = "Risk";
            // 
            // ribbonButton12
            // 
            this.ribbonButton12.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton12.LargeImage")));
            this.ribbonButton12.Name = "ribbonButton12";
            this.ribbonButton12.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton12.SmallImage")));
            this.ribbonButton12.Text = "Create Risk Entry";
            this.ribbonButton12.Click += new System.EventHandler(this.ribbonButton12_Click);
            // 
            // ribbonGroup13
            // 
            this.ribbonGroup13.Items.Add(this.ribbonButton13);
            this.ribbonGroup13.Name = "ribbonGroup13";
            this.ribbonGroup13.Text = "Advanced Risk";
            // 
            // ribbonButton13
            // 
            this.ribbonButton13.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton13.LargeImage")));
            this.ribbonButton13.Name = "ribbonButton13";
            this.ribbonButton13.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton13.SmallImage")));
            this.ribbonButton13.Text = "Edit Risk Entry";
            this.ribbonButton13.Click += new System.EventHandler(this.ribbonButton13_Click);
            // 
            // ribbonGroup6
            // 
            this.ribbonGroup6.Items.Add(this.ribbonButton6);
            this.ribbonGroup6.Name = "ribbonGroup6";
            this.ribbonGroup6.Text = "Revision";
            // 
            // ribbonButton6
            // 
            this.ribbonButton6.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.LargeImage")));
            this.ribbonButton6.Name = "ribbonButton6";
            this.ribbonButton6.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton6.SmallImage")));
            this.ribbonButton6.Text = "IMS Pulse Revision Master";
            this.ribbonButton6.Click += new System.EventHandler(this.ribbonButton6_Click_1);
            // 
            // ribbonGroup20
            // 
            this.ribbonGroup20.Items.Add(this.ribbonButton20);
            this.ribbonGroup20.Name = "ribbonGroup20";
            this.ribbonGroup20.Text = "Users [Admin Only]";
            // 
            // ribbonButton20
            // 
            this.ribbonButton20.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton20.LargeImage")));
            this.ribbonButton20.Name = "ribbonButton20";
            this.ribbonButton20.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton20.SmallImage")));
            this.ribbonButton20.Text = "View System Users";
            this.ribbonButton20.Click += new System.EventHandler(this.ribbonButton20_Click);
            // 
            // ribbonGroup3
            // 
            this.ribbonGroup3.Items.Add(this.ribbonButton3);
            this.ribbonGroup3.Name = "ribbonGroup3";
            this.ribbonGroup3.Text = "Environment";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.LargeImage")));
            this.ribbonButton3.Name = "ribbonButton3";
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "Open Environmental Folder";
            this.ribbonButton3.Click += new System.EventHandler(this.ribbonButton3_Click_1);
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.Groups.Add(this.ribbonGroup15);
            this.ribbonTab4.Groups.Add(this.ribbonGroup16);
            this.ribbonTab4.Groups.Add(this.ribbonGroup18);
            this.ribbonTab4.Name = "ribbonTab4";
            this.ribbonTab4.Text = "Help";
            // 
            // ribbonGroup15
            // 
            this.ribbonGroup15.Items.Add(this.ribbonButton15);
            this.ribbonGroup15.Items.Add(this.ribbonButton19);
            this.ribbonGroup15.Name = "ribbonGroup15";
            this.ribbonGroup15.Text = "Contact";
            this.ribbonGroup15.DialogLauncherClick += new System.EventHandler(this.ribbonGroup15_DialogLauncherClick);
            // 
            // ribbonButton15
            // 
            this.ribbonButton15.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton15.LargeImage")));
            this.ribbonButton15.Name = "ribbonButton15";
            this.ribbonButton15.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton15.SmallImage")));
            this.ribbonButton15.Text = "Website";
            this.ribbonButton15.Click += new System.EventHandler(this.ribbonButton15_Click);
            // 
            // ribbonButton19
            // 
            this.ribbonButton19.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton19.LargeImage")));
            this.ribbonButton19.Name = "ribbonButton19";
            this.ribbonButton19.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton19.SmallImage")));
            this.ribbonButton19.Text = "Direct Email";
            this.ribbonButton19.Click += new System.EventHandler(this.ribbonButton19_Click);
            // 
            // ribbonGroup16
            // 
            this.ribbonGroup16.Items.Add(this.ribbonButton16);
            this.ribbonGroup16.Name = "ribbonGroup16";
            this.ribbonGroup16.Text = "Product";
            // 
            // ribbonButton16
            // 
            this.ribbonButton16.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton16.LargeImage")));
            this.ribbonButton16.Name = "ribbonButton16";
            this.ribbonButton16.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton16.SmallImage")));
            this.ribbonButton16.Text = "About Product";
            this.ribbonButton16.Click += new System.EventHandler(this.ribbonButton16_Click);
            // 
            // ribbonGroup18
            // 
            this.ribbonGroup18.Items.Add(this.ribbonButton18);
            this.ribbonGroup18.Name = "ribbonGroup18";
            this.ribbonGroup18.Text = "User Manual";
            // 
            // ribbonButton18
            // 
            this.ribbonButton18.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton18.LargeImage")));
            this.ribbonButton18.Name = "ribbonButton18";
            this.ribbonButton18.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton18.SmallImage")));
            this.ribbonButton18.Text = "Manual";
            // 
            // ribbonTopToolBar1
            // 
            this.ribbonTopToolBar1.Name = "ribbonTopToolBar1";
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuGradientPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.bunifuGradientPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuGradientPanel3.Controls.Add(this.label3);
            this.bunifuGradientPanel3.Controls.Add(this.label2);
            this.bunifuGradientPanel3.Controls.Add(this.label1);
            this.bunifuGradientPanel3.Controls.Add(this.pictureBox2);
            this.bunifuGradientPanel3.Controls.Add(this.emsForms1);
            this.bunifuGradientPanel3.Controls.Add(this.emsProcedure1);
            this.bunifuGradientPanel3.Controls.Add(this.emsPolicy1);
            this.bunifuGradientPanel3.Controls.Add(this.emsProcessFlow1);
            this.bunifuGradientPanel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(290, 152);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(984, 523);
            this.bunifuGradientPanel3.TabIndex = 34;
            this.c1ThemeController1.SetTheme(this.bunifuGradientPanel3, "(default)");
            this.bunifuGradientPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.bunifuGradientPanel3_Paint);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label3.Location = new System.Drawing.Point(253, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(588, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "3. Pick a document to either preview or open in Office.";
            this.c1ThemeController1.SetTheme(this.label3, "(default)");
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label2.Location = new System.Drawing.Point(253, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(535, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "2. If applicable, choose the relevant departments.";
            this.c1ThemeController1.SetTheme(this.label2, "(default)");
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label1.Location = new System.Drawing.Point(253, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(320, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "1. Choose one of the folders.";
            this.c1ThemeController1.SetTheme(this.label1, "(default)");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(980, 519);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.bunifuGradientPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel1.BackgroundImage")));
            this.bunifuGradientPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuGradientPanel1.Controls.Add(this.btnForm);
            this.bunifuGradientPanel1.Controls.Add(this.btnProcess);
            this.bunifuGradientPanel1.Controls.Add(this.btnPol);
            this.bunifuGradientPanel1.Controls.Add(this.btnProcedure);
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Transparent;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.WhiteSmoke;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 152);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(284, 523);
            this.bunifuGradientPanel1.TabIndex = 33;
            // 
            // btnForm
            // 
            this.btnForm.BackColor = System.Drawing.Color.SteelBlue;
            this.btnForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnForm.color = System.Drawing.Color.SteelBlue;
            this.btnForm.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnForm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnForm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnForm.ForeColor = System.Drawing.Color.White;
            this.btnForm.Image = ((System.Drawing.Image)(resources.GetObject("btnForm.Image")));
            this.btnForm.ImagePosition = 13;
            this.btnForm.ImageZoom = 30;
            this.btnForm.LabelPosition = 27;
            this.btnForm.LabelText = "FORMS";
            this.btnForm.Location = new System.Drawing.Point(72, 21);
            this.btnForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnForm.Name = "btnForm";
            this.btnForm.Size = new System.Drawing.Size(126, 96);
            this.btnForm.TabIndex = 0;
            this.btnForm.Click += new System.EventHandler(this.btnForm_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.SteelBlue;
            this.btnProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnProcess.color = System.Drawing.Color.SteelBlue;
            this.btnProcess.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnProcess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcess.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.ForeColor = System.Drawing.Color.Transparent;
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImagePosition = 13;
            this.btnProcess.ImageZoom = 30;
            this.btnProcess.LabelPosition = 27;
            this.btnProcess.LabelText = "PROCESS FLOW";
            this.btnProcess.Location = new System.Drawing.Point(72, 404);
            this.btnProcess.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(126, 96);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnPol
            // 
            this.btnPol.BackColor = System.Drawing.Color.SteelBlue;
            this.btnPol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnPol.color = System.Drawing.Color.SteelBlue;
            this.btnPol.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnPol.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPol.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPol.ForeColor = System.Drawing.Color.Transparent;
            this.btnPol.Image = ((System.Drawing.Image)(resources.GetObject("btnPol.Image")));
            this.btnPol.ImagePosition = 13;
            this.btnPol.ImageZoom = 30;
            this.btnPol.LabelPosition = 27;
            this.btnPol.LabelText = "POLICIES";
            this.btnPol.Location = new System.Drawing.Point(72, 276);
            this.btnPol.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPol.Name = "btnPol";
            this.btnPol.Size = new System.Drawing.Size(126, 96);
            this.btnPol.TabIndex = 2;
            this.btnPol.Click += new System.EventHandler(this.btnPol_Click);
            // 
            // btnProcedure
            // 
            this.btnProcedure.BackColor = System.Drawing.Color.SteelBlue;
            this.btnProcedure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnProcedure.color = System.Drawing.Color.SteelBlue;
            this.btnProcedure.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnProcedure.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcedure.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcedure.ForeColor = System.Drawing.Color.Transparent;
            this.btnProcedure.Image = ((System.Drawing.Image)(resources.GetObject("btnProcedure.Image")));
            this.btnProcedure.ImagePosition = 13;
            this.btnProcedure.ImageZoom = 30;
            this.btnProcedure.LabelPosition = 27;
            this.btnProcedure.LabelText = "PROCEDURES";
            this.btnProcedure.Location = new System.Drawing.Point(72, 147);
            this.btnProcedure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProcedure.Name = "btnProcedure";
            this.btnProcedure.Size = new System.Drawing.Size(126, 96);
            this.btnProcedure.TabIndex = 1;
            this.btnProcedure.Click += new System.EventHandler(this.btnProcedure_Click);
            // 
            // emsForms1
            // 
            this.emsForms1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emsForms1.BackColor = System.Drawing.Color.Transparent;
            this.emsForms1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("emsForms1.BackgroundImage")));
            this.emsForms1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.emsForms1.Location = new System.Drawing.Point(3, 3);
            this.emsForms1.Name = "emsForms1";
            this.emsForms1.Size = new System.Drawing.Size(976, 519);
            this.emsForms1.TabIndex = 5;
            // 
            // emsProcedure1
            // 
            this.emsProcedure1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emsProcedure1.BackColor = System.Drawing.Color.Transparent;
            this.emsProcedure1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("emsProcedure1.BackgroundImage")));
            this.emsProcedure1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.emsProcedure1.Location = new System.Drawing.Point(3, 3);
            this.emsProcedure1.Name = "emsProcedure1";
            this.emsProcedure1.Size = new System.Drawing.Size(976, 519);
            this.emsProcedure1.TabIndex = 6;
            // 
            // emsPolicy1
            // 
            this.emsPolicy1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emsPolicy1.BackColor = System.Drawing.Color.Transparent;
            this.emsPolicy1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("emsPolicy1.BackgroundImage")));
            this.emsPolicy1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.emsPolicy1.Location = new System.Drawing.Point(3, 3);
            this.emsPolicy1.Name = "emsPolicy1";
            this.emsPolicy1.Size = new System.Drawing.Size(976, 637);
            this.emsPolicy1.TabIndex = 7;
            // 
            // emsProcessFlow1
            // 
            this.emsProcessFlow1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.emsProcessFlow1.BackColor = System.Drawing.Color.Transparent;
            this.emsProcessFlow1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("emsProcessFlow1.BackgroundImage")));
            this.emsProcessFlow1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.emsProcessFlow1.Location = new System.Drawing.Point(3, 3);
            this.emsProcessFlow1.Name = "emsProcessFlow1";
            this.emsProcessFlow1.Size = new System.Drawing.Size(980, 631);
            this.emsProcessFlow1.TabIndex = 8;
            // 
            // EnvironmentCentre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1274, 752);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.c1Ribbon1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.picBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EnvironmentCentre";
            this.Text = "Environmental Centre";
            this.c1ThemeController1.SetTheme(this, "Office2016White");
            this.VisualStyleHolder = C1.Win.C1Ribbon.VisualStyle.Custom;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EnvironmentCentre_FormClosed);
            this.Load += new System.EventHandler(this.EnvironmentCentre_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuTextbox txtSearch;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem tlsUserName;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem tlsSystemName;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem tlsDate;
        private System.Windows.Forms.ToolStripMenuItem mEMBERSHIPToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.PictureBox picBar;
        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonBottomToolBar ribbonBottomToolBar1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
        private C1.Win.C1Ribbon.RibbonTopToolBar ribbonTopToolBar1;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup4;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup5;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab2;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab3;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab4;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup8;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup9;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup10;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup11;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup15;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup16;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup18;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton1;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton2;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton4;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton5;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup19;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton8;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton9;
        private C1.Win.C1Ribbon.RibbonComboBox rcmbThemes;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton10;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton11;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton15;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton16;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton18;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton19;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup20;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton20;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup12;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton12;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup13;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton13;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuTileButton btnForm;
        private Bunifu.Framework.UI.BunifuTileButton btnProcess;
        private Bunifu.Framework.UI.BunifuTileButton btnPol;
        private Bunifu.Framework.UI.BunifuTileButton btnProcedure;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private EMSForms emsForms1;
        private EMSProcedure emsProcedure1;
        private EMSPolicy emsPolicy1;
        private EMSProcessFlow emsProcessFlow1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup3;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton3;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup6;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton6;
    }
}