﻿using Aspose.Cells;
using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Management_System
{
    public partial class EnvironmentSection : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public EnvironmentSection()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            InitializeComponent();
        }

        private void EnvironmentSection_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            NewDocument newDocument = new NewDocument();
            newDocument.ShowDialog();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {

            EditDocument editDocument = new EditDocument();

            editDocument.ShowDialog();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainForm main = new MainForm();
            main.Show();
            this.Hide();
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            MainForm main = new MainForm();
            main.MovePanelToFront();
            main.Show();
            this.Hide();
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            LocationChanger locationChanger = new LocationChanger();
            locationChanger.ShowDialog();
        }

        private void barButtonItem12_ItemClick(object sender, ItemClickEventArgs e)
        {
            DocumentCode documentCode = new DocumentCode();
            documentCode.ShowDialog();
        }

        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }

        public void ValidateUsers()
        {

            if (isAdmin() == "admin")
            {
                UserLog userLog = new UserLog();
                userLog.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void EditRisk()
        {
            if (isAdmin() == "admin")
            {
                Advanced_Risk_Register register = new Advanced_Risk_Register();
                register.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void EditCorrective()
        {
            if (isAdmin() == "admin")
            {
                AdvancedCorrective advancedCorrective = new AdvancedCorrective();
                advancedCorrective.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use the NCR/CAR feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();
            nCRandCAR.ShowDialog();
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditCorrective();
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            RiskRegister riskRegister = new RiskRegister();
            riskRegister.ShowDialog();
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditRisk();
        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                RevisionSafety revisionSafety = new RevisionSafety();
                revisionSafety.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            ValidateUsers();
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            Process.Start("http://www.imspulse.com");
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            Email email = new Email();
            email.ShowDialog();
        }
    }
}