﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Management_System
{
    public partial class LocationChanger : Form
    {
        public LocationChanger()
        {
            InitializeComponent();
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Excel Document 2003|*.xls|Excel Document 2007|*.xlsx|Word Document 2003|*.doc|Word Document 2007|*.docx|Powerpoint Document 2003|*.ppt|Powerpoint Document 2007|*.pptx|Visio Document 2003|*.vsd|Visio Document 2007|*.vsdx";
            saveFileDialog1.FilterIndex = 1;


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = saveFileDialog1.FileName;
            }
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    txtLocation.Text = fbd.SelectedPath + @"\" + Path.GetFileName(txtPath.Text);
                }
            }
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPath.Text != "" && txtLocation.Text != "")
                {
                    File.Move(txtPath.Text, txtLocation.Text);
                    MessageBox.Show("Done");

                    if (File.Exists(txtLocation.Text))
                    {
                        Process.Start("explorer.exe", Path.GetDirectoryName(txtLocation.Text));
                    }
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          

        }
    }
}
