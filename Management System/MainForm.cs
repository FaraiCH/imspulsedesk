﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentManager;
using Aspose.Cells;
using Microsoft.VisualBasic.FileIO;
using System.Net;

namespace Management_System
{
    public partial class MainForm : Form
    {
        NetworkCredential networkCredential = new NetworkCredential("Cinfratec", "@Paradice1");

        Color colToFadeTo;
        int x = 50;

        Timer tFadeOut = new Timer();
        Timer tFadeOut2 = new Timer();
        public const int WM_MOUSEBUTTONDOW = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWindow, int Msg, int wParam, int iParam);

        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public MainForm()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);

            InitializeComponent();
            colToFadeTo = this.BackColor;
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to exit?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                Application.Exit();               
            }
        }

        public void MovePanelToFront()
        {
            pnlNames.Visible = true;
            pnlNew.Visible = false;

            pic1.SendToBack();
        }

        public void ReadProces()
        {
           
            string isChecked = File.ReadAllText(@"C:\Management\Management System\ID_46664\Process.txt");

            if (isChecked.Contains("Checked"))
            {
                MyNext myNext = new MyNext();
            }

        }

        public void CreateFeatures()
        {
            string processPrime = @"C:\Management";
            string processSub = @"C:\Management\Management System";

            string processThird = @"C:\Management\Management System\ID_46664";
            string processThirdSet = @"C:\Management\Management System\ID_52525";
            string processFourthSet = @"C:\Management\Management System\ID_98889";

            string filenameProcess = @"C:\Management\Management System\ID_46664\Process.txt";
            string filenameDuedates = @"C:\Management\Management System\ID_46664\Mula.txt";
            string filenameSettings = @"C:\Management\Management System\ID_52525\Settings.txt";
            string userLog = ServerPath.PathLocation() + @"Management\Management System\ID_98889";

            string Safety = ServerPath.PathLocation() + @"Safety Management";
            string Quality = ServerPath.PathLocation() + @"MC QMS";
            string Environemnet = ServerPath.PathLocation() + @"Environment Management";
            string assets = ServerPath.PathLocation() + @"Office Assets";
            string shared = ServerPath.PathLocation() + @"Shared";

    

            if (!Directory.Exists(processPrime))
            {
                Directory.CreateDirectory(processPrime);
            }

            if (!Directory.Exists(processSub))
            {
                Directory.CreateDirectory(processSub);
            }

            if (!Directory.Exists(processThird))
            {
                Directory.CreateDirectory(processThird);

            }

            if (!Directory.Exists(processThirdSet))
            {
                Directory.CreateDirectory(processThirdSet);
            }

            if (!Directory.Exists(processFourthSet))
            {
                Directory.CreateDirectory(processFourthSet);
            }

            if (!File.Exists(filenameProcess))
            {
                var pagan = File.Create(filenameProcess);
                pagan.Close();
            }
            if (!File.Exists(filenameDuedates))
            {
                File.Create(filenameDuedates);
            }

            if (!File.Exists(filenameSettings))
            {
                File.Create(filenameSettings);
            }

            if (!Directory.Exists(userLog))
            {
                FileSystem.CreateDirectory(userLog);
                var meh = File.Create(ServerPath.PathLocation() + @"Management\Management System\ID_98889\UserLog.txt");
                meh.Close();
            }

            if (!Directory.Exists(Quality))
            {
                FileSystem.CopyDirectory(@"C:\MC QMS", Quality);

            }

            if (!Directory.Exists(Safety))
            {
                FileSystem.CopyDirectory(@"C:\Safety Management", Safety);
            }

            if (!Directory.Exists(assets))
            {
                FileSystem.CopyDirectory(@"C:\Office Assets", assets);

            }
            if (!Directory.Exists(shared))
            {
                FileSystem.CopyDirectory(@"\Shared", shared);
            }
            if (!Directory.Exists(Environemnet))
            {

                FileSystem.CopyDirectory(@"C:\Environment Management", Environemnet);
            }
            if (!Directory.Exists(ServerPath.PathLocation() + @"Revision Master"))
            {
                FileSystem.CreateDirectory(ServerPath.PathLocation() + @"Revision Master");
            }

        }

        public void ValidateUser()
        {
            try
            {
                //Proper way to open a protected worksheet with password
                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                Worksheet sheet = work.Worksheets[0];

                string[] passwordBase = { "@Paradice1", "System@123", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000", sheet.Cells["E1"].StringValue};

      
                Subscribe subscribe = new Subscribe();
                string made = subscribe.Result();

                if (sheet.Cells["B1"].Value != null)
                {
                    string password = sheet.Cells["B1"].Value.ToString();
                  

                    if (password == passwordBase[0] || password == passwordBase[1] || password == passwordBase[2] ||
                        password == passwordBase[3] || password == passwordBase[4] || password == passwordBase[5])
                    {
                        label1.Text = "Admin";
                        MessageBox.Show("You have been logged in as the Admin. You have full priveledges. Enjoy!", "Logged in as 'Admin'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    else
                    {
                        label1.Text = "User";
                        MessageBox.Show("You have been logged into the system as a User. You do not have administrative access to edit any documents. " +
                            "You do not have permission to make CAR/NCR entries or make Risk entries. You may only read. To have admin rights, exit the application and log in with the correct password.", "Logged in as 'User'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }

                }
        
                work.Settings.Password = "@Paradice1";
                work.Save(@"C:\Reader\Loader.xlsx");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
       
        }


        private void MainForm_Load(object sender, EventArgs e)
        {

            ValidateUser();
            //DoRegister(); 
            pnlNames.Visible = true;
            pnlNew.Visible = false;
            pictureBox1.Visible = true;
            pictureBox2.Visible = false;
            pic1.Visible = true;


            //Lock or Unlock Features (Don't forget to delete documents if you lock)

            //bunifuImageButton1.Enabled = false;
            //btnEnvironmental.Enabled = false;


            //bunifuThinButton22.Visible = false;
            //bunifuThinButton24.Visible = false;

            CreateFeatures();
          

            tFadeOut2.Interval = 4500;

            tFadeOut2.Start();
            tFadeOut2.Tick += TFadeOut2_Tick;
            ReadProces();
        }

        private void TFadeOut2_Tick(object sender, EventArgs e)
        {
            pictureBox2.Visible = true;
        }

        private void tFadeOut_Tick(object sender, EventArgs e)
        {
           
            if(x == 102)
            {
                x = 50;

                tFadeOut.Stop();
                pic1.SendToBack();
              
                tFadeOut2.Start();
            }
            else
            {
                pic1.Image = Lighter(pic1.Image, x+2, colToFadeTo.R,
                              colToFadeTo.G, colToFadeTo.B);
            }
        }

      


        private Image Lighter(Image imgLight, int level, int nRed, int nGreen, int nBlue)
        {
            //convert image to graphics object
            Graphics graphics = Graphics.FromImage(imgLight);
            int conversion = (5 * (level - 50)); //calculate new alpha value
                                                 //create mask with blended alpha value and chosen color as pen 
            Pen pLight = new Pen(Color.FromArgb(conversion, nRed,
                                 nGreen, nBlue), imgLight.Width * 2);
            //apply created mask to graphics object
            graphics.DrawLine(pLight, -1, -1, imgLight.Width, imgLight.Height);
            //save created graphics object and modify image object by that
            graphics.Save();
            graphics.Dispose(); //dispose graphics object
            return imgLight; //return modified image
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            pnlNames.Visible = true;
            pnlNew.Visible = false;
       
            pic1.SendToBack();
          
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            pnlNew.Visible = true;
            pnlNames.Visible = false;

           
        
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_MOUSEBUTTONDOW, HT_CAPTION, 0);
            }
        }

        private void btnQuality_Click(object sender, EventArgs e)
        {
            try
            {
                QualitySection qualityCentre = new QualitySection();
                this.Hide();
                qualityCentre.Show();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {
            try
            {
                SafetySection safetyCentre = new SafetySection();
                this.Hide();
                safetyCentre.Show();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        
        }

        private void btnQuality_MouseHover(object sender, EventArgs e)
        {
          
        }

        private void bunifuImageButton1_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void btnEnvironmental_MouseHover(object sender, EventArgs e)
        {
          
        }

        private void bunifuImageButton1_MouseLeave(object sender, EventArgs e)
        {
          
        }

        private void btnQuality_MouseLeave(object sender, EventArgs e)
        {
          
        }

        private void btnEnvironmental_MouseLeave(object sender, EventArgs e)
        {
           
        }

        public void OpenFileSuite()
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Excel Document 2003|*.xls|Excel Document 2007|*.xlsx|Word Document 2003|*.doc|Word Document 2007|*.docx|Powerpoint Document 2003|*.ppt|Powerpoint Document 2007|*.pptx|Visio Document 2003|*.vsd|Visio Document 2007|*.vsdx";
            saveFileDialog1.FilterIndex = 1;
            

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtfile.Text = saveFileDialog1.FileName;
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            OpenFileSuite();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtfile.Text == string.Empty)
                {
                    MessageBox.Show("Please select any document on your computer to edit.", "Nothing Picked.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    Process.Start(txtfile.Text);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
            
        }

        private void txtfile_OnValueChanged(object sender, EventArgs e)
        {
          
        }

        private void btnEnvironmental_Click(object sender, EventArgs e)
        {
            try
            {
                EnvironmentSection environmentCentre = new EnvironmentSection();


                environmentCentre.Show();
                this.Hide();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
          
        }

        private void pic1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_MOUSEBUTTONDOW, HT_CAPTION, 0);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(e.CloseReason == CloseReason.UserClosing)
            {
                MyNext myNext = new MyNext();

                e.Cancel = true;
            }
        }

        private void btnSafety_Click(object sender, EventArgs e)
        {
            SafetyCentre safetyCentre = new SafetyCentre();
            safetyCentre.Show();
            this.Hide();
        }

        private void btnQualityMa_Click(object sender, EventArgs e)
        {
            QualitySection qualityCentre = new QualitySection();
            qualityCentre.Show();
            this.Hide();

        }

        private void txtEnvironment_Click(object sender, EventArgs e)
        {
            EnvironmentCentre environmentCentre = new EnvironmentCentre();
            environmentCentre.Show();
            this.Hide();
        }

        private void pic1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {
            SafetySection safetyCentre = new SafetySection();
            safetyCentre.Show();
            this.Hide();
        }

        private void bunifuThinButton23_Click(object sender, EventArgs e)
        {
            QualitySection qualityCentre = new QualitySection();
            qualityCentre.Show();
            this.Hide();
        }

        private void bunifuThinButton24_Click(object sender, EventArgs e)
        {
            EnvironmentSection qualityCentre = new EnvironmentSection();
            qualityCentre.Show();
            this.Hide();
        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
