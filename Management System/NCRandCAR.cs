﻿using DocumentManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XlApp = Microsoft.Office.Interop.Excel;
using Aspose.Cells;
using System.IO;
using Renci.SshNet;
using Aspose.Email;
using Aspose.Email.Clients.Smtp;
using Aspose.Email.Clients;
using System.Text.RegularExpressions;

namespace Management_System
{
    public partial class NCRandCAR : Form
    {
        string path = ServerPath.PathLocation() + @"Shared\CAR LOG.xlsx";

        public NCRandCAR()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));
            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            stream2.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            new Aspose.Email.License().SetLicense(stream2);
            InitializeComponent();
        }

        private void NCRandCAR_Load(object sender, EventArgs e)
        {
            try
            {
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";

                //try
                //{
                using (var client = new SftpClient(host, port, username, password))
                {

                    client.Connect();

                    if (client.IsConnected)
                    {

                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {


                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                            Workbook works = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                            Worksheet sheets = works.Worksheets[1];

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue);
                                
                            }

                            if(!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared");
                            }

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                            {
                                using (var fileStream = new FileStream(path, FileMode.Open))
                                {

                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }
                            }
                            else
                            {
                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["C1"].StringValue + @"/Shared/"))
                                {
                                    if (!item.IsDirectory)
                                    {
                                        using (Stream fileStreams = File.Create(ServerPath.PathLocation() + @"/Shared/" + item.Name))
                                        {
                                            client.DownloadFile(item.FullName, fileStreams);
                                        }

                                    }

                                }
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Unable to connect to the cloud. Please check your internet connection.");
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
     
            //}
            //catch (Exception ex)
            //{

            //    MessageBox.Show("Connection to the server cannot be established. Make sure you have a valid internet connection. Due To this, will not be able to get the updated CAR/NCR.", "Server Not Found");
            //}


     

            datAssigned.Value = DateTime.Now;
            datDueDate.Value = DateTime.Now;
           

            Workbook work = new Workbook(path);
            Worksheet sheet = work.Worksheets[3];

            int row = sheet.Cells.Rows.Count;

            for (int i = 4; i < row; i++)
            {
                if (sheet.Cells[i, 0].Value != null)
                {
                    cboProcess.Items.Add(sheet.Cells[i, 0].Value);
                }

            }

            cboType.Items.Add("Corrective Action");
            cboType.Items.Add("Preventive Action");
            cboType.Items.Add("Non-Conformity");
            cboType.Items.Add("Suggestion");

            cboSource.Items.Add("Customer Feedback");
            cboSource.Items.Add("Employer/Employee Feedback");
            cboSource.Items.Add("Supplier/Subcontractor");
            cboSource.Items.Add("Internal Audit");
            cboSource.Items.Add("External Audit");
            cboSource.Items.Add("Management Review");
            cboSource.Items.Add("Other");

            //DateTime myway = DateTime.MinValue;
            //txtActionCompleteDate.Text = myway.ToString("dd-MM-yyyy");
        }

        public void InputCorretive()
        {

            Workbook work = new Workbook(path);
            Worksheet sheet = work.Worksheets[0];

            int rowCount = 0;
            int row = sheet.Cells.MaxDataRow;
            int combine = 0;
            DateTime date = DateTime.Now;

           
            for (int i = 1; i < row; i++)
            {             
               if(sheet.Cells[i, 5].Type != CellValueType.IsNull)
                {
                    rowCount += 1;
                }
                                  
            }
            rowCount += 2;

            
            //Fill the Data Here:

            sheet.Cells["A" + rowCount].PutValue(rowCount - 3);
            sheet.Cells["B" + rowCount].PutValue(date);
            sheet.Cells["C" + rowCount].PutValue(cboType.SelectedItem.ToString());
            sheet.Cells["D" + rowCount].PutValue(cboSource.SelectedItem.ToString());
            sheet.Cells["E" + rowCount].PutValue(cboProcess.SelectedItem.ToString());
            sheet.Cells["F" + rowCount].PutValue(txtSumbitter.Text);
            sheet.Cells["G" + rowCount].PutValue(txtDescription.Text);
            sheet.Cells["H" + rowCount].PutValue(txtAssignedTo.Text);
            sheet.Cells["I" + rowCount].PutValue(datAssigned.Value.Date);
            sheet.Cells["J" + rowCount].PutValue(datDueDate.Value.Date);
            work.Save(path);

            MessageBox.Show("Entry Added. Please click View Entries or Edit Car for more information and access to the CAR LOG", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            //InputCorrectiveAction();
            try
            {
               

                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";



                using (var client = new SftpClient(host, port, username, password))
                {


                    if (File.Exists(@"C:\Reader\Loader.xlsx"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                        Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                        Worksheet sheet = work.Worksheets[1];

                        client.Connect();


                        if (client.IsConnected)
                        {
                            InputCorretive();
                            //MessageBox.Show("I'm connected to the client");  

                            string result = new DirectoryInfo(path).Parent.Name;

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                            }


                            using (var fileStream = new FileStream(path, FileMode.Open))
                            {

                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                if (client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                                {
                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));

                                    Regex regex = new Regex(@"^[a-zA-Z][\w\.-]{2,28}[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");

                                    if (regex.IsMatch(txtEmail.Text))
                                    {
                                        Run();
                                    }

                                    MessageBox.Show("NCR Entry Created");
                                }
                            }                         

                        }
                        else
                        {
                            MessageBox.Show("We couldn't connect to the server. It could be an internet connection issue. Therefore, your NCR will not be updated on the server.");
                           
                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }


        public void Run()
        {
            // Declare msg as MailMessage instance
            MailMessage msg = new MailMessage();

            // Use MailMessage properties like specify sender, recipient, message and HtmlBody
            msg.From = "info@imspulse.com";
            msg.To = txtEmail.Text ;
            msg.Subject = "New NCR/CAR Entry Started";
            msg.HtmlBody =
                @"<html xmlns=""http://www.w3.org/1999/xhtml"">
                        <head>
                            <meta name = ""viewport"" content = ""width=device-width, initial-scale=1.0"" />
   
                            <meta http - equiv = ""Content-Type"" content = ""text/html; charset=UTF-8"" />
                                
                            <!-- Latest compiled and minified CSS -->
                            <link rel=""stylesheet"" href=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"" integrity=""sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"" crossorigin=""anonymous"">

                            <!-- Optional theme-- >
                            <link rel = ""stylesheet"" href = ""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"" integrity = ""sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"" crossorigin = ""anonymous"" >
       

                            <!-- Latest compiled and minified JavaScript -->
                            <script src = ""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"" integrity = ""sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"" crossorigin = ""anonymous"" ></script>
            
                        </head>
                    <body>
       

                        <table class=""wrapper layout-default"" width=""100%"" cellpadding=""0"" cellspacing=""0"">

                            <!-- Header -->
                            <tr>
                                <td class=""header"" style=""text-align: center"">
                                        <h3>
                                           CAR/NCR Process Has Started
                                        </h3>
                                </td>
                            </tr>

                            <tr>
                                <td align = ""center"">
 
                                    <table class= ""content"" width = ""100%"" cellpadding = ""0"" cellspacing = ""0"" >
       
                                        <!-- Email Body -->
       
                                        <tr>
       
                                            <td class= ""body"" width = ""100%"" cellpadding = ""0"" cellspacing = ""0"">
             
                                         <table class= ""inner-body"" align = ""center"" width = ""570"" cellpadding = ""0"" cellspacing = ""0"">
                     

                                                     <tr>
                     
                                                         <td style=""text-align:center"" class= ""content-cell text-center"">
                      
                                                              <img style=""height:100%; width:100%;"" src= ""http://imspulse.com/storage/app/media/imscc.png"" alt = ""Image"">
                         
                                                        </td>
                         
                                                    </tr>
                         
                                                    <!-- Body content -->
                         
                                                    <tr>
                         
                                                        <td class= ""content-cell"">
                                                        <p>Good Day " + txtAssignedTo.Text + @"</p>
                                                                
                                                        <p>An NCR has been logged that needs your attention. Please Sign to the IMS Pulse App to address the issue</p>

                                                        <p>Regards <p>

                                                        <p>The IMS Pulse Team <p>
                                                        </td>
                                                    </tr>

                                                    </table >
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <!-- Footer -->
                               <tr>
                                    <td>
                                        <table class=""footer"" align=""center"" width=""570"" cellpadding=""0"" cellspacing=""0"">
                                                <tr>
                                                    <td style=""text-align:center"" class=""content-cell"" align=""center"">
                                                        <p>&copy; "+ DateTime.Now.Year + @" IMS Pulse. All rights Reserved.</p>
                                                    </td >
                                                </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                                    
                        </body>
                        </html> ";



            SmtpClient client = GetSmtpClient();
            try
            {
                // Client will send this message
                client.Send(msg);
                MessageBox.Show("Email has been sent to +");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private  SmtpClient GetSmtpClient()
        {
            SmtpClient client = new SmtpClient("smtpout.secureserver.net", 465, "info@imspulse.com", "@Paradice1");
            client.SecurityOptions = SecurityOptions.Auto;
            return client;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void txtIsClosed_Click(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            AdvancedCorrective advancedCorrective = new AdvancedCorrective();
            advancedCorrective.ShowDialog();
        }


        private void datAssigned_onValueChanged(object sender, EventArgs e)
        {

        }

        private void datDueDate_onValueChanged(object sender, EventArgs e)
        {

        }
    }
}
