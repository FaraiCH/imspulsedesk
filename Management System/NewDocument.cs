﻿using DocumentManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Management_System
{
    public partial class NewDocument : Form
    {
        OleDbConnection cn = new OleDbConnection();
        OleDbCommand cmd = new OleDbCommand();
        DataTable dataTable = new DataTable();
        string sql = "";
        string filename = ServerPath.PathLocation() + @"Shared\NewDocuments.accdb";
        string conString;

        public NewDocument()
        {
            InitializeComponent();
        }

        public string subLocation()
        {
            string subfile = string.Empty;
            if(radHR.Checked)
            {              
                subfile +=  @"Resources\Custom";                
            }

            else if(radOperations.Checked)
            {
                subfile = @"Operations\Custom";
            }
            
            return subfile;
        }

        public string subLocationAll()
        {
            string subfile = string.Empty;
            if (radHR.Checked)
            {
                subfile += @"Resources\";
            }

            else if (radOperations.Checked)
            {
                subfile = @"Operations\";
            }

            return subfile;
        }

        public string NewLocation()
        {
            string filename;
            string from = txtPath.Text;
            string to = "";
            
            filename = Path.GetFileName(from);

            if(radSafety.Checked == true)
            {
                to = Path.Combine(ServerPath.PathLocation() + @"Safety Management\IMS Global\" + subLocationAll()  + filename);
            }
            else if(radQuality.Checked == true)
            {
                to = Path.Combine(ServerPath.PathLocation() + @"MC QMS\" + subLocation() + filename);
            }
            else if (radEnvironment.Checked == true)
            {
                to = Path.Combine(ServerPath.PathLocation() + @"C:\Environment Management\" + subLocationAll() + filename);
            }
            else
            {
                MessageBox.Show("You must tick a management system", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (File.Exists(to))
            {
                MessageBox.Show("There is already a file there that matches the name of this one", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                File.Copy(from, to);
                return to;
            }

            return null;
        }

        public void PickManagement()
        {
            if(radQuality.Checked == true)
            {
                try
                {
                    conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename);

                    sql = "INSERT INTO NewDocument(DocumentPath) values(@doc)";
                    cn = new OleDbConnection(conString);
                    cmd = new OleDbCommand(sql, cn);
                    cn.Open();
                    cmd.Parameters.AddWithValue("@doc", NewLocation());
                    cmd.ExecuteNonQuery();
                    cn.Close();
                    MessageBox.Show("Done");
                    
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if(radSafety.Checked == true)
            {

                try
                {
                    conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename);

                    sql = "INSERT INTO NewDocS(DocumentPath) values(@doc)";
                    cn = new OleDbConnection(conString);
                    cmd = new OleDbCommand(sql, cn);
                    cn.Open();
                    cmd.Parameters.AddWithValue("@doc", NewLocation());
                    cmd.ExecuteNonQuery();
                    cn.Close();
                    MessageBox.Show("Done");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if(radEnvironment.Checked == true)
            {
                try
                {
                    conString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename);

                    sql = "INSERT INTO NewDocE(DocumentPath) values(@doc)";
                    cn = new OleDbConnection(conString);
                    cmd = new OleDbCommand(sql, cn);
                    cn.Open();
                    cmd.Parameters.AddWithValue("@doc", NewLocation());
                    cmd.ExecuteNonQuery();
                    cn.Close();
                    MessageBox.Show("Done");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
          
           
        }

        private void NewDocument_Load(object sender, EventArgs e)
        {
            bunifuFlatButton3.Visible = false;
        }

        private void NewDocument_FormClosed(object sender, FormClosedEventArgs e)
        {
         
        }

        public void OpenFileSuite()
        {
            OpenFileDialog saveFileDialog1 = new OpenFileDialog();
            saveFileDialog1.InitialDirectory = Convert.ToString(Environment.SpecialFolder.MyDocuments);
            saveFileDialog1.Filter = "Excel Document 2003|*.xls|Excel Document 2007|*.xlsx|Word Document 2003|*.doc|Word Document 2007|*.docx|Powerpoint Document 2003|*.ppt|Powerpoint Document 2007|*.pptx|Visio Document 2003|*.vsd|Visio Document 2007|*.vsdx";
            saveFileDialog1.FilterIndex = 1;


            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = saveFileDialog1.FileName;
            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            OpenFileSuite();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if((radEnvironment.Checked == true || radQuality.Checked == true || radSafety.Checked == true) && txtPath.Text != "")
            {
                PickManagement();
            }
            else
            {
                MessageBox.Show("You need to choose the document and which system you want to put the document in.", "Missing Value", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void NewDocument_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            DialogResult dialogue =  MessageBox.Show("You are about to change the default department folders to custom folders. Are you sure?", "Default Folder Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(dialogue == DialogResult.Yes)
            {
                CustomDocs customDocs = new CustomDocs();
                customDocs.ShowDialog();
            }
        }
    }
}
