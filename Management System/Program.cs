﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Aspose.Cells;
using System.IO;
using DocumentManager;

namespace Management_System
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new SplashScreen1());
            LicenseHelper licenseHelper = new LicenseHelper();
            Application.Run(new SplashScreen1());

            //if (licenseHelper.Check(false))
            //{
            //    if (Directory.Exists(@"C:\Management"))
            //    {
            //        Application.Run(new SplashScreen1());
            //    }
            //    else
            //    {
            //        Application.Run(new SplashScreen1());
            //    }
            //}


        }
    }


    public class MyNext : ApplicationContext
    {
        NotifyIcon notifyIcon = new NotifyIcon();
        LateDates configWindow = new LateDates();

        public MyNext()
        {
            
            MenuItem configMenuItem = new MenuItem("View Late Entries", new EventHandler(ShowConfig));
            MenuItem exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));
           
            NotifyIcon notifyIcon = new NotifyIcon();
            notifyIcon.Icon = Management_System.Properties.Resources.hnet_com_image__1_;
            notifyIcon.ContextMenu = new ContextMenu(new MenuItem[]
                { configMenuItem, exitMenuItem });
            
            notifyIcon.Visible = true;
            LateDates(notifyIcon);
            notifyIcon.DoubleClick += NotifyIcon_DoubleClick;
            RemoveApplicationFromStartup();
           
        }

        public MyNext(string Anything)
        {
           
        }

       
        public void LateDates(NotifyIcon notifyIcon)
        {
            try
            {
                string path = ServerPath.PathLocation() + @"Shared\CAR LOG.xlsx";
                string Late;
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];


                if(sheet.Cells[3,0].Type != CellValueType.IsNull)
                {
                    for (int i = 3; i < sheet.Cells.Rows.Count; i++)
                    {

                        if (sheet.Cells[i, 1].Type != CellValueType.IsNull)
                        {
                            if (sheet.Cells[i, 20].IntValue > 0)
                            {
                                //Late += string.Format("{0} \t {1}", dataTable.Rows[i]["F6"].ToString(), dataTable.Rows[i]["F15"].ToString()) + Environment.NewLine;
                                Late = "There are users who are late in their CAR entries. Right click the application icon in notification menu and then click 'View Late Entries'";
                                notifyIcon.ShowBalloonTip(1400, "Late CAR Entries Detected", Late, ToolTipIcon.Warning);
                                notifyIcon.BalloonTipClicked += NotifyIcon_BalloonTipClicked;
                            }
                         
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
          
                   
        }
            

        
        

        private void NotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            LateDates lateDates = new LateDates();
            lateDates.Show();
        }

        public void AddApplicationToStartup()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.SetValue("Management System", "\"" + Application.ExecutablePath + "\"");
            }
        }

        public void RemoveApplicationFromStartup()
        {
            using (RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                key.DeleteValue("Management System", false);
            }
        }

        private void NotifyIcon_DoubleClick(object sender, EventArgs e)
        {
              
        }

     

        void ShowConfig(object sender, EventArgs e)
        {
            // If we are already showing the window, merely focus it.
            if (configWindow.Visible)
            {
                configWindow.Activate();
            }
            else
            {
                configWindow.ShowDialog();
            }
        }

        void Exit(object sender, EventArgs e)
        {
            // We must manually tidy up and remove the icon before we exit.
            // Otherwise it will be left behind until the user mouses over.
            notifyIcon.Visible = false;
            Application.Exit();
            this.Dispose();
        }
    }
}
