﻿namespace Management_System
{
    partial class QMSForms
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QMSForms));
            this.bunifuTileButton2 = new Bunifu.Framework.UI.BunifuTileButton();
            this.adornerUIManager1 = new DevExpress.Utils.VisualEffects.AdornerUIManager(this.components);
            this.badge1 = new DevExpress.Utils.VisualEffects.Badge();
            this.treeList1 = new DevExpress.XtraTreeList.TreeList();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.treeList2 = new DevExpress.XtraTreeList.TreeList();
            this.treeList3 = new DevExpress.XtraTreeList.TreeList();
            this.bunifuTileButton3 = new Bunifu.Framework.UI.BunifuTileButton();
            this.bunifuTileButton1 = new Bunifu.Framework.UI.BunifuTileButton();
            this.xtraOpenFileDialog1 = new DevExpress.XtraEditors.XtraOpenFileDialog(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.adornerUIManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuTileButton2
            // 
            this.bunifuTileButton2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuTileButton2.BackColor = System.Drawing.Color.DodgerBlue;
            this.bunifuTileButton2.color = System.Drawing.Color.DodgerBlue;
            this.bunifuTileButton2.colorActive = System.Drawing.Color.LightSkyBlue;
            this.bunifuTileButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton2.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton2.Image")));
            this.bunifuTileButton2.ImagePosition = 16;
            this.bunifuTileButton2.ImageZoom = 50;
            this.bunifuTileButton2.LabelPosition = 33;
            this.bunifuTileButton2.LabelText = "Quality Management System";
            this.bunifuTileButton2.Location = new System.Drawing.Point(339, 63);
            this.bunifuTileButton2.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuTileButton2.Name = "bunifuTileButton2";
            this.bunifuTileButton2.Size = new System.Drawing.Size(182, 140);
            this.bunifuTileButton2.TabIndex = 16;
            this.bunifuTileButton2.Click += new System.EventHandler(this.bunifuTileButton2_Click);
            // 
            // adornerUIManager1
            // 
            this.adornerUIManager1.BadgeProperties.Image = ((System.Drawing.Image)(resources.GetObject("adornerUIManager1.BadgeProperties.Image")));
            this.adornerUIManager1.BadgeProperties.Location = System.Drawing.ContentAlignment.MiddleRight;
            this.adornerUIManager1.BadgeProperties.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("adornerUIManager1.BadgeProperties.SvgImage")));
            this.adornerUIManager1.Elements.Add(this.badge1);
            this.adornerUIManager1.Owner = this;
            this.adornerUIManager1.ValidationHintProperties.ValidState.Icon = ((System.Drawing.Image)(resources.GetObject("adornerUIManager1.ValidationHintProperties.ValidState.Icon")));
            // 
            // badge1
            // 
            this.badge1.TargetElementRegion = DevExpress.Utils.VisualEffects.TargetElementRegion.Control;
            // 
            // treeList1
            // 
            this.treeList1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.treeList1.Location = new System.Drawing.Point(295, 224);
            this.treeList1.Name = "treeList1";
            this.treeList1.Size = new System.Drawing.Size(272, 237);
            this.treeList1.TabIndex = 26;
            this.treeList1.PopupMenuShowing += new DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeList1_PopupMenuShowing);
            this.treeList1.DoubleClick += new System.EventHandler(this.treeList1_DoubleClick);
            this.treeList1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeList1_MouseDown);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "thumb_14486407500Folder.png");
            this.imageCollection1.Images.SetKeyName(1, "Word.png");
            this.imageCollection1.Images.SetKeyName(2, "Excel.png");
            this.imageCollection1.Images.SetKeyName(3, "Powerpoint.png");
            this.imageCollection1.Images.SetKeyName(4, "Visio.png");
            this.imageCollection1.Images.SetKeyName(5, "xpdf.jpg");
            this.imageCollection1.Images.SetKeyName(6, "IMS Pulse V2-04.png");
            this.imageCollection1.Images.SetKeyName(7, "images.png");
            this.imageCollection1.Images.SetKeyName(8, "download.png");
            this.imageCollection1.Images.SetKeyName(9, "download (1).png");
            // 
            // treeList2
            // 
            this.treeList2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.treeList2.Location = new System.Drawing.Point(3, 224);
            this.treeList2.Name = "treeList2";
            this.treeList2.Size = new System.Drawing.Size(272, 237);
            this.treeList2.TabIndex = 27;
            this.treeList2.PopupMenuShowing += new DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeList2_PopupMenuShowing_1);
            this.treeList2.DoubleClick += new System.EventHandler(this.treeList2_DoubleClick);
            // 
            // treeList3
            // 
            this.treeList3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.treeList3.Location = new System.Drawing.Point(600, 224);
            this.treeList3.Name = "treeList3";
            this.treeList3.Size = new System.Drawing.Size(272, 237);
            this.treeList3.TabIndex = 28;
            this.treeList3.PopupMenuShowing += new DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeList3_PopupMenuShowing);
            this.treeList3.DoubleClick += new System.EventHandler(this.treeList3_DoubleClick);
            // 
            // bunifuTileButton3
            // 
            this.bunifuTileButton3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuTileButton3.BackColor = System.Drawing.Color.ForestGreen;
            this.bunifuTileButton3.color = System.Drawing.Color.ForestGreen;
            this.bunifuTileButton3.colorActive = System.Drawing.Color.LimeGreen;
            this.bunifuTileButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton3.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton3.Image")));
            this.bunifuTileButton3.ImagePosition = 16;
            this.bunifuTileButton3.ImageZoom = 50;
            this.bunifuTileButton3.LabelPosition = 33;
            this.bunifuTileButton3.LabelText = "OPERATIONS";
            this.bunifuTileButton3.Location = new System.Drawing.Point(644, 63);
            this.bunifuTileButton3.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuTileButton3.Name = "bunifuTileButton3";
            this.bunifuTileButton3.Size = new System.Drawing.Size(182, 140);
            this.bunifuTileButton3.TabIndex = 30;
            // 
            // bunifuTileButton1
            // 
            this.bunifuTileButton1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bunifuTileButton1.BackColor = System.Drawing.Color.Chocolate;
            this.bunifuTileButton1.color = System.Drawing.Color.Chocolate;
            this.bunifuTileButton1.colorActive = System.Drawing.Color.Orange;
            this.bunifuTileButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuTileButton1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuTileButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuTileButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuTileButton1.Image")));
            this.bunifuTileButton1.ImagePosition = 16;
            this.bunifuTileButton1.ImageZoom = 50;
            this.bunifuTileButton1.LabelPosition = 33;
            this.bunifuTileButton1.LabelText = "HR";
            this.bunifuTileButton1.Location = new System.Drawing.Point(48, 63);
            this.bunifuTileButton1.Margin = new System.Windows.Forms.Padding(5);
            this.bunifuTileButton1.Name = "bunifuTileButton1";
            this.bunifuTileButton1.Size = new System.Drawing.Size(182, 140);
            this.bunifuTileButton1.TabIndex = 29;
            // 
            // xtraOpenFileDialog1
            // 
            this.xtraOpenFileDialog1.FileName = "xtraOpenFileDialog1";
            // 
            // QMSForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.bunifuTileButton3);
            this.Controls.Add(this.bunifuTileButton1);
            this.Controls.Add(this.treeList3);
            this.Controls.Add(this.treeList2);
            this.Controls.Add(this.treeList1);
            this.Controls.Add(this.bunifuTileButton2);
            this.DoubleBuffered = true;
            this.Name = "QMSForms";
            this.Size = new System.Drawing.Size(884, 582);
            this.Load += new System.EventHandler(this.HRQuality_Load);
            ((System.ComponentModel.ISupportInitialize)(this.adornerUIManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton2;
        private DevExpress.Utils.VisualEffects.AdornerUIManager adornerUIManager1;
        private DevExpress.Utils.VisualEffects.Badge badge1;
        private DevExpress.XtraTreeList.TreeList treeList1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraTreeList.TreeList treeList3;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton3;
        private Bunifu.Framework.UI.BunifuTileButton bunifuTileButton1;
        private DevExpress.XtraEditors.XtraOpenFileDialog xtraOpenFileDialog1;
    }
}
