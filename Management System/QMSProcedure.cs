﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentManager;
using System.IO;
using Aspose.Words;
using Aspose.Cells;
using Aspose.Slides;
using System.Net;
using Microsoft.VisualBasic.FileIO;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraEditors;
using System.Diagnostics;
using DevExpress.XtraTreeList.Menu;
using Aspose.Pdf;
using Renci.SshNet;
using TheRelease;
using MySql.Data.MySqlClient;
using System.Threading;

namespace Management_System
{
    public partial class QMSProcedure : UserControl
    {


        WaitForm1 wait = new WaitForm1();
        public QMSProcedure()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Words.License().SetLicense(stream);

            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));

            stream2.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream2);


            Stream stream3 = new MemoryStream(Convert.FromBase64String(LData));

            stream3.Seek(0, SeekOrigin.Begin);
            new Aspose.Slides.License().SetLicense(stream3);


            Stream stream4 = new MemoryStream(Convert.FromBase64String(LData));
            stream4.Seek(0, SeekOrigin.Begin);
            new Aspose.Pdf.License().SetLicense(stream4);
            InitializeComponent();
        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
    
        }

        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
    
        }

        private void bunifuTileButton3_Click(object sender, EventArgs e)
        {
            
        }

        private void ShowGif()
        {

            this.Invoke(new Action(() => { wait.Show(); }));

        }

        private void ProcedureQuality_Load(object sender, EventArgs e)
        {
            try
            {

                System.Threading.Thread waitThread = new System.Threading.Thread(() =>
                {
                    ShowGif();

                });
                waitThread.Start();
                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                Worksheet sheet = work.Worksheets[1];

                if (sheet.Cells["B1"].StringValue == "cloud")
                {

                    LoadData();


                }
                else
                {
                    System.Threading.Thread longTaskThread = new System.Threading.Thread(loadlist);
                    longTaskThread.Start();

                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void LoadData()
        {
  
            System.Threading.Thread.Sleep(1000);
  
            // Start a thread to load data asynchronously.
            Thread loadDataThread = new Thread(LoadDataAsync);
            loadDataThread.Start();
        }

        // This method is called asynchronously
        private void LoadDataAsync()
        {


            var qms = CloudDataGenerator.CreateDataPro("MC QMS");
            var hr = CloudDataGenerator.CreateDataHRPro();
            var op = CloudDataGenerator.CreateDataOperationPro();
            // ... get data from connection

            // Since this method is executed from another thread than the main thread (AKA UI thread),
            // Any logic that tried to manipulate UI from this thread, must go into BeginInvoke() call.
            // By using BeginInvoke() we state that this code needs to be executed on UI thread.

            // Check if this code is executed on some other thread than UI thread
            if (InvokeRequired) // In this example, this will return `true`.
            {
                BeginInvoke(new Action(() =>
                {
                    PopulateUI(qms, hr, op);
                }));
            }
        }

        private void PopulateUI(List<CloudData> first, List<CloudData> second, List<CloudData> third)
        {
            treeList1.KeyFieldName = "ID";
            treeList2.KeyFieldName = "ID";
            treeList3.KeyFieldName = "ID";
            treeList1.ParentFieldName = "RegionID";
            treeList2.ParentFieldName = "RegionID";
            treeList3.ParentFieldName = "RegionID";
            //Allow the treelist to create columns bound to the fields the KeyFieldName and ParentFieldName properties specify.
            treeList1.OptionsBehavior.PopulateServiceColumns = true;
            treeList2.OptionsBehavior.PopulateServiceColumns = true;
            treeList3.OptionsBehavior.PopulateServiceColumns = true;
            treeList1.DataSource = first;

            treeList2.DataSource = second;

            treeList3.DataSource = third;

            //Change the row height.
            treeList1.RowHeight = 23;
            treeList2.RowHeight = 23;
            treeList3.RowHeight = 23;

            //Access the automatically created columns.

            TreeListColumn colRegion;
            TreeListColumn colRegio2;
            TreeListColumn colRegio3;

            colRegion = treeList1.Columns["Document"]; colRegion.OptionsColumn.ReadOnly = true;
            colRegio2 = treeList1.Columns["Document"]; colRegio2.OptionsColumn.ReadOnly = true;
            colRegio3 = treeList1.Columns["Document"]; colRegio3.OptionsColumn.ReadOnly = true;

            //Hide the key columns. An end-user can access them from the Customization Form.
            treeList1.Columns[treeList1.KeyFieldName].Visible = false;
            treeList2.Columns[treeList1.KeyFieldName].Visible = false;
            treeList3.Columns[treeList1.KeyFieldName].Visible = false;
            treeList1.Columns[treeList1.ParentFieldName].Visible = false;
            treeList2.Columns[treeList1.ParentFieldName].Visible = false;
            treeList3.Columns[treeList1.ParentFieldName].Visible = false;

            //Do not stretch columns to the treelist width.

            treeList1.StateImageList = imageCollection1;

            treeList2.StateImageList = imageCollection1;
            treeList3.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList1.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 7;

                }
                else
                {
                    node.StateImageIndex = 7;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

            foreach (TreeListNode node in treeList2.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 8;

                }
                else
                {
                    node.StateImageIndex = 8;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            foreach (TreeListNode node in treeList3.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 9;

                }
                else
                {
                    node.StateImageIndex = 9;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            this.Invoke(new Action(() => { wait.Dispose(); }));
        }


        public void loadlist()
        {
            // long load...
            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(1000);
            }

            //Specify the fields that arrange underlying data as a hierarchy.
            this.Invoke(new Action(() => { treeList1.KeyFieldName = "ID"; }));
            this.Invoke(new Action(() => { treeList2.KeyFieldName = "ID"; }));
            this.Invoke(new Action(() => { treeList3.KeyFieldName = "ID"; }));
            this.Invoke(new Action(() => { treeList1.ParentFieldName = "RegionID"; }));
            this.Invoke(new Action(() => { treeList2.ParentFieldName = "RegionID"; }));
            this.Invoke(new Action(() => { treeList3.ParentFieldName = "RegionID"; }));
            //Allow the treelist to create columns bound to the fields the KeyFieldName and ParentFieldName properties specify.
            this.Invoke(new Action(() => { treeList1.OptionsBehavior.PopulateServiceColumns = true; }));
            this.Invoke(new Action(() => { treeList2.OptionsBehavior.PopulateServiceColumns = true; }));
            this.Invoke(new Action(() => { treeList3.OptionsBehavior.PopulateServiceColumns = true; }));
            //Specify the data source.
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];

            this.Invoke(new Action(() => { treeList1.DataSource = SalesDataGenerator.CreateDataPro(@"MC QMS\QMS"); }));
            this.Invoke(new Action(() => { treeList2.DataSource = SalesDataGenerator.CreateDataHRPro(@"MC QMS\Resources"); }));
            this.Invoke(new Action(() => { treeList3.DataSource = SalesDataGenerator.CreateDataOperationPro(@"MC QMS\Operations"); }));



            //The treelist automatically creates columns for the public fields found in the data source. 
            //You do not need to call the TreeList.PopulateColumns method unless the treeList1.OptionsBehavior.AutoPopulateColumns option is disabled.

            //Change the row height.
            this.Invoke(new Action(() => { treeList1.RowHeight = 23; }));
            this.Invoke(new Action(() => { treeList2.RowHeight = 23; }));
            this.Invoke(new Action(() => { treeList3.RowHeight = 23; }));

            //Access the automatically created columns.

            TreeListColumn colRegion;
            TreeListColumn colRegio2;
            TreeListColumn colRegio3;

            this.Invoke(new Action(() => { colRegion = treeList1.Columns["Document"]; colRegion.OptionsColumn.ReadOnly = true; }));
            this.Invoke(new Action(() => { colRegio2 = treeList1.Columns["Document"]; colRegio2.OptionsColumn.ReadOnly = true; }));
            this.Invoke(new Action(() => { colRegio3 = treeList1.Columns["Document"]; colRegio3.OptionsColumn.ReadOnly = true; }));

            //Hide the key columns. An end-user can access them from the Customization Form.
            this.Invoke(new Action(() => { treeList1.Columns[treeList1.KeyFieldName].Visible = false; }));
            this.Invoke(new Action(() => { treeList2.Columns[treeList1.KeyFieldName].Visible = false; }));
            this.Invoke(new Action(() => { treeList3.Columns[treeList1.KeyFieldName].Visible = false; }));
            this.Invoke(new Action(() => { treeList1.Columns[treeList1.ParentFieldName].Visible = false; }));
            this.Invoke(new Action(() => { treeList2.Columns[treeList1.ParentFieldName].Visible = false; }));
            this.Invoke(new Action(() => { treeList3.Columns[treeList1.ParentFieldName].Visible = false; }));

            //Do not stretch columns to the treelist width.

            this.Invoke(new Action(() => { treeList1.StateImageList = imageCollection1; }));

            this.Invoke(new Action(() => { treeList2.StateImageList = imageCollection1; }));
            this.Invoke(new Action(() => { treeList3.StateImageList = imageCollection1; }));

            foreach (TreeListNode node in treeList1.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 7;

                }
                else
                {
                    node.StateImageIndex = 7;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

            foreach (TreeListNode node in treeList2.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 8;

                }
                else
                {
                    node.StateImageIndex = 8;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            foreach (TreeListNode node in treeList3.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 9;

                }
                else
                {
                    node.StateImageIndex = 9;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            this.Invoke(new Action(() => { wait.Dispose(); }));
            //Thread.Sleep(10000);

        }


        public void ResetAll()
        {
            //lstHR.Hide();
            //lstOperation.Hide();
            //lstSMS.Hide();
            //bunifuTileButton2.color = Color.DodgerBlue;
            //bunifuTileButton1.color = Color.Chocolate;
            //bunifuTileButton3.color = Color.ForestGreen;
            //bunifuFlatButton1.Enabled = false;
            //bunifuFlatButton2.Enabled = false;
            //bunifuFlatButton3.Enabled = false;
        }



        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }

        private void bbDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList1, 7, @"/QMS");

        }

        private void huDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList2, 8, @"/Resources");

        }
        private void opDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList3, 9, @"/Operations");

        }


        private void bbEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList1, 7, @"/QMS");

        }

        private void huEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList2, 8, @"/Resources");

        }
        private void opEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList3, 9, @"/Operations");

        }
        public void bbDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList1, 7, @"/QMS");
        }
        public void huDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList2, 8, @"/Resources");
        }
        public void opDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList3, 9, @"/Operations");
        }

        public void treelistDecrypt(TreeList treeList, int color, string folder)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;


            string primeFolder = @"MC QMS";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
            try
            {

                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                Worksheet sheet = work.Worksheets[1];

                if (sheet.Cells["B1"].StringValue == "cloud")
                {
                    string first = string.Empty;
                    var host = "imspulse.com";
                    var port = 22;
                    var username = "farai";
                    var password = "@Paradice1";


                    if (selectedNodes[0].HasChildren == false)
                    {

                        using (var client = new SftpClient(host, port, username, password))
                        {
                            string directorys = "";

                            client.Connect();
                            if (client.IsConnected)
                            {

                                if (File.Exists(@"C:\Reader\Loader.xlsx"))
                                {

                                    foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                                    {
                                        if (item.IsDirectory)
                                        {
                                            directorys = item.Name;

                                            if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys))
                                            {
                                                Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys);
                                            }
                                        }

                                        foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directorys))
                                        {
                                            if (!itemdoc.IsDirectory)
                                            {


                                                if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                                {

                                                    using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys + "/" + itemdoc.Name))
                                                    {
                                                        client.DownloadFile(itemdoc.FullName, fileStream);

                                                    }

                                                    string path = Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys + "/" + itemdoc.Name;
                                                    // initialize a new XtraInputBoxArgs instance
                                                    XtraInputBoxArgs args = new XtraInputBoxArgs();
                                                    // set required Input Box options
                                                    args.Caption = "Password Protection";
                                                    args.Prompt = "Enter Password To Encrypt";
                                                    args.DefaultButtonIndex = 0;
                                                    // initialize a DateEdit editor with custom settings
                                                    TextEdit editor = new TextEdit();

                                                    args.Editor = editor;
                                                    // a default DateEdit value
                                                    args.DefaultResponse = "My Password";
                                                    // display an Input Box with the custom editor
                                                    var result = XtraInputBox.Show(args).ToString();

                                                    if (path.Contains(".doc"))
                                                    {

                                                        Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                                                        opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                                                        opt.Password = result;
                                                        Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                                        Aspose.Words.Document docu = new Aspose.Words.Document(path, getum12);
                                                        docu.Unprotect();
                                                        docu.Save(path);
                                                    }
                                                    if (path.Contains(".xls"))
                                                    {
                                                        Aspose.Cells.LoadOptions getum3 = new Aspose.Cells.LoadOptions { Password = result };
                                                        Workbook workt = new Workbook(path, getum3);

                                                        workt.Settings.Password = "";

                                                        workt.Save(path);
                                                    }
                                                    if (path.Contains(".ppt"))
                                                    {
                                                        Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                                        using (Presentation presentation = new Presentation(path, getpere))
                                                        {
                                                            presentation.ProtectionManager.RemoveEncryption();

                                                            presentation.Save(path, Aspose.Slides.Export.SaveFormat.Pptx);
                                                        }
                                                    }
                                                    if (path.Contains(".pdf"))
                                                    {

                                                        Aspose.Pdf.Document document = new Aspose.Pdf.Document(path, result);
                                                        document.Decrypt();
                                                        document.Save(path);
                                                    }

                                                    Workbook cloudbooker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                    Worksheet cloudsheet = cloudbooker.Worksheets[0];

                                                    if (cloudsheet.Cells.MaxDataRow == 0)
                                                    {
                                                        cloudsheet.Cells[1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[1, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[1, 2].PutValue(folder);
                                                    }
                                                    else
                                                    {

                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow + 1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 2].PutValue(folder);
                                                    }



                                                    //cloudsheet.Cells["A" + row].PutValue(itemdoc.Name);

                                                    cloudbooker.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                    break;
                                                }


                                            }
                                        }

                                    }

                                }
                            }
                        }

                        //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                        //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                    }
                }
                else
                {
                    for (int i = 0; i < directory.Length; i++)
                    {
                        if (selectedNodes[0].HasChildren == false)
                        {

                            if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                            {
                                // initialize a new XtraInputBoxArgs instance
                                XtraInputBoxArgs args = new XtraInputBoxArgs();
                                // set required Input Box options
                                args.Caption = "Password Protection";
                                args.Prompt = "Enter Password To Decrypt";
                                args.DefaultButtonIndex = 0;
                                // initialize a DateEdit editor with custom settings
                                TextEdit editor = new TextEdit();

                                args.Editor = editor;
                                // a default DateEdit value
                                args.DefaultResponse = "My Password";
                                // display an Input Box with the custom editor
                                var result = XtraInputBox.Show(args).ToString();

                                if (directory[i].Contains(".doc"))
                                {
                                    Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                    Aspose.Words.Document docu = new Aspose.Words.Document(directory[i], getum12);
                                    docu.Unprotect();
                                    docu.Save(directory[i]);
                                }
                                if (directory[i].Contains(".xls"))
                                {
                                    Aspose.Cells.LoadOptions getums = new Aspose.Cells.LoadOptions { Password = result };
                                    Workbook worsk = new Workbook(directory[i], getums);

                                    worsk.Settings.Password = null;

                                    worsk.Save(directory[i]);
                                }
                                if (directory[i].Contains(".ppt"))
                                {
                                    Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                    using (Presentation presentation = new Presentation(directory[i], getpere))
                                    {
                                        presentation.ProtectionManager.RemoveEncryption();
                                        presentation.Save(directory[i], Aspose.Slides.Export.SaveFormat.Pptx);
                                    }
                                }
                                if (directory[i].Contains(".pdf"))
                                {
                                    Aspose.Pdf.Document document = new Aspose.Pdf.Document(directory[i], result);
                                    document.Decrypt();
                                    document.Save(directory[i]);
                                }

                            }
                        }

                    }


                    treeList.RefreshDataSource();
                    treeList.StateImageList = imageCollection1;

                    foreach (TreeListNode node in treeList.Nodes)
                    {


                        // first node
                        if (node.HasChildren)
                        {
                            node.StateImageIndex = color;

                        }
                        else
                        {
                            node.StateImageIndex = color;
                        }
                        foreach (TreeListNode mumba in node.Nodes)
                        {
                            if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                            {
                                mumba.StateImageIndex = 1;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                            {
                                mumba.StateImageIndex = 2;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                            {
                                mumba.StateImageIndex = 3;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                            {
                                mumba.StateImageIndex = 4;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                            {
                                mumba.StateImageIndex = 5;
                            }
                            else
                            {
                                mumba.StateImageIndex = 6;
                            }
                        }

                    }
                }
                MessageBox.Show("Item Decrypted");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void treelistEncrypt(TreeList treeList, int color, string folder)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;

            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];

            if (sheet.Cells["B1"].StringValue == "cloud")
            {
                string first = string.Empty;
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";


                if (selectedNodes[0].HasChildren == false)
                {

                    using (var client = new SftpClient(host, port, username, password))
                    {
                        string directory = "";

                        client.Connect();
                        if (client.IsConnected)
                        {

                            if (File.Exists(@"C:\Reader\Loader.xlsx"))
                            {

                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directory = item.Name;

                                        if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory);
                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directory))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {


                                            if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                            {

                                                using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory + "/" + itemdoc.Name))
                                                {
                                                    client.DownloadFile(itemdoc.FullName, fileStream);

                                                }

                                                string path = Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory + "/" + itemdoc.Name;
                                                // initialize a new XtraInputBoxArgs instance
                                                XtraInputBoxArgs args = new XtraInputBoxArgs();
                                                // set required Input Box options
                                                args.Caption = "Password Protection";
                                                args.Prompt = "Enter Password To Encrypt";
                                                args.DefaultButtonIndex = 0;
                                                // initialize a DateEdit editor with custom settings
                                                TextEdit editor = new TextEdit();

                                                args.Editor = editor;
                                                // a default DateEdit value
                                                args.DefaultResponse = "My Password";
                                                // display an Input Box with the custom editor
                                                var result = XtraInputBox.Show(args).ToString();

                                                if (path.Contains(".doc"))
                                                {

                                                    Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                                                    opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                                                    opt.Password = result;
                                                    Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                                    Aspose.Words.Document docu = new Aspose.Words.Document(path, getum12);

                                                    docu.Save(path, opt);
                                                }
                                                if (path.Contains(".xls"))
                                                {
                                                    Aspose.Cells.LoadOptions getum3 = new Aspose.Cells.LoadOptions { Password = result };
                                                    Workbook workt = new Workbook(path, getum);

                                                    workt.Settings.Password = result;

                                                    workt.Save(path);
                                                }
                                                if (path.Contains(".ppt"))
                                                {
                                                    Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                                    using (Presentation presentation = new Presentation(path))
                                                    {
                                                        presentation.ProtectionManager.Encrypt(result);

                                                        presentation.Save(path, Aspose.Slides.Export.SaveFormat.Pptx);
                                                    }
                                                }
                                                if (path.Contains(".pdf"))
                                                {

                                                    // Open document
                                                    Aspose.Pdf.Document document = new Aspose.Pdf.Document(path);
                                                    // Encrypt PDF
                                                    document.Encrypt(result, result, 0, CryptoAlgorithm.RC4x128);

                                                    // Save updated PDF
                                                    document.Save(path);
                                                }

                                                Workbook cloudbooker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                Worksheet cloudsheet = cloudbooker.Worksheets[0];

                                                if (cloudsheet.Cells.MaxDataRow == 0)
                                                {
                                                    cloudsheet.Cells[1, 0].PutValue(itemdoc.Name);
                                                    cloudsheet.Cells[1, 1].PutValue(item.Name);
                                                    cloudsheet.Cells[1, 2].PutValue(folder);
                                                }
                                                else
                                                {

                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow + 1, 0].PutValue(itemdoc.Name);
                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 1].PutValue(item.Name);
                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 2].PutValue(folder);
                                                }



                                                //cloudsheet.Cells["A" + row].PutValue(itemdoc.Name);

                                                cloudbooker.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                break;
                                            }


                                        }
                                    }

                                }

                            }
                        }
                    }

                    //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                    //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                }
            }
            else
            {
                string primeFolder = @"MC QMS";

                string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < directory.Length; i++)
                {
                    if (selectedNodes[0].HasChildren == false)
                    {

                        if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                        {
                            // initialize a new XtraInputBoxArgs instance
                            XtraInputBoxArgs args = new XtraInputBoxArgs();
                            // set required Input Box options
                            args.Caption = "Password Protection";
                            args.Prompt = "Enter Password To Encrypt";
                            args.DefaultButtonIndex = 0;
                            // initialize a DateEdit editor with custom settings
                            TextEdit editor = new TextEdit();

                            args.Editor = editor;
                            // a default DateEdit value
                            args.DefaultResponse = "My Password";
                            // display an Input Box with the custom editor
                            var result = XtraInputBox.Show(args).ToString();

                            if (directory[i].Contains(".doc"))
                            {

                                Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                                opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                                opt.Password = result;
                                Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                Aspose.Words.Document docu = new Aspose.Words.Document(directory[i], getum12);

                                docu.Save(directory[i], opt);
                            }
                            if (directory[i].Contains(".xls"))
                            {
                                Aspose.Cells.LoadOptions getum3 = new Aspose.Cells.LoadOptions { Password = result };
                                Workbook workt = new Workbook(directory[i], getum);

                                work.Settings.Password = result;

                                work.Save(directory[i]);
                            }
                            if (directory[i].Contains(".ppt"))
                            {
                                Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                using (Presentation presentation = new Presentation(directory[i]))
                                {
                                    presentation.ProtectionManager.Encrypt(result);

                                    presentation.Save(directory[i], Aspose.Slides.Export.SaveFormat.Pptx);
                                }
                            }
                            if (directory[i].Contains(".pdf"))
                            {

                                // Open document
                                Aspose.Pdf.Document document = new Aspose.Pdf.Document(directory[i]);
                                // Encrypt PDF
                                document.Encrypt(result, result, 0, CryptoAlgorithm.RC4x128);

                                // Save updated PDF
                                document.Save(directory[i]);
                            }



                        }
                    }

                }
            }

            MessageBox.Show("Item Encrypted");

            treeList.RefreshDataSource();
            treeList.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = color;

                }
                else
                {
                    node.StateImageIndex = color;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

        }

        public void treelistDoubleClick(string folder, TreeList treeList)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;

            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];



            //Connect to Remote Mysql Database

            int idGroup = 0;

            int idUser = 0;
            var dbCon = DBConnection.Instance();
            dbCon.Server = "92.205.25.31";
            dbCon.DatabaseName = "imspulse";
            dbCon.UserName = "manny";
            dbCon.Password = "@Paradice1";

            if (dbCon.IsConnect())
            {
                //Get all users and compare that user to the login
                string query = "SELECT * FROM imspulse.backend_users;";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string idInfo = reader.GetString(0);
                    string loginInfo = reader.GetString(3);


                    if (sheet.Cells["C1"].StringValue == loginInfo)
                    {
                        idUser = int.Parse(idInfo);

                        break;
                    }

                }
                reader.Close();

            }

            if (dbCon.IsConnect())
            {
                //Get the user group and check to see if the user is on Trial
                string query = "SELECT * FROM imspulse.backend_users_groups;";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int user_id = int.Parse(reader.GetString(0));
                    int user_group = int.Parse(reader.GetString(1));

                    //Check to see if User and Group are occupied with the details of the table

                    if ((user_id == idUser) && (user_group != 0))
                    {
                        //Put the User Group of the User in idGroup
                        idGroup = user_group;
                        break;
                    }
                    else
                    {
                        idGroup = 0;
                    }


                }
                reader.Close();


                if (idGroup == 8)
                {
                    //suppose col0 and col1 are defined as VARCHAR in the DB
                    string query2 = "select * from imspulse.backend_users_groups where user_group_id = " + idGroup + ";";
                    var cmd2 = new MySqlCommand(query2, dbCon.Connection);
                    var reader2 = cmd.ExecuteReader();
                    while (reader2.Read())
                    {

                        MessageBox.Show("This user is suspended", "Suspended");

                        break;

                    }
                    reader.Close();
                    Application.Exit();
                 
                }

            }

            if (sheet.Cells["B1"].StringValue == "cloud")
            {
                string first = string.Empty;
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";


                if (selectedNodes[0].HasChildren == false)
                {

                    using (var client = new SftpClient(host, port, username, password))
                    {
                        string directory = "";

                        client.Connect();
                        if (client.IsConnected)
                        {

                            if (File.Exists(@"C:\Reader\Loader.xlsx"))
                            {

                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directory = item.Name;

                                        if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory);
                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directory))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {


                                            if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                            {

                                                using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory + "/" + itemdoc.Name))
                                                {
                                                    client.DownloadFile(itemdoc.FullName, fileStream);

                                                }

                                                Process.Start(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directory + "/" + itemdoc.Name);
                                                Workbook cloudbooker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                Worksheet cloudsheet = cloudbooker.Worksheets[0];

                                                if (cloudsheet.Cells.MaxDataRow == 0)
                                                {
                                                    cloudsheet.Cells[1, 0].PutValue(itemdoc.Name);
                                                    cloudsheet.Cells[1, 1].PutValue(item.Name);
                                                    cloudsheet.Cells[1, 2].PutValue(folder);
                                                }
                                                else
                                                {

                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow + 1, 0].PutValue(itemdoc.Name);
                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 1].PutValue(item.Name);
                                                    cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 2].PutValue(folder);
                                                }



                                                //cloudsheet.Cells["A" + row].PutValue(itemdoc.Name);

                                                cloudbooker.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");

                                                break;
                                            }


                                        }
                                    }

                                }

                            }
                        }
                    }

                    //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                    //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                }
            }
            else
            {
                try
                {
                    string primeFolder = @"MC QMS" + folder;
                    int duplicates = 0;
                    string first = string.Empty;
                    string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                    for (int i = 0; i < directory.Length; i++)
                    {
                        if (selectedNodes[0].HasChildren == false)
                        {

                            if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                            {
                                duplicates++;

                                if (duplicates == 1)
                                {
                                    if (selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString() != selectedNodes[0].ParentNode.GetDisplayText(treeList.Columns[2]).ToString())
                                    {

                                        Process.Start(directory[i]);
                                        first = directory[i];
                                    }
                                }

                                if (duplicates > 1)
                                {
                                    DialogResult da = MessageBox.Show("You have a duplicate document of the exact same name , code and/or revision located at " + directory[i] + ". The first instance of this document, which will be treated as the original, is located at: " + first + ".  Do you want to delete or keep the duplicate? Please note that if you choose to keep it, IMS Pulse will continuously ask you to delete it, as the document would infridge on the integrity of the Quality Management System structure.", "Duplicate Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                    if (da == DialogResult.Yes)
                                    {
                                        FileSystem.DeleteFile(directory[i],
                              Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                              Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin,
                              Microsoft.VisualBasic.FileIO.UICancelOption.ThrowException);
                                        MessageBox.Show("The Duplicate has been scheduled for removal. The next time you load the Quality Centre, the duplicate will be removed");
                                        break;
                                    }

                                }


                            }


                        }

                    }
                }
                catch (Exception)
                {

                    MessageBox.Show("What you double clicked was not a document", "Invalid Double Click", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
           
        }
        public void treelistDeleteItem(TreeList treeList, int color, string folder)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;
            string primeFolder = @"MC QMS";
            string first = string.Empty;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];

            if (sheet.Cells["B1"].StringValue == "cloud")
            {
                using (var client = new SftpClient(host, port, username, password))
                {
                    string directorys = "";

                    client.Connect();
                    if (client.IsConnected)
                    {

                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {

                            foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                            {
                                if (item.IsDirectory)
                                {
                                    directorys = item.Name;
                                }

                                foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directorys))
                                {
                                    if (!itemdoc.IsDirectory)
                                    {

                                        if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                        {

                                            client.Delete("/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directorys + "/" + itemdoc.Name);
                                            break;
                                        }

                                    }

                                }

                            }
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < directory.Length; i++)
                {
                    if (selectedNodes[0].HasChildren == false)
                    {

                        if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                        {
                            FileSystem.DeleteFile(directory[i],
                            Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                            Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin,
                            Microsoft.VisualBasic.FileIO.UICancelOption.ThrowException);
                            break;

                        }
                    }


                }
            }


            treeList.DeleteNode(treeList.FocusedNode);
            MessageBox.Show("Item Deleted");
            treeList.RefreshDataSource();
            treeList.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList.Nodes)
            {
                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = color;

                }
                else
                {
                    node.StateImageIndex = color;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }


        }


        public void treelistAddItem(string subfolder, TreeList treeList, int color)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;

            string first = string.Empty;
            var host = "imspulse.com";
            var port = 22;
            var username = "farai";
            var password = "@Paradice1";

            var filePath = string.Empty;
            if (selectedNodes[0].ParentNode == null)
            {


                xtraOpenFileDialog1.InitialDirectory = "c:\\";
                xtraOpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*|Word 2003 (*.doc)|*.doc|Word 2007 (*.docx)|*.docx|Excel 2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx|Powerpoint 2003 (*.ppt)|*.ppt|Powerpoint 2007 (*.pptx)|*.pptx";
                xtraOpenFileDialog1.FilterIndex = 2;
                xtraOpenFileDialog1.RestoreDirectory = true;

                if (xtraOpenFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = xtraOpenFileDialog1.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = xtraOpenFileDialog1.OpenFile();

                    Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                    Worksheet sheet = work.Worksheets[1];

                    if (sheet.Cells["B1"].StringValue == "cloud")
                    {
                        using (var client = new SftpClient(host, port, username, password))
                        {
                            string directorys = "";

                            client.Connect();
                            if (client.IsConnected)
                            {

                                if (File.Exists(@"C:\Reader\Loader.xlsx"))
                                {
                                    if (subfolder.Contains("QMS"))
                                    {
                                        subfolder = "/QMS";
                                    }
                                    else if (subfolder.Contains("Resources"))
                                    {
                                        subfolder = "/Resources";
                                    }
                                    else if (subfolder.Contains("Operations"))
                                    {
                                        subfolder = "/Operations";
                                    }


                                    foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + subfolder + @"/"))
                                    {
                                        if (item.IsDirectory)
                                        {

                                            if (item.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                            {

                                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                                if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + subfolder + @"/" + item.Name + "/" + Path.GetFileName(filePath)))
                                                {

                                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + subfolder + @"/" + item.Name + "/");
                                                    client.UploadFile(fileStream, Path.GetFileName(filePath));
                                                }
                                            }

                                        }



                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        File.Copy(filePath, ServerPath.PathLocation() + @"MC QMS\" + subfolder + @"\" + selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString() + @"\" + Path.GetFileName(filePath));
                    }

                }
                if (filePath != string.Empty)
                {
                    TreeListNode newNode = treeList.AppendNode(new object[] { "New first child" }, treeList.FocusedNode);
                    treeList.SetNodeIndex(newNode, 20001);
                    newNode.SetValue(2, Path.GetFileName(filePath));
                    treeList.OptionsBehavior.Editable = !treeList.OptionsBehavior.Editable;
                    treeList.RefreshDataSource();
                    MessageBox.Show("Document Added");
                    treeList.StateImageList = imageCollection1;
                }


                foreach (TreeListNode node in treeList.Nodes)
                {


                    // first node
                    if (node.HasChildren)
                    {
                        node.StateImageIndex = color;

                    }
                    else
                    {
                        node.StateImageIndex = color;
                    }
                    foreach (TreeListNode mumba in node.Nodes)
                    {
                        if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                        {
                            mumba.StateImageIndex = 1;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                        {
                            mumba.StateImageIndex = 2;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                        {
                            mumba.StateImageIndex = 3;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                        {
                            mumba.StateImageIndex = 4;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                        {
                            mumba.StateImageIndex = 5;
                        }
                        else
                        {
                            mumba.StateImageIndex = 6;
                        }
                    }

                }
            }
            else
            {
                MessageBox.Show("This is not a folder. You need to select a folder and then add a document to it.");
            }
        }

        private void bbAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\QMS", treeList1, 7);
        }

        private void opAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\Operations", treeList3, 9);
        }

        private void huAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\Resources", treeList2, 8);
        }

        private void bbReadonly_ItemClick(object sender, EventArgs e)
        {

            MakeReadonly(treeList1, 7, @"/QMS");

        }

        private void huReadonly_ItemClick(object sender, EventArgs e)
        {

            MakeReadonly(treeList1, 8, @"/Resources");

        }
        private void opReadonly_ItemClick(object sender, EventArgs e)
        {

            MakeReadonly(treeList1, 9, @"/Operations");

        }

        private void bbRename_ItemClick(object sender, EventArgs e)
        {

            Rename(treeList1, 7, @"/QMS");

        }
        private void huRename_ItemClick(object sender, EventArgs e)
        {

            Rename(treeList2, 8, @"/Resources");

        }
        private void opRename_ItemClick(object sender, EventArgs e)
        {

            Rename(treeList3, 9, @"/Operations");

        }

        public void Rename(TreeList treeList, int color, string folder)
        {
            try
            {
                TreeListMultiSelection selectedNodes = treeList.Selection;

                string result = string.Empty;
                //Proper way to open a protected worksheet with password
                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                Worksheet sheet = work.Worksheets[1];
                string first = string.Empty;
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";

                if (selectedNodes[0].HasChildren == false)
                {

                    using (var client = new SftpClient(host, port, username, password))
                    {
                        string directorys = "";

                        client.Connect();
                        if (client.IsConnected)
                        {

                            if (File.Exists(@"C:\Reader\Loader.xlsx"))
                            {

                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                                {
                                    if (item.IsDirectory)
                                    {
                                        directorys = item.Name;

                                        if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys))
                                        {
                                            Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys);
                                        }
                                    }

                                    foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directorys))
                                    {
                                        if (!itemdoc.IsDirectory)
                                        {

                                            if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                            {

                                                // initialize a new XtraInputBoxArgs instance
                                                XtraInputBoxArgs args = new XtraInputBoxArgs();
                                                // set required Input Box options
                                                args.Caption = "Rename Document";
                                                args.Prompt = "Note: Renaming can take time.";
                                                args.DefaultButtonIndex = 0;
                                                // initialize a DateEdit editor with custom settings
                                                TextEdit editor = new TextEdit();

                                                args.Editor = editor;
                                                // a default DateEdit value
                                                args.DefaultResponse = itemdoc.Name;
                                                // display an Input Box with the custom editor
                                                result = XtraInputBox.Show(args).ToString();

                                                client.RenameFile(itemdoc.FullName, "/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + "/" + folder + "/" + directorys + "/" + result);
                                                break;
                                            }

                                        }

                                    }

                                }
                            }
                        }
                    }
                }

                selectedNodes[0].SetValue(treeList.Columns[2], result);
                treeList.RefreshDataSource();
                treeList.StateImageList = imageCollection1;

                foreach (TreeListNode node in treeList.Nodes)
                {


                    // first node
                    if (node.HasChildren)
                    {
                        node.StateImageIndex = color;

                    }
                    else
                    {
                        node.StateImageIndex = color;
                    }
                    foreach (TreeListNode mumba in node.Nodes)
                    {
                        if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                        {
                            mumba.StateImageIndex = 1;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                        {
                            mumba.StateImageIndex = 2;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                        {
                            mumba.StateImageIndex = 3;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                        {
                            mumba.StateImageIndex = 4;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                        {
                            mumba.StateImageIndex = 5;
                        }
                        else
                        {
                            mumba.StateImageIndex = 6;
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
 
        }


        public void MakeReadonly(TreeList treeList, int color, string folder)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;


            string primeFolder = @"MC QMS";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
            try
            {

                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                Worksheet sheet = work.Worksheets[1];

                if (sheet.Cells["B1"].StringValue == "cloud")
                {
                    string first = string.Empty;
                    var host = "imspulse.com";
                    var port = 22;
                    var username = "farai";
                    var password = "@Paradice1";


                    if (selectedNodes[0].HasChildren == false)
                    {

                        using (var client = new SftpClient(host, port, username, password))
                        {
                            string directorys = "";

                            client.Connect();
                            if (client.IsConnected)
                            {

                                if (File.Exists(@"C:\Reader\Loader.xlsx"))
                                {

                                    foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + @"/"))
                                    {
                                        if (item.IsDirectory)
                                        {
                                            directorys = item.Name;

                                            if (!Directory.Exists(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys))
                                            {
                                                Directory.CreateDirectory(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys);
                                            }
                                        }

                                        foreach (var itemdoc in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + folder + "/" + directorys))
                                        {
                                            if (!itemdoc.IsDirectory)
                                            {


                                                if (itemdoc.Name.Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                                                {

                                                    using (Stream fileStream = File.Create(Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys + "/" + itemdoc.Name))
                                                    {
                                                        client.DownloadFile(itemdoc.FullName, fileStream);

                                                    }

                                                    string path = Application.StartupPath + "/IMS Pulse/" + "MC QMS" + folder + "/" + directorys + "/" + itemdoc.Name;
                                                    // initialize a new XtraInputBoxArgs instance
                                                    XtraInputBoxArgs args = new XtraInputBoxArgs();
                                                    // set required Input Box options
                                                    args.Caption = "Password Protection";
                                                    args.Prompt = "Enter Password To Make Readonly";
                                                    args.DefaultButtonIndex = 0;
                                                    // initialize a DateEdit editor with custom settings
                                                    TextEdit editor = new TextEdit();

                                                    args.Editor = editor;
                                                    // a default DateEdit value
                                                    args.DefaultResponse = "My Password";
                                                    // display an Input Box with the custom editor
                                                    var result = XtraInputBox.Show(args).ToString();

                                                    if (path.Contains(".doc"))
                                                    {

                                                        Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                                                        opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                                                        opt.Password = result;
                                                        Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                                        Aspose.Words.Document docu = new Aspose.Words.Document(path, getum12);
                                                        docu.Protect(Aspose.Words.ProtectionType.ReadOnly, result);
                                                        docu.Save(path);
                                                    }
                                                    if (path.Contains(".xls"))
                                                    {
                                                        Aspose.Cells.LoadOptions getum3 = new Aspose.Cells.LoadOptions { Password = result };
                                                        Workbook workt = new Workbook(path, getum3);

                                                        workt.Settings.WriteProtection.Password = result;

                                                        workt.Save(path);
                                                    }
                                                    if (path.Contains(".ppt"))
                                                    {
                                                        Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                                        using (Presentation presentation = new Presentation(path, getpere))
                                                        {
                                                            presentation.ProtectionManager.SetWriteProtection(result);

                                                            presentation.Save(path, Aspose.Slides.Export.SaveFormat.Pptx);
                                                        }
                                                    }

                                                    Workbook cloudbooker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                    Worksheet cloudsheet = cloudbooker.Worksheets[0];

                                                    if (cloudsheet.Cells.MaxDataRow == 0)
                                                    {
                                                        cloudsheet.Cells[1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[1, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[1, 2].PutValue(folder);
                                                    }
                                                    else
                                                    {

                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow + 1, 0].PutValue(itemdoc.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 1].PutValue(item.Name);
                                                        cloudsheet.Cells[cloudsheet.Cells.MaxDataRow, 2].PutValue(folder);
                                                    }



                                                    //cloudsheet.Cells["A" + row].PutValue(itemdoc.Name);

                                                    cloudbooker.Save(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                                                    break;
                                                }


                                            }
                                        }

                                    }

                                }
                            }
                        }

                        //Document doc = new Document(@"/var/www/html/bunch-box/Word.docx");
                        //String docText = doc.ToString(Aspose.Words.SaveFormat.Text).Trim()              
                    }
                }
                else
                {
                    for (int i = 0; i < directory.Length; i++)
                    {
                        if (selectedNodes[0].HasChildren == false)
                        {

                            if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                            {
                                // initialize a new XtraInputBoxArgs instance
                                XtraInputBoxArgs args = new XtraInputBoxArgs();
                                // set required Input Box options
                                args.Caption = "Password Protection";
                                args.Prompt = "Enter Password To Decrypt";
                                args.DefaultButtonIndex = 0;
                                // initialize a DateEdit editor with custom settings
                                TextEdit editor = new TextEdit();

                                args.Editor = editor;
                                // a default DateEdit value
                                args.DefaultResponse = "My Password";
                                // display an Input Box with the custom editor
                                var result = XtraInputBox.Show(args).ToString();

                                if (directory[i].Contains(".doc"))
                                {
                                    Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                                    Aspose.Words.Document docu = new Aspose.Words.Document(directory[i], getum12);
                                    docu.Unprotect();
                                    docu.Save(directory[i]);
                                }
                                if (directory[i].Contains(".xls"))
                                {
                                    Aspose.Cells.LoadOptions getums = new Aspose.Cells.LoadOptions { Password = result };
                                    Workbook worsk = new Workbook(directory[i], getums);

                                    worsk.Settings.Password = null;

                                    worsk.Save(directory[i]);
                                }
                                if (directory[i].Contains(".ppt"))
                                {
                                    Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                    using (Presentation presentation = new Presentation(directory[i], getpere))
                                    {
                                        presentation.ProtectionManager.RemoveEncryption();
                                        presentation.Save(directory[i], Aspose.Slides.Export.SaveFormat.Pptx);
                                    }
                                }
                                if (directory[i].Contains(".pdf"))
                                {
                                    Aspose.Pdf.Document document = new Aspose.Pdf.Document(directory[i], result);
                                    document.Decrypt();
                                    document.Save(directory[i]);
                                }

                            }
                        }

                    }


                    treeList.RefreshDataSource();
                    treeList.StateImageList = imageCollection1;

                    foreach (TreeListNode node in treeList.Nodes)
                    {


                        // first node
                        if (node.HasChildren)
                        {
                            node.StateImageIndex = color;

                        }
                        else
                        {
                            node.StateImageIndex = color;
                        }
                        foreach (TreeListNode mumba in node.Nodes)
                        {
                            if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                            {
                                mumba.StateImageIndex = 1;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                            {
                                mumba.StateImageIndex = 2;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                            {
                                mumba.StateImageIndex = 3;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                            {
                                mumba.StateImageIndex = 4;
                            }
                            else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                            {
                                mumba.StateImageIndex = 5;
                            }
                            else
                            {
                                mumba.StateImageIndex = 6;
                            }
                        }

                    }
                }
                MessageBox.Show("Item Decrypted");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void treeList1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList1.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList1.Selection;

                    Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                    Worksheet sheet = work.Worksheets[1];

                    if (sheet.Cells["B1"].StringValue == "cloud")
                    {

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", bbAddChild_ItemClick));

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", bbDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", bbEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", bbDecrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Make Readonly", bbReadonly_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Rename Document", bbRename_ItemClick));

                    }
                    else
                    {
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", bbAddChild_ItemClick));

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", bbDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", bbEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", bbDecrypt_ItemClick));

                    }

                }
            }
        }

        private void treeList1_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"/QMS", treeList1);
        }

        private void treeList2_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList3.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList3.Selection;

                    Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                    Worksheet sheet = work.Worksheets[1];


                    if (sheet.Cells["B1"].StringValue == "cloud")
                    {

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", huAddChild_ItemClick));
                         e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", huDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", huEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", huDecrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Make Readonly", huReadonly_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Rename Document", huRename_ItemClick));

                    }
                    else
                    {

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", huAddChild_ItemClick));

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", huDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", huEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", huDecrypt_ItemClick));

                    }

                }
            }
        }

        private void treeList2_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"/Resources", treeList2);
        }

        private void treeList3_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList3.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList3.Selection;

                    Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
                    Worksheet sheet = work.Worksheets[1];

                    if (sheet.Cells["B1"].StringValue == "cloud")
                    {

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", opAddChild_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", opDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", opEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", opDecrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Make Readonly", opReadonly_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Rename Document", opRename_ItemClick));

                    }
                    else
                    {

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", opAddChild_ItemClick));

                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", opDelete_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", opEncrypt_ItemClick));
                        e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", opDecrypt_ItemClick));

                    }

                }
            }
        }

        private void treeList3_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"/Operations", treeList3);
        }
    }
}
