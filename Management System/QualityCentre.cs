﻿using Aspose.Cells;
using C1.Win.C1Themes;
using DocumentManager;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Management_System
{
    public partial class QualityCentre : C1.Win.C1Ribbon.C1RibbonForm
    {
        NetworkCredential networkCredential = new NetworkCredential("Cinfratec", "admincinfratec");

        public QualityCentre()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            InitializeComponent();      
        }
       
        private void DocEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
         
        }
        
        public void Themes()
        {
            rcmbThemes.Items.Clear();
            string[] themes = C1ThemeController.GetThemes();
            foreach (string theme in themes)
                rcmbThemes.Items.Add(theme);
        }

        public void DueDates()
        {
            try
            {
                string path = ServerPath.PathLocation() + @"Shared\CAR LOG.xlsx";
                string Late = "";
                int todayIs = DateTime.Today.Day;
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];

                if (sheet.Cells[3, 0].Type != CellValueType.IsNull)
                {
                    for (int i = 3; i < sheet.Cells.Rows.Count; i++)
                    {

                        if (sheet.Cells[i, 1].Type != CellValueType.IsNull)
                        {
                            int dueDate = sheet.Cells[i, 9].DateTimeValue.Day;

                            if (todayIs < dueDate)
                            {
                                int answer = todayIs - dueDate;

                                if (answer < 5)
                                {
                                    //Late += string.Format("{ 0} \t {1}", dataTable.Rows[i]["F6"].ToString(), dataTable.Rows[i]["F15"].ToString()) + Environment.NewLine;
                                    Late = "Some CAR Entries are approaching their due dates. Please open the CAR/NCR for more details.";

                                }
                                MessageBox.Show(Late, "Due dates are approaching", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }


                        }
                        break;
                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadProces()
        {
            string isChecked = File.ReadAllText(@"C:\Management\Management System\ID_46664\Mula.txt");

            if (isChecked.Contains("Checked"))
            {
                DueDates();
            }
        }

        public void Log()
        {
           
                string filenameUser = ServerPath.PathLocation() + @"Management\Management System\ID_98889\UserLog.txt";

                using (StreamWriter w = File.AppendText(filenameUser))
                {
                    w.WriteLine("Date:  " + DateTime.Now.ToShortDateString() + "\t\t  " + "User Name:  " + Environment.UserName.ToString() + "\t\t  " + "Management Centre: " + this.Text + "\t\t\t  " + "PC:  " + Environment.MachineName.ToString() + Environment.NewLine);
                }
        
        }



        private void QualityCentre_Load(object sender, EventArgs e)
        {
    

            ribbonButton3.Text = "IMS Pulse Revision Master" + "\u2122";
          
          
       
         

            tlsDate.Text = DateTime.Now.ToShortDateString();
            tlsUserName.Text = Environment.UserName.ToString();
            tlsSystemName.Text = Environment.MachineName.ToString();
            Themes();

            //using (new NetworkConnection(@"\\SERVER-PC\ISO 9001", networkCredential))
            //{
            //    string[] theFolders = Directory.GetDirectories(@"\\SERVER-PC\ISO 9001");
            //    string hold = "";

            //    for (int i = 0; i < theFolders.Length; i++)
            //    {
            //        hold += theFolders[i] + "  \n";
            //    }
            //    if (hold != null)
            //    {
            
                

                        hrQuality1.Hide();
                        procedureQuality1.Hide();
                        policyQuality1.Hide();
                        pfQuality1.Hide();
                        Log();
                        ReadProces();
                
                    
            //    }
            //}

        }

        private void tslRedo_Click(object sender, EventArgs e)
        {
            //if (richTextBox1.CanRedo)
            //{
            //    richTextBox1.Redo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to redo.", "Cannot Redo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void tlsUndo_Click(object sender, EventArgs e)
        {
            //if(richTextBox1.CanUndo)
            //{
            //    richTextBox1.Undo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to undo.", "Cannot Undo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}    
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //if(e.Modifiers == Keys.Control && e.KeyCode == Keys.Z)
            //{
            //    if (richTextBox1.CanUndo)
            //    {
            //        richTextBox1.Undo();
            //        e.SuppressKeyPress = true;
            //    }
            //     else
            //    {
            //        MessageBox.Show("There is nothing to Undo.", "Cannot Undo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}

            //if (e.Modifiers == Keys.Control && e.KeyCode == Keys.Y)
            //{
            //    if (richTextBox1.CanRedo)
            //    {
            //        richTextBox1.Redo();
            //        e.SuppressKeyPress = true;
            //    }
            //    else
            //    {
            //        MessageBox.Show("There is nothing to Redo.", "Cannot Redo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }
            //}

            //if(e.KeyCode == Keys.Delete)
            //{
            //    richTextBox1.Clear();
            //}
        }

        private void tlsRemove_Click(object sender, EventArgs e)
        {
            //richTextBox1.Clear();
        }

        private void tlsTools_Click(object sender, EventArgs e)
        {
          
        }

        private void tlsNew_Click(object sender, EventArgs e)
        {
            NewDocument newDocument = new NewDocument();
            
            if(Application.OpenForms["newDocument"] as NewDocument == null)
            {
                newDocument.ShowDialog();
                newDocument.TopMost = true;   
            }
  
        }

        private void tlsEditDocument_Click(object sender, EventArgs e)
        {
            EditDocument editDocument = new EditDocument();

            if(Application.OpenForms["editDocument"] as EditDocument == null)
            {
                editDocument.ShowDialog();
                editDocument.TopMost = true;
            }
        }

        private void tlsSettings_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            
            if(Application.OpenForms["settings"] as Settings == null)
            {
                settings.ShowDialog();
                settings.TopMost = true;
            }
        }

        private void tlsPrint_Click(object sender, EventArgs e)
        {
            PrintSettingsSafety printSettings = new PrintSettingsSafety();

            if(Application.OpenForms["printSettings"] as PrintSettingsSafety == null)
            {
                printSettings.ShowDialog();
                printSettings.TopMost = true;
            }
        }

        private void tlsMainCentre_Click(object sender, EventArgs e)
        {
            MainForm mainForm = new MainForm();
            this.Visible = false;
            mainForm.Show();
        }

        private void tlsExit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to exit?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if(dialogResult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void tlsReplaceWord_Click(object sender, EventArgs e)
        {
            ReplaceWord replaceWord = new ReplaceWord();

            if(Application.OpenForms["replaceWord"] as ReplaceWord == null)
            {
                replaceWord.ShowDialog();
                replaceWord.TopMost = true;
            }
        }

        private void tlsReplaceExcel_Click(object sender, EventArgs e)
        {
            ReplaceExcel replaceExcel = new ReplaceExcel();

            if (Application.OpenForms["replaceExcel"] as ReplaceExcel == null)
            {
                replaceExcel.ShowDialog();
                replaceExcel.TopMost = true;
            }
        }

        private void tlsCode_Click(object sender, EventArgs e)
        {
            DocumentCode documentCode = new DocumentCode();

            if(Application.OpenForms["documentCode"] as DocumentCode == null)
            {
                documentCode.ShowDialog();
                documentCode.TopMost = true;
            }
        }

        private void tlsLocation_Click(object sender, EventArgs e)
        {
            LocationChanger locationChanger = new LocationChanger();

            if(Application.OpenForms["locationChanger"] as LocationChanger == null)
            {
                locationChanger.ShowDialog();
                locationChanger.TopMost = true;
            }
        }

        private void tlsAdvancedCar_Click(object sender, EventArgs e)
        {
           
        }

        private void tlsNCRandCar_Click(object sender, EventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();

            if(Application.OpenForms["nCRandCAR"] as NCRandCAR == null)
            {
                nCRandCAR.ShowDialog();
                nCRandCAR.TopMost = true;
            }
        }

        private void tlsReports_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();

            if(Application.OpenForms["reports"] as Reports == null)
            {
                reports.ShowDialog();
            }
        }

        private void tlsRegisters_Click(object sender, EventArgs e)
        {
            CustomDocs registers = new CustomDocs();

            if(Application.OpenForms["registers"] as CustomDocs == null)
            {
                registers.ShowDialog();
                registers.TopMost = true;
            }
        }

        private void tlsFormat_Click(object sender, EventArgs e)
        {
            DocumentFormat format = new DocumentFormat();

            if(Application.OpenForms["format"] as DocumentFormat == null)
            {
                format.ShowDialog();
                format.TopMost = true;
            }
        }

        private void tlsAbout_Click(object sender, EventArgs e)
        {
            About about = new About();

            if(Application.OpenForms["about"] as About == null)
            {
                about.ShowDialog();
                about.TopMost = true;
            }
        }

        private void tlsQMS_Click(object sender, EventArgs e)
        {
       
        }

        private void QualityCentre_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtSearch_OnTextChange(object sender, EventArgs e)
        {
            //if (txtSearch.text == "")
            //{
            //    lstItem.Items.Clear();
            //    lstItem.Items.Add("============== Word Documentation ===============");
            //    lstItem.Items.Add("");
            //    LoadItems();
            //    lstItem.Items.Add("");
            //    lstItem.Items.Add("============== Excel Documentation ===============");
            //    lstItem.Items.Add("");
            //    LoadExcel();
            //    lstItem.Items.Add("");
            //    lstItem.Items.Add("============ PowerPoint Documentation =============");
            //    lstItem.Items.Add("");
            //    LoadPower();
            //    lstItem.Items.Add("");
            //    lstItem.Items.Add("=============== Visio Documentation ================");
            //    lstItem.Items.Add("");
            //    LoadVisio();
            //    lstItem.Items.Add("");
            //    lstItem.Items.Add("=============== NEW Documentation ================");
            //    lstItem.Items.Add("");
            //    loadNew();
            //}
            //else
            //{
            //    lstItem.Items.Clear();
            //    FindMyStringInList(txtSearch.text);
            //}
        }

        private void lstItem_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //richTextBox1.Clear();
            //DocumentExtensions document = new DocumentExtensions();

            //document.DocumentInfo(lstItem, lblDoc, lblCode, lblFormat, picBar);

            //document.SwitchView(lstItem, richTextBox1, dataCell);

            //if(lstItem.SelectedItem.ToString().Contains("doc") || lstItem.SelectedItem.ToString().Contains("docx"))
            //{
            //    try
            //    {
            //        WordWrapper word = new WordWrapper();
            //        word.DisplayOnePage(richTextBox1, lstItem, groupBox2);
            //        ribbonButton3.Enabled = true;
            //    }
            //    catch (Exception)
            //    {
            //        MessageBox.Show("New Documents cannot have a preview but you can open them", "No Preview Support", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //        groupBox2.Visible = false;
            //    }
                           
            //}

            //if (lstItem.SelectedItem.ToString().Contains("vsd") || lstItem.SelectedItem.ToString().Contains("vsdx"))
            //{
            //    try
            //    {
            //        VisioWrapper visioWrapper = new VisioWrapper();
            //        visioWrapper.DisplayVisio(richTextBox1, lstItem);
            //        ribbonButton3.Enabled = false;
            //    }
            //    catch (Exception)
            //    {

            //        MessageBox.Show("New Documents cannot have a preview but you can open them.", "No Preview Support", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //        groupBox2.Visible = false;
            //    }
              
            //}

            //if(lstItem.SelectedItem.ToString().Contains("REG-001"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocs(lstItem, dataCell, "Document INDEX");
            //    ribbonButton3.Enabled = false;
            //}

            //if (lstItem.SelectedItem.ToString().Contains("REG-002"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocs(lstItem, dataCell, "NCR");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("SCH"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocs(lstItem, dataCell, "Audit Schedule");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("Data Analysis (R1)"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Sheet1");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("MTX-001"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Competency Matrix");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("REG-003"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Sheet1");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("REG-007"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Front Page");
            //    ribbonButton3.Enabled = false;
            //}
            //if (lstItem.SelectedItem.ToString().Contains("REG-009"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Calibration");
            //    ribbonButton3.Enabled = false;
            //}

            //if (lstItem.SelectedItem.ToString().Contains("RISK"))
            //{
            //    ExcelWrapper excelWrapper = new ExcelWrapper();
            //    excelWrapper.DisplayExcelDocsNew(lstItem, dataCell, "Risk_Application");
            //    ribbonButton3.Enabled = false;
            //}


        }

        private void contactUsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
           
        }

        private void richTextBox1_ContentsResized(object sender, ContentsResizedEventArgs e)
        {

            //const int margin = 5;
            //RichTextBox rch = sender as RichTextBox;
            //rch.ClientSize = new Size(
            //    e.NewRectangle.Width + margin,
            //    e.NewRectangle.Height + margin);
        }

        private void toolStripMenuItem21_Click(object sender, EventArgs e)
        {
            IMSDocEditor docEditor = new IMSDocEditor();
            docEditor.ShowDialog();
        }

        private void tlsViewDocEditor_Click(object sender, EventArgs e)
        {
           
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ribbonButton5_Click(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            main.MovePanelToFront();
            main.Show();
            this.Hide();
        }

        private void ribbonButton4_Click(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            main.Show();
            this.Hide();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            //try
            //{

            //    string filename = @"C:\Quality Management\NewDocuments.accdb";
            //    OleDbCommand cmd = new OleDbCommand();
            //    OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename));
            //    string sql = "Select DocumentPath FROM NewDocument";
            //    con.Open();
            //    cmd = new OleDbCommand(sql, con);
            //    OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
            //    DataTable dataTable = new DataTable();
            //    adaptor.Fill(dataTable);


            //    for (int i = 0; i < dataTable.Rows.Count; i++)
            //    {
            //        if (dataTable.Rows[i]["DocumentPath"].ToString().Contains(lstItem.SelectedItem.ToString()))
            //        {
            //            string file = @"C:\Quality Management\" + lstItem.SelectedItem.ToString();

            //            Process.Start(file);
            //            break;
            //        }
            //    }

            //    con.Close();

            //    if (lstItem.SelectedItem.ToString() == "")
            //    {
            //        MessageBox.Show("Please select a valid document.", "No Valid Document Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    }
            //    else if(lstItem.SelectedItem.ToString() != "")
            //    {
            //        try
            //        {
            //            string filepath = @"C:\Quality Management\QMS\" + lstItem.SelectedItem.ToString();

            //            Process.Start(filepath);
            //        }
            //        catch (Exception)
            //        {

            //            for (int i = 0; i < dataTable.Rows.Count; i++)
            //            {
            //                if (dataTable.Rows[i]["DocumentPath"].ToString().Contains(lstItem.SelectedItem.ToString()))
            //                {
            //                    string file = @"C:\Quality Management\" + lstItem.SelectedItem.ToString();
            //                    groupBox2.Visible = false;
            //                    Process.Start(file);
            //                    break;
            //                }
            //            }
            //        }
                   
            //    }
            //}
            //catch (Exception)
            //{

            //    MessageBox.Show("Please select a valid document.", "No Valid Document Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //}
            
           
        }

        private void rcmbThemes_DropDown(object sender, EventArgs e)
        {
            Themes();
        }

        private void rcmbThemes_ChangeCommitted(object sender, EventArgs e)
        {
            C1Theme theme = C1ThemeController.GetThemeByName(rcmbThemes.Text, false);
            if (theme != null)
                C1ThemeController.ApplyThemeToControlTree(this, theme);           
        }

        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }
        public void ValidateUsers()
        {

            if (isAdmin() == "admin")
            {
                UserLog userLog = new UserLog();
                userLog.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void EditRisk()
        {
            if (isAdmin() == "admin")
            {
                Advanced_Risk_Register register = new Advanced_Risk_Register();
                register.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void EditCorrective()
        {
            if (isAdmin() == "admin")
            {
                AdvancedCorrective advancedCorrective = new AdvancedCorrective();
                advancedCorrective.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use the NCR/CAR feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ribbonButton10_Click(object sender, EventArgs e)
        {
            EditCorrective();

        }

        private void ribbonButton11_Click(object sender, EventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();
            nCRandCAR.ShowDialog();
        }

        private void ribbonButton7_Click(object sender, EventArgs e)
        {
            ValidateUsers();
        }

        private void ribbonButton6_Click(object sender, EventArgs e)
        {
            //if (richTextBox1.CanUndo)
            //{
            //    richTextBox1.Undo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to undo.", "Cannot Undo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void ribbonButton19_Click(object sender, EventArgs e)
        {
            //if (richTextBox1.CanRedo)
            //{
            //    richTextBox1.Redo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to redo.", "Cannot Redo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            EditDocument editDocument = new EditDocument();
            editDocument.ShowDialog();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            NewDocument newDocument = new NewDocument();
            newDocument.ShowDialog();
        }

        private void ribbonButton18_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(@"C:\MC QMS\ISO 9001 QMS(1).ppsm");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void ribbonButton14_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.imsglobal.co.za");
        }

        private void ribbonButton16_Click(object sender, EventArgs e)
        {

        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            //    string file = @"C:\Quality Management\QMS\" + lstItem.SelectedItem.ToString();
            //    Microsoft.Office.Interop.Word.Document doc = word.Documents.Open(file);
            //    if (doc == null)
            //    {
            //        // here i assume fileName has been assigned
            //        doc = word.Documents.Open(file, ReadOnly: true, Visible: false);
            //    }
            //    doc.PrintOut();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Cannot Do That", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //}
            try
            {
                //Document doc = new Document(hrQuality1.PrintableDocument());
                //doc.Print();

               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
          
        }

        void ToggleSelectionFontStyle(FontStyle fontStyle)
        {
            //if (this.richTextBox1.SelectionFont == null)
            //{
            //    MessageBox.Show("Cannot change font style while selected text has more than one font.");
            //}
            //else
            //{
            //    this.richTextBox1.SelectionFont = new Font(this.richTextBox1.SelectionFont,
            //        this.richTextBox1.SelectionFont.Style ^ fontStyle);
            //}
        }

        private void UpdateFontGroupBasedOnCurrentTextSelection()
        {
            //Font font = this.richTextBox1.SelectionFont;
            //bool none = font == null;
            //ribbonToggleButton1.Pressed = none ? false : font.Bold;
            //ribbonToggleButton2.Pressed = none ? false : font.Italic;
            //ribbonToggleButton3.Pressed = none ? false : font.Underline;
        }

        private void ribbonToggleButton1_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Bold);
        }

        private void ribbonToggleButton2_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Italic);
        }

        private void ribbonToggleButton3_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Underline);
        }

        private void ribbonButton8_Click(object sender, EventArgs e)
        {
            LocationChanger locationChanger = new LocationChanger();
            locationChanger.ShowDialog();
        }

        private void ribbonButton9_Click(object sender, EventArgs e)
        {
            DocumentCode document = new DocumentCode();
            document.ShowDialog();
        }

        private void ribbonButton15_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void ribbonButton12_Click(object sender, EventArgs e)
        {
            RiskRegister riskRegister = new RiskRegister();
            riskRegister.ShowDialog();
        }

        private void ribbonButton13_Click(object sender, EventArgs e)
        {
            EditRisk();
        }

        private void btnForm_Click(object sender, EventArgs e)
        {
            if (hrQuality1.Visible == true)
            {
                hrQuality1.Hide();
                btnForm.color = Color.SteelBlue;
                pictureBox2.Show();
                label1.Show();
                label2.Show();
                label3.Show();
                hrQuality1.ResetAll();
            }
            else
            {
                btnForm.color = Color.LightSteelBlue;
                btnProcedure.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                hrQuality1.Show();
                procedureQuality1.Hide();
                policyQuality1.Hide();
                pictureBox2.Hide();
                pfQuality1.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void btnProcedure_Click(object sender, EventArgs e)
        {
            if (procedureQuality1.Visible == true)
            {                
                procedureQuality1.Hide();
                btnProcedure.color = Color.SteelBlue;
                pictureBox2.Show();
                label1.Show();
                label2.Show();
                label3.Show();
                procedureQuality1.ResetAll();
            }
            else
            {
                btnProcedure.color = Color.LightSteelBlue;
                btnForm.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                procedureQuality1.Show();
                policyQuality1.Hide();
                hrQuality1.Hide();
                pfQuality1.Hide();
                pictureBox2.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void btnPol_Click(object sender, EventArgs e)
        {
            if (policyQuality1.Visible == true)
            {
                btnPol.color = Color.SteelBlue;
                policyQuality1.Hide();
                pictureBox2.Show();
                label1.Show();
                label2.Show();
                label3.Show();
                policyQuality1.ResetAll();
            }
            else
            {
                btnProcedure.color = Color.SteelBlue;
                btnForm.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                btnPol.color = Color.LightSteelBlue;
                policyQuality1.Show();
                procedureQuality1.Hide();
                hrQuality1.Hide();
                pfQuality1.Hide();
                pictureBox2.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (pfQuality1.Visible == true)
            {
                btnProcess.color = Color.SteelBlue;
                pfQuality1.Hide();
                pictureBox2.Show();
                label1.Show();
                label2.Show();
                label3.Show();
                pfQuality1.ResetAll();
            }
            else
            {
                btnProcedure.color = Color.SteelBlue;
                btnForm.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.LightSteelBlue;
                pfQuality1.Show();
                policyQuality1.Hide();
                procedureQuality1.Hide();
               
                hrQuality1.Hide();
                pictureBox2.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void ribbonButton22_Click(object sender, EventArgs e)
        {
            Email email = new Email();
            email.ShowDialog();
        }

        private void c1Ribbon1_RibbonEvent(object sender, C1.Win.C1Ribbon.RibbonEventArgs e)
        {

        }
      
        private void ribbonButton3_Click_1(object sender, EventArgs e)
        {
            if (isAdmin() == "admin")
            {
                RevisionQuality revisionSafety = new RevisionQuality();
                revisionSafety.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        
        }
     

        private void ribbonButton6_Click_1(object sender, EventArgs e)
        {
           
                if (isAdmin() == "admin")
                {
                    using (new NetworkConnection(@"\\BT-PC\IMS Pulse", networkCredential))
                    {
                    Process.Start(ServerPath.PathLocation() + "MC QMS");
                    }
                }
                else
                {
                    MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
