﻿using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraSplashScreen;
using DocumentManager;
using MySql.Data.MySqlClient;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TheRelease;

namespace Management_System
{
    public partial class QualitySection : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        FluentSplashScreenOptions op = new FluentSplashScreenOptions();

        public QualitySection()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));
            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));
            Stream stream3 = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            stream2.Seek(0, SeekOrigin.Begin);
            stream3.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            new Aspose.Words.License().SetLicense(stream2);
            new Aspose.Slides.License().SetLicense(stream3);
            InitializeComponent();
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            NewDocument newDocument = new NewDocument();
            newDocument.ShowDialog();
        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditDocument editDocument = new EditDocument();
            editDocument.ShowDialog();
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            saveFiles();
            MainForm main = new MainForm();
            main.Show();
            this.Hide();
        }

        private void barButtonItem6_ItemClick(object sender, ItemClickEventArgs e)
        {
            saveFiles();
            MainForm main = new MainForm();
            main.MovePanelToFront();
            main.Show();
            this.Hide();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            LocationChanger locationChanger = new LocationChanger();
            locationChanger.ShowDialog();
        }

        private void barButtonItem3_ItemClick(object sender, ItemClickEventArgs e)
        {
            DocumentCode document = new DocumentCode();
            document.ShowDialog();
        }

        private void barButtonItem7_ItemClick(object sender, ItemClickEventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();
            nCRandCAR.ShowDialog();
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditCorrective();
        }

        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }
        public void ValidateUsers()
        {

            if (isAdmin() == "admin")
            {
                UserLog userLog = new UserLog();
                userLog.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void EditRisk()
        {

                Advanced_Risk_Register register = new Advanced_Risk_Register();
                register.ShowDialog();

        }

        public void EditCorrective()
        {
 
                AdvancedCorrective advancedCorrective = new AdvancedCorrective();
                advancedCorrective.ShowDialog();

        }

        private void barButtonItem9_ItemClick(object sender, ItemClickEventArgs e)
        {
            RiskRegister riskRegister = new RiskRegister();
            riskRegister.ShowDialog();
        }

        private void barButtonItem10_ItemClick(object sender, ItemClickEventArgs e)
        {
            EditRisk();
        }

        private void barButtonItem11_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook works = new Workbook("C:/Reader/Loader.xlsx", getum);
                Worksheet sheets = works.Worksheets[1];
                if (sheets.Cells["B1"].StringValue != "cloud")
                {
                    RevisionQuality revisionSafety = new RevisionQuality();
                    revisionSafety.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("No support for cloud.");
                }
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void barButtonItem12_ItemClick(object sender, ItemClickEventArgs e)
        {
            ValidateUsers();
        }

        private void barButtonItem13_ItemClick(object sender, ItemClickEventArgs e)
        {
            Process.Start("http://www.imspulse.com");
        }

        private void barButtonItem14_ItemClick(object sender, ItemClickEventArgs e)
        {
            Email email = new Email();
            email.ShowDialog();
        }

        private void QualitySection_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void barButtonItem15_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                string primeFolder = @"MC QMS";

                string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
                // initialize a new XtraInputBoxArgs instance
                XtraInputBoxArgs args = new XtraInputBoxArgs();
                // set required Input Box options
                args.Caption = "Password Protection";
                args.Prompt = "Enter Password To Encrypt";
                args.DefaultButtonIndex = 0;
                // initialize a DateEdit editor with custom settings
                TextEdit editor = new TextEdit();

                args.Editor = editor;
                // a default DateEdit value
                args.DefaultResponse = "My Password";
                // display an Input Box with the custom editor
                var result = XtraInputBox.Show(args).ToString();



                op.Title = "IMS Pulse Is Busy Encrypting";
                op.Subtitle = "Please Be Patient...";
                op.RightFooter = "Encrypting Documents. This Can Take Several Minutes...";
                op.LeftFooter = "Copyright © 2021" + " Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
                op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                op.OpacityColor = Color.DarkOrange;
                op.Opacity = 60;
                op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                    op,
                    parentForm: this,
                    useFadeIn: true,
                    useFadeOut: true
                );

                for (int i = 0; i < directory.Length; i++)
                {


                    if (directory[i].Contains(".doc") && !directory[i].Contains("~"))
                    {

                        Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                        opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                        opt.Password = result;
                        Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                        Document docu = new Document(directory[i]);

                        docu.Save(directory[i], opt);
                    }
                    if (directory[i].Contains(".xls") && !directory[i].Contains("~"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                        Workbook work = new Workbook(directory[i], getum);

                        work.Settings.Password = result;

                        work.Save(directory[i]);
                    }

                }

                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        
        }

        private void barButtonItem16_ItemClick(object sender, ItemClickEventArgs e)
        {
            if(isAdmin() == "admin")
            {
                try
                {
                    string primeFolder = @"MC QMS";

                    string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
                    // initialize a new XtraInputBoxArgs instance
                    XtraInputBoxArgs args = new XtraInputBoxArgs();
                    // set required Input Box options
                    args.Caption = "Password Protection";
                    args.Prompt = "Enter Password To Decrypt";
                    args.DefaultButtonIndex = 0;
                    // initialize a DateEdit editor with custom settings
                    TextEdit editor = new TextEdit();

                    args.Editor = editor;
                    // a default DateEdit value
                    args.DefaultResponse = "My Password";
                    // display an Input Box with the custom editor
                    var result = XtraInputBox.Show(args).ToString();



                    op.Title = "IMS Pulse Is Busy Decrypting";
                    op.Subtitle = "Please Be Patient...";
                    op.RightFooter = "Encrypting Documents. This Can Take Several Minutes...";
                    op.LeftFooter = "Copyright © 2021 " + " Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
                    op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                    op.OpacityColor = Color.DarkOrange;
                    op.Opacity = 60;
                    op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                    DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                        op,
                        parentForm: this,
                        useFadeIn: true,
                        useFadeOut: true
                    );

                    for (int i = 0; i < directory.Length; i++)
                    {


                        if (directory[i].Contains(".doc") && !directory[i].Contains("~"))
                        {

                            Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                            Document docu = new Document(directory[i], getum12);
                            docu.Unprotect();
                            docu.Save(directory[i]);
                        }
                        if (directory[i].Contains(".xls") && !directory[i].Contains("~"))
                        {
                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                            Workbook work = new Workbook(directory[i], getum);

                            work.Settings.Password = "";

                            work.Save(directory[i]);
                        }

                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
       
           
        }

        private void barButtonItem17_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                try
                {
                    string primeFolder = @"MC QMS";

                    string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
                    // initialize a new XtraInputBoxArgs instance
                    XtraInputBoxArgs args = new XtraInputBoxArgs();
                    // set required Input Box options
                    args.Caption = "Password Protection";
                    args.Prompt = "Enter Password To Make Readonly";
                    args.DefaultButtonIndex = 0;
                    // initialize a DateEdit editor with custom settings
                    TextEdit editor = new TextEdit();

                    args.Editor = editor;
                    // a default DateEdit value
                    args.DefaultResponse = "My Password";
                    // display an Input Box with the custom editor
                    var result = XtraInputBox.Show(args).ToString();



                    op.Title = "IMS Pulse Is Busy Making Document Readonly";
                    op.Subtitle = "Please Be Patient...";
                    op.RightFooter = "Reading Documents. This Can Take Several Minutes...";
                    op.LeftFooter = "Copyright © 2021" + " Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
                    op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                    op.OpacityColor = Color.DarkOrange;
                    op.Opacity = 60;
                    op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                    DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                        op,
                        parentForm: this,
                        useFadeIn: true,
                        useFadeOut: true
                    );

                    for (int i = 0; i < directory.Length; i++)
                    {


                        if (directory[i].Contains(".doc") && !directory[i].Contains("~"))
                        {

                            Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                            opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                            opt.Password = result;
                            Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                            Document docu = new Document(directory[i]);
                            docu.Protect(Aspose.Words.ProtectionType.ReadOnly);
                            docu.Save(directory[i]);
                        }
                        if (directory[i].Contains(".xls") && !directory[i].Contains("~"))
                        {
                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                            Workbook work = new Workbook(directory[i], getum);

                            work.Settings.WriteProtection.Password = result;

                            work.Save(directory[i]);
                        }

                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
           
        }

        private void barButtonItem18_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                try
                {
                    string primeFolder = @"MC QMS";

                    string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
                    // initialize a new XtraInputBoxArgs instance
                    XtraInputBoxArgs args = new XtraInputBoxArgs();
                    // set required Input Box options
                    args.Caption = "Password Protection";
                    args.Prompt = "Enter Password To Make Document Editable";
                    args.DefaultButtonIndex = 0;
                    // initialize a DateEdit editor with custom settings
                    TextEdit editor = new TextEdit();

                    args.Editor = editor;
                    // a default DateEdit value
                    args.DefaultResponse = "My Password";
                    // display an Input Box with the custom editor
                    var result = XtraInputBox.Show(args).ToString();



                    op.Title = "IMS Pulse Is Busy Making Document Editable";
                    op.Subtitle = "Please Be Patient...";
                    op.RightFooter = "Enabling Documents. This Can Take Several Minutes...";
                    op.LeftFooter = "Copyright © 2021" + " Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
                    op.LoadingIndicatorType = FluentLoadingIndicatorType.Spinner;
                    op.OpacityColor = Color.DarkOrange;
                    op.Opacity = 60;
                    op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

                    DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                        op,
                        parentForm: this,
                        useFadeIn: true,
                        useFadeOut: true
                    );

                    for (int i = 0; i < directory.Length; i++)
                    {


                        if (directory[i].Contains(".doc") && !directory[i].Contains("~"))
                        {

                            Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                            opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                            opt.Password = result;
                            Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                            Document docu = new Document(directory[i], getum12);
                            docu.Unprotect();
                            docu.Save(directory[i]);
                        }
                        if (directory[i].Contains(".xls") && !directory[i].Contains("~"))
                        {
                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                            Workbook work = new Workbook(directory[i], getum);

                            work.Settings.WriteProtection.Password = "";

                            work.Save(directory[i]);
                        }

                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

            
        }

        private void QualitySection_Load(object sender, EventArgs e)
        {
            
            var dbCon = DBConnection.Instance();
            dbCon.Server = "92.205.25.31";
            dbCon.DatabaseName = "imspulse";
            dbCon.UserName = "manny";
            dbCon.Password = "@Paradice1";

            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];
            try
            {
                if (dbCon.IsConnect())
                {
                    //Get all users and compare that user to the login
                    string query = "SELECT * FROM imspulse.farai_document_codes where company_name = '" + sheet.Cells["A1"].StringValue + "';";
                    var cmd = new MySqlCommand(query, dbCon.Connection);
                    var reader = cmd.ExecuteReader();
                    if (reader.HasRows == false)
                    {
                        navigationPage2.Caption = "All Documents";

                        navigationPage1.Dispose();
                        navigationPage3.Dispose();
                        navigationPage4.Dispose();
                    }
                    reader.Close();
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Failed to establish contact with the server. IMS Pulse Will continue to try and load from the cloud. However, you might experience issues. Please check and make sure you have a decent internet connection");
            }
          

          

            if (isAdmin() != "admin")
            {
                barButtonItem18.Enabled = false;
                barButtonItem17.Enabled = false;
                barButtonItem16.Enabled = false;
                barButtonItem15.Enabled = false;
            }
        }

        private void QualitySection_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        public void saveFiles()
        {
            try
            {
                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                Workbook works = new Workbook("C:/Reader/Loader.xlsx", getum);
                Worksheet sheets = works.Worksheets[1];
                if (sheets.Cells["B1"].StringValue == "cloud")
                {


                    var host = "imspulse.com";
                    var port = 22;
                    var username = "farai";
                    var password = "@Paradice1";


                    using (var client = new SftpClient(host, port, username, password))
                    {
                        client.Connect();
                        Workbook worker = new Workbook(Application.StartupPath + @"\IMS Pulse\\MC QMS\" + "Record.xlsx");
                        Worksheet files = worker.Worksheets[0];


                        for (int i = 0; i <= files.Cells.MaxDataRow; i++)
                        {
                            using (var fileStream = new FileStream(Application.StartupPath + "/IMS Pulse/MC QMS/" + files.Cells[i, 2].StringValue + "/" + files.Cells[i, 1].StringValue + "/" + files.Cells[i, 0].StringValue, FileMode.Open))
                            {

                                client.BufferSize = 4 * 1024; // bypass Payload error large files

                                client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/" + files.Cells[i, 2].StringValue + "/" + files.Cells[i, 1].StringValue + "/");
                                client.UploadFile(fileStream, Path.GetFileName(files.Cells[i, 0].StringValue));


                            }
                        }
                    }
                   
                }

                MessageBox.Show("Your work was successfully saved", "Saved");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void barButtonItem19_ItemClick(object sender, ItemClickEventArgs e)
        {
            saveFiles();
        }
    }
}