﻿namespace Management_System
{
    partial class RevisionEnvironment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RevisionEnvironment));
            this.chkFocus = new DevExpress.XtraEditors.CheckEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.txtReplaceTo = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtReplace = new DevExpress.XtraEditors.TextEdit();
            this.txtFind = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtLabel = new DevExpress.XtraEditors.TextEdit();
            this.txtStandard = new DevExpress.XtraEditors.TextEdit();
            this.btnOpen = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkObselete = new DevExpress.XtraEditors.CheckEdit();
            this.btnRevise = new DevExpress.XtraEditors.SimpleButton();
            this.btnOpenO = new DevExpress.XtraEditors.SimpleButton();
            this.btnObselete = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtNumber = new DevExpress.XtraEditors.TextEdit();
            this.lstItem = new DevExpress.XtraEditors.ListBoxControl();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.chkFocus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkObselete.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // chkFocus
            // 
            this.chkFocus.Location = new System.Drawing.Point(509, 311);
            this.chkFocus.MenuManager = this.barManager1;
            this.chkFocus.Name = "chkFocus";
            this.chkFocus.Properties.Caption = "Focus On Automatic Updating";
            this.chkFocus.Size = new System.Drawing.Size(163, 20);
            this.chkFocus.TabIndex = 76;
            this.chkFocus.CheckedChanged += new System.EventHandler(this.chkFocus_CheckedChanged);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.barSubItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barStaticItem1,
            this.txtReplaceTo,
            this.barButtonItem4});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 9;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2});
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4)});
            this.bar1.Text = "Tools";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "About";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Replace Text";
            this.barButtonItem4.Id = 7;
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(828, 41);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 573);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(828, 20);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 41);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 532);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(828, 41);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 532);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 1;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "barButtonItem3";
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Find Text";
            this.barStaticItem1.Hint = "Find Text";
            this.barStaticItem1.Id = 4;
            this.barStaticItem1.Name = "barStaticItem1";
            // 
            // txtReplaceTo
            // 
            this.txtReplaceTo.Edit = this.repositoryItemTextEdit2;
            this.txtReplaceTo.Hint = "Replace Text";
            this.txtReplaceTo.Id = 6;
            this.txtReplaceTo.Name = "txtReplaceTo";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(287, 14);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(63, 13);
            this.labelControl8.TabIndex = 75;
            this.labelControl8.Text = "Replace With";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(169, 13);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(20, 13);
            this.labelControl7.TabIndex = 74;
            this.labelControl7.Text = "Find";
            // 
            // txtReplace
            // 
            this.txtReplace.Location = new System.Drawing.Point(287, 33);
            this.txtReplace.MenuManager = this.barManager1;
            this.txtReplace.Name = "txtReplace";
            this.txtReplace.Size = new System.Drawing.Size(100, 20);
            this.txtReplace.TabIndex = 73;
            // 
            // txtFind
            // 
            this.txtFind.Location = new System.Drawing.Point(169, 33);
            this.txtFind.MenuManager = this.barManager1;
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(100, 20);
            this.txtFind.TabIndex = 72;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(509, 187);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(25, 13);
            this.labelControl6.TabIndex = 71;
            this.labelControl6.Text = "Label";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(509, 128);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(44, 13);
            this.labelControl5.TabIndex = 70;
            this.labelControl5.Text = "Standard";
            // 
            // txtLabel
            // 
            this.txtLabel.Location = new System.Drawing.Point(509, 207);
            this.txtLabel.MenuManager = this.barManager1;
            this.txtLabel.Name = "txtLabel";
            this.txtLabel.Size = new System.Drawing.Size(119, 20);
            this.txtLabel.TabIndex = 69;
            // 
            // txtStandard
            // 
            this.txtStandard.Location = new System.Drawing.Point(509, 148);
            this.txtStandard.MenuManager = this.barManager1;
            this.txtStandard.Name = "txtStandard";
            this.txtStandard.Size = new System.Drawing.Size(119, 20);
            this.txtStandard.TabIndex = 68;
            // 
            // btnOpen
            // 
            this.btnOpen.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOpen.ImageOptions.Image")));
            this.btnOpen.Location = new System.Drawing.Point(669, 448);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(147, 62);
            this.btnOpen.TabIndex = 67;
            this.btnOpen.Text = "Open Document";
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(698, 187);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 13);
            this.labelControl4.TabIndex = 66;
            this.labelControl4.Text = "Revision No.";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(698, 128);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(37, 13);
            this.labelControl3.TabIndex = 65;
            this.labelControl3.Text = "Number";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(509, 60);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(78, 13);
            this.labelControl2.TabIndex = 64;
            this.labelControl2.Text = "Document Name";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 540);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(540, 13);
            this.labelControl1.TabIndex = 63;
            this.labelControl1.Text = "For more techincal users: Double click and copy the path below to the File Explor" +
    "er to access the document on it.";
            // 
            // chkObselete
            // 
            this.chkObselete.Location = new System.Drawing.Point(509, 354);
            this.chkObselete.MenuManager = this.barManager1;
            this.chkObselete.Name = "chkObselete";
            this.chkObselete.Properties.Caption = "Turn Off Prompt for Obselete";
            this.chkObselete.Size = new System.Drawing.Size(163, 20);
            this.chkObselete.TabIndex = 62;
            // 
            // btnRevise
            // 
            this.btnRevise.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRevise.ImageOptions.Image")));
            this.btnRevise.Location = new System.Drawing.Point(509, 448);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(154, 62);
            this.btnRevise.TabIndex = 61;
            this.btnRevise.Text = "Revise Document";
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // btnOpenO
            // 
            this.btnOpenO.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenO.ImageOptions.Image")));
            this.btnOpenO.Location = new System.Drawing.Point(669, 380);
            this.btnOpenO.Name = "btnOpenO";
            this.btnOpenO.Size = new System.Drawing.Size(147, 62);
            this.btnOpenO.TabIndex = 60;
            this.btnOpenO.Text = "Open Obsolete";
            this.btnOpenO.Click += new System.EventHandler(this.btnOpenO_Click);
            // 
            // btnObselete
            // 
            this.btnObselete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnObselete.ImageOptions.Image")));
            this.btnObselete.Location = new System.Drawing.Point(509, 380);
            this.btnObselete.Name = "btnObselete";
            this.btnObselete.Size = new System.Drawing.Size(154, 62);
            this.btnObselete.TabIndex = 59;
            this.btnObselete.Text = "Send to Obsolete";
            this.btnObselete.Click += new System.EventHandler(this.btnObselete_Click);
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(12, 559);
            this.textEdit4.MenuManager = this.barManager1;
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(804, 20);
            this.textEdit4.TabIndex = 58;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(698, 207);
            this.txtCode.MenuManager = this.barManager1;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(118, 20);
            this.txtCode.TabIndex = 57;
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(698, 148);
            this.txtNumber.MenuManager = this.barManager1;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(118, 20);
            this.txtNumber.TabIndex = 56;
            // 
            // lstItem
            // 
            this.lstItem.Location = new System.Drawing.Point(12, 87);
            this.lstItem.Name = "lstItem";
            this.lstItem.Size = new System.Drawing.Size(481, 423);
            this.lstItem.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.lstItem.TabIndex = 54;
            this.lstItem.SelectedIndexChanged += new System.EventHandler(this.lstItem_SelectedIndexChanged);
            this.lstItem.Click += new System.EventHandler(this.lstItem_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(509, 87);
            this.txtName.MenuManager = this.barManager1;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(307, 20);
            this.txtName.TabIndex = 55;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(775, 314);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(41, 37);
            this.simpleButton1.TabIndex = 81;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.ToolTip = "Open Safety Management Folder";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // RevisionEnvironment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 593);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.chkFocus);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.txtReplace);
            this.Controls.Add(this.txtFind);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.txtLabel);
            this.Controls.Add(this.txtStandard);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.chkObselete);
            this.Controls.Add(this.btnRevise);
            this.Controls.Add(this.btnOpenO);
            this.Controls.Add(this.btnObselete);
            this.Controls.Add(this.textEdit4);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.lstItem);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.IconOptions.Image = ((System.Drawing.Image)(resources.GetObject("RevisionEnvironment.IconOptions.Image")));
            this.Name = "RevisionEnvironment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RevisionEnvironment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RevisionEnvironment_FormClosed);
            this.Load += new System.EventHandler(this.RevisionEnvironment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chkFocus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStandard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkObselete.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lstItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.CheckEdit chkFocus;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txtReplace;
        private DevExpress.XtraEditors.TextEdit txtFind;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txtLabel;
        private DevExpress.XtraEditors.TextEdit txtStandard;
        private DevExpress.XtraEditors.SimpleButton btnOpen;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chkObselete;
        private DevExpress.XtraEditors.SimpleButton btnRevise;
        private DevExpress.XtraEditors.SimpleButton btnOpenO;
        private DevExpress.XtraEditors.SimpleButton btnObselete;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtNumber;
        private DevExpress.XtraEditors.ListBoxControl lstItem;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem txtReplaceTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}