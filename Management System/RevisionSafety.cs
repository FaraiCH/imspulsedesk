﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DocumentManager;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using DevExpress.XtraSplashScreen;

using Microsoft.VisualBasic.FileIO;

using Aspose.Words;
using Aspose.Cells;

using Aspose.Words.Drawing;
using Aspose.Cells.Drawing;

namespace Management_System
{
    public partial class RevisionSafety : DevExpress.XtraEditors.XtraForm
    {

        Timer t;

        FluentSplashScreenOptions op = new FluentSplashScreenOptions();
        public RevisionSafety()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));

            stream2.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream2);
            new Aspose.Words.License().SetLicense(stream);

            t = new Timer();
            t.Interval = 3000;
            t.Tick += T_Tick;
            t.Start();
          
            op.Title = "IMS Pulse Revision Master" + "\u2122";
            op.Subtitle = "Your Document Revision Solution";
            op.RightFooter = "Starting...";
            op.LeftFooter = "Copyright © 2000 - 2020 | Farai Chakarisa" + Environment.NewLine + "All Rights reserved.";
            op.LoadingIndicatorType = FluentLoadingIndicatorType.Dots;
            op.OpacityColor = Color.Gray;
            op.Opacity = 5;
            op.LogoImageOptions.ImageUri = @"E:\Applications IMS Pulse\Ultimate Edition\Management System\Management System\Resources\imspulse.png";

            DevExpress.XtraSplashScreen.SplashScreenManager.ShowFluentSplashScreen(
                op,
                parentForm: this,
                useFadeIn: true,
                useFadeOut: true
            );

            InitializeComponent();
        }

        private void T_Tick(object sender, EventArgs e)
        {
            t.Stop();
            //Close the splashscreen
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
           
        }

        public void Splash()
        {        
            // Show a splashscreen.
            
        

            //Do an operation
            //...

     
        }

        public void MessageBoxOk()
        {
            XtraMessageBoxArgs args = new XtraMessageBoxArgs();
            args.AutoCloseOptions.Delay = 5000;
            args.Caption = "Auto-close message";
            args.Text = "This message closes automatically after 5 seconds.";
            args.Buttons = new DialogResult[] { DialogResult.OK};
            XtraMessageBox.Show(args).ToString();

        }

        public void MessageYesNo(string header, string text)
        {
            XtraMessageBoxArgs args = new XtraMessageBoxArgs();
            args.AutoCloseOptions.Delay = 5000;
            args.Caption = header;
            args.Text = text;
            args.Buttons = new DialogResult[] { DialogResult.Yes, DialogResult.No };
            XtraMessageBox.Show(args).ToString();

            if (XtraMessageBox.Show(args) != DialogResult.Yes)
            {
               
            }
            else
            {

            }
        }

        public void LoadSelected(string primeFolder)
        {
            this.Text = "IMS Pulse Revision Master" + "\u2122";
            var myString = "EMa123_33";
            var onlyLetters = new String(myString.Where(Char.IsLetter).ToArray());
        
            string pathandfiles = "";
            try
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    
                    pathandfiles = fileArray[i];
                    if(pathandfiles.Contains("docx") || pathandfiles.Contains("doc") || pathandfiles.Contains("xls") || pathandfiles.Contains("xlsx") || pathandfiles.Contains("vsd") || pathandfiles.Contains("vsdx"))
                    {                     
                       lstItem.Items.Add(Path.GetFileName(fileArray[i]));                           
                    }                   
                }

               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ExtractNum(string primeFolder)
        {

            try
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                    {
                        string input = Path.GetFileNameWithoutExtension(fileArray[i]);
                        // Split on one or more non-digit characters.



                        char[] meh = { '-', ' ' };
                        string[] name = Path.GetFileNameWithoutExtension(fileArray[i]).Split(meh, 4);

                        var numbers2 = Regex.Matches(input, @"\d+").OfType<Match>().Select(m => int.Parse(m.Value)).ToArray();
                        var count = numbers2.Count();


                        if (count == 2)
                        {
                            txtName.Text = name[3];
                            txtStandard.Text = name[0];
                            txtLabel.Text = name[1];
                            textEdit4.Text = fileArray[i];
                            txtNumber.Text = numbers2[0].ToString().PadLeft(3,'0');
                            txtCode.Text = numbers2[1].ToString();
                        }
                        else if (count == 3)
                        {
                            txtName.Text = name[3];
                            txtStandard.Text = name[0];
                            txtLabel.Text = name[1];
                            textEdit4.Text = fileArray[i];
                            txtNumber.Text = numbers2[0].ToString().PadLeft(3,'0');
                            txtCode.Text = numbers2[2].ToString();
                        }
                        else
                        {

                            MessageBox.Show("The document format is not correct. It must be:\n\n STANDARD-LABEL-CODE-NAME (Revision No)", "Incorrect Document Name", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

}


        public void MakeField(string primeFolder)
        {
            string pathandfiles = "";
            string pathWord = "";

            try
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    pathandfiles = fileArray[i];

                    Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");

                    Worksheet sheet = work.Worksheets[0];
                    sheet.Cells[i, 0].PutValue(Path.GetFileNameWithoutExtension(pathandfiles));

                    work.Save(@"C:\Testing\Revision Tracking.xlsx");                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void Merge()
        {
            try
            {

                List<string> documentList = new List<string>();

                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + @"Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                    {
                        if (fileArray[i].Contains("doc"))
                        {
                            string path = Path.GetFileNameWithoutExtension(fileArray[i]);

                            // Open an existing document.
                            Document doc = new Document(fileArray[i]);

                            DocumentBuilder builder = new DocumentBuilder(doc);

                            foreach (string fieldName in doc.MailMerge.GetFieldNames())
                            {

                                builder.MoveToMergeField(fieldName, false, false);

                                builder.StartBookmark(fieldName);

                                builder.MoveToMergeField(fieldName, true, false);

                                builder.EndBookmark(fieldName);

                            }

                            // Trim trailing and leading whitespaces mail merge values
                            doc.MailMerge.TrimWhitespaces = false;

                            // Fill the fields in the document with user data.
                            doc.MailMerge.Execute(
                                new string[] { "Document Revisions" },
                                new object[] { path });


                            foreach (Bookmark bookmark in doc.Range.Bookmarks)
                            {
                                // Retrieve the field name and the merged field value using the bookmark.

                                string fieldName = bookmark.Name;

                                string fieldValue = bookmark.Text;

                                Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                                Worksheet sheet = work.Worksheets[0];
                                string documentname = sheet.Cells[i, 0].StringValue.Replace(".docx", "").Replace(".doc", "");
                                
                                doc.Range.Replace(fieldValue, documentname);
                                doc.Range.Replace("[Doc Number]", txtNumber.Text);
                                doc.Range.Replace("[Revision Number]", "REV" + txtCode.Text);
                                doc.Range.Replace("[Date Value]", DateTime.Now.ToString());
                                // Send the document in Word format to the client browser with an option to save to disk or open inside the current browser.
                                doc.Save(fileArray[i]);
                            }

                        }
              

                        if ((fileArray[i].Contains("xls") || fileArray[i].Contains("xlsx")) && (!fileArray[i].Contains("CAR") || !fileArray[i].Contains("RISK REGISTER")))
                        {
                            Workbook work2 = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                            Worksheet sheet2 = work2.Worksheets[0];

                            string documentName = sheet2.Cells[i, 0].StringValue;
                            work2.Save(@"C:\Testing\Revision Tracking.xlsx");

                            Workbook work = new Workbook(fileArray[i]);
                            Worksheet sheet = work.Worksheets[0];
                            sheet.Cells[0, 0].PutValue(documentName);
                            work.Save(fileArray[i]);
                        }
                    }
                }
            }
            catch (Exception)
            {

                MessageBox.Show("There is no revision field in this document. To be able to control document revisions, make a merge field in this document", "Revision Field Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Revision_Load(object sender, EventArgs e)
        {
            labelControl9.Text = "IMS Pulse Revision Master" + "\u2122";
            MakeField("Safety Management");

            LoadSelected("Safety Management");

            if (!Directory.Exists(ServerPath.PathLocation() + "Obsolete"))
            {
                FileSystem.CreateDirectory(ServerPath.PathLocation() + "Obsolete");
            }         
        }

        public void Tracking()
        {

            try
            {
                string[] fileArrayNew = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);
                string[] fileArrayOld = Directory.GetFiles(ServerPath.PathLocation() + "Obsolete" + @"\", "*", System.IO.SearchOption.AllDirectories);
                int newCode = int.Parse(txtCode.Text) - 1;

                for (int i = 0; i < fileArrayNew.Length; i++)
                {
                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArrayNew[i])))
                    {
                        if(fileArrayNew[i].Contains("docx") || fileArrayNew[i].Contains("doc"))
                        {
                            string compareNew = txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text;
                          
                            string compareOld = compareNew.Replace(txtCode.Text, newCode.ToString());


                            Document docNew = new Document(fileArrayNew[i]);
                            string newTRack = docNew.GetText().Replace(compareNew, "");



                            for (int j = 0; j < fileArrayOld.Length; j++)
                            {

                               
                                Document docOld = new Document(fileArrayOld[j]);
                                string OldTrack = docOld.GetText().Replace(compareOld, "");


                            
                               
                                if(Path.GetFileNameWithoutExtension(fileArrayOld[j]).Contains(compareOld))
                                {
                                    if(OldTrack.Contains(newTRack))
                                    {
                                        MessageBox.Show("The Revision Master has noticed that an older revision of this document in the Obsolete Folder contains the same content", "Obsolete Notice", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                }

                            }
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
     
          


        }

     
        public void AutoRevise()
        {
            if(chkFocus.Checked == true)
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);

                int newCode = int.Parse(txtCode.Text) + 1;

                for (int i = 0; i < fileArray.Length; i++)
                {

                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                    {
                        if (!File.Exists(ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i])))
                        {
                            File.Copy(fileArray[i], ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]));
                        }
                        if (fileArray[i].Contains("docx"))
                        {
                            string compareNew = txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text;
                            string compareOld = txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text;
                            Document docNew = new Document(fileArray[i]);
                            string newTRack = docNew.GetText().Replace(compareOld, "");
                            string[] fileArrayOld = Directory.GetFiles(ServerPath.PathLocation() + "Revision Master" + @"\", "*", System.IO.SearchOption.AllDirectories);

                            for (int j = 0; j < fileArrayOld.Length; j++)
                            {
                                if (Path.GetFileName(fileArrayOld[j]).Contains(compareNew))
                                {
                                    Document docOld = new Document(fileArrayOld[j]);
                                    string OldTrack = docOld.GetText().Replace(compareNew, "");


                                    if (!OldTrack.Contains(newTRack))
                                    {

                                        XtraMessageBoxArgs args = new XtraMessageBoxArgs();
                                      
                                        args.Caption = "Content Changed";
                                        args.Text = "Revisions have been made to this document while the Revision Number remains the same. \nIMS Pulse Revision Master\u2122 would like to update the revision number. \nAllow?";
                                        args.Buttons = new DialogResult[] { DialogResult.Yes, DialogResult.No };
         


                                        if (XtraMessageBox.Show(args) == DialogResult.Yes)
                                        {
                                            int newNumber = int.Parse(txtCode.Text) + 1;
                                            File.Delete(ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]));

                                            File.Move(fileArray[i], Path.GetDirectoryName(fileArray[i]) + @"\" + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text.Replace(txtCode.Text, newCode.ToString()) + Path.GetExtension(fileArray[i]));

                                            string[] filearryMoved = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);
                                            Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                                            Worksheet sheet = work.Worksheets[0];

                                            sheet.Cells[i, 0].PutValue(Path.GetDirectoryName(Path.GetFileName(fileArray[i])) + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text.Replace(txtCode.Text, newCode.ToString()));
                                            work.Save(@"C:\Testing\Revision Tracking.xlsx");


                                            Workbook work1 = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                                            Worksheet sheet2 = work.Worksheets[0];

                                            MessageBox.Show("Document has been revised. Make sure that the revision change is valid", "Revision Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            lstItem.Items.Remove(Path.GetFileName(fileArray[i]));
                                            lstItem.Items.Add(Path.GetFileName(sheet2.Cells[i, 0].StringValue) + ".docx");

                                            if (filearryMoved[j].Contains(Path.GetFileName(sheet2.Cells[i, 0].StringValue)))
                                            {
                                                File.Copy(filearryMoved[j], ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]), true);
                                                docOld.Range.Replace(".docx", "");
                                                docOld.Save(ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]));
                                            }
                                        }
                                     
                                     
                                    }
                                }
                            }
                        }
                        else if (fileArray[i].Contains("doc"))
                        {
                            string compareNew = txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text;
                            string compareOld = txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text;
                            Document docNew = new Document(fileArray[i]);
                            string newTRack = docNew.GetText().Replace(compareOld, "");
                            string[] fileArrayOld = Directory.GetFiles(ServerPath.PathLocation() + "Revision Master" + @"\", "*", System.IO.SearchOption.AllDirectories);

                            for (int j = 0; j < fileArrayOld.Length; j++)
                            {
                                if (Path.GetFileName(fileArrayOld[j]).Contains(compareNew))
                                {
                                    Document docOld = new Document(fileArrayOld[j]);
                                    string OldTrack = docOld.GetText().Replace(compareNew, "");


                                    if (!OldTrack.Contains(newTRack))
                                    {
                                        DialogResult dialog = MessageBox.Show("Revision have been made to this document while the Revision Number remains the same. IMS Pulse Revision Master would like to change the revision number. Allow?", "Content Changed", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                                        if (dialog == DialogResult.Yes)
                                        {
                                            int newNumber = int.Parse(txtCode.Text) + 1;
                                            File.Delete(ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]));

                                            File.Move(fileArray[i], Path.GetDirectoryName(fileArray[i]) + @"\" + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text.Replace(txtCode.Text, newCode.ToString()) + Path.GetExtension(fileArray[i]));

                                            string[] filearryMoved = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);
                                            Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                                            Worksheet sheet = work.Worksheets[0];

                                            sheet.Cells[i, 0].PutValue(Path.GetDirectoryName(Path.GetFileName(fileArray[i])) + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text.Replace(txtCode.Text, newCode.ToString()));
                                            work.Save(@"C:\Testing\Revision Tracking.xlsx");


                                            Workbook work1 = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                                            Worksheet sheet2 = work.Worksheets[0];

                                            MessageBox.Show("Document has been revised. Make sure that the revision change is valid", "Revision Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            lstItem.Items.Remove(Path.GetFileName(fileArray[i]));
                                            lstItem.Items.Add(Path.GetFileName(sheet2.Cells[i, 0].StringValue) + ".doc");

                                            if (filearryMoved[j].Contains(Path.GetFileName(sheet2.Cells[i, 0].StringValue)))
                                            {
                                                File.Copy(filearryMoved[j], ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]), true);
                                                docOld.Range.Replace(".doc", "");
                                                docOld.Save(ServerPath.PathLocation() + @"Revision Master\" + Path.GetFileName(fileArray[i]));
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
       
        }

        

        private void lstItem_Click(object sender, EventArgs e)
        {
       
            ExtractNum("Safety Management");
            Merge();          
            Tracking();
            AutoRevise();

        }

        private void textEdit4_DoubleClick(object sender, EventArgs e)
        {
            textEdit4.SelectAll();
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);

            for (int i = 0; i < fileArray.Length; i++)
            {
                if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                {
                    if(chkObselete.Checked == false)
                    {
                        if (!File.Exists(ServerPath.PathLocation() + @"Obsolete\" + Path.GetFileName(fileArray[i])))
                        {
                            MessageBox.Show("You have not made an Obsolete Copy of this document yet. Please send a copy of this document to the Obsolete folder before revising.", "Send to Obsolete Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {

                            File.Move(fileArray[i], Path.GetDirectoryName(fileArray[i]) + @"\" + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text + "REV (" + txtCode.Text + ")" + Path.GetExtension(fileArray[i]));

                            Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                            Worksheet sheet = work.Worksheets[0];

                            sheet.Cells[i, 0].PutValue(Path.GetDirectoryName(Path.GetFileName(fileArray[i])) + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text + "REV (" + txtCode.Text + ")" + Path.GetExtension(fileArray[i]));
                            work.Save(@"C:\Testing\Revision Tracking.xlsx");


                            Workbook work1 = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                            Worksheet sheet2 = work.Worksheets[0];

                            MessageBox.Show("Document has been revised. Make sure that the revision change is valid", "Revision Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lstItem.Items.Remove(Path.GetFileName(fileArray[i]));
                            lstItem.Items.Add(Path.GetFileName(sheet2.Cells[i, 0].StringValue));

                        }
                    }
                    else
                    {
                        File.Move(fileArray[i], Path.GetDirectoryName(fileArray[i]) + @"\" + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text + " REV (" + txtCode.Text + ")" + Path.GetExtension(fileArray[i]));

                        Workbook work = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                        Worksheet sheet = work.Worksheets[0];

                        sheet.Cells[i, 0].PutValue(Path.GetDirectoryName(Path.GetFileName(fileArray[i])) + txtStandard.Text + "-" + txtLabel.Text + "-" + txtNumber.Text + "-" + txtName.Text + " REV (" + txtCode.Text + ")" + Path.GetExtension(fileArray[i]));
                        work.Save(@"C:\Testing\Revision Tracking.xlsx");


                        Workbook work1 = new Workbook(@"C:\Testing\Revision Tracking.xlsx");
                        Worksheet sheet2 = work.Worksheets[0];

                        MessageBox.Show("Document has been revised. Make sure that the revision change is valid", "Revision Changed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lstItem.Items.Remove(Path.GetFileName(fileArray[i]));
                        lstItem.Items.Add(Path.GetFileName(sheet2.Cells[i, 0].StringValue));
                    }
                 
                }    
            }           
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);
            if (chkObselete.Checked == false)
            {
                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                    {
                                        
                        if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                        {
                            Process.Start(fileArray[i]);
                        }                       
                    }
                }

            }
        }

        static void insertWatermarkIntoHeader(Paragraph watermarkPara, Section sect, HeaderFooterType headerType)
        {
            HeaderFooter header = sect.HeadersFooters[headerType];
            if (header == null)
            {
                // There is no header of the specified type in the current section, create it.
                header = new HeaderFooter(sect.Document, headerType);
                sect.HeadersFooters.Add(header);
            }

            // Insert a clone of the watermark into the header.
            header.AppendChild(watermarkPara.Clone(true));


        }

        private void btnObselete_Click(object sender, EventArgs e)
        {
           
            if (chkObselete.Checked == false)
            {
                string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);

                for (int i = 0; i < fileArray.Length; i++)
                {
                    if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                    {
                        File.Copy(fileArray[i], ServerPath.PathLocation() + @"Obsolete\" + Path.GetFileName(fileArray[i]), true);

                        if(fileArray[i].Contains(".doc") || fileArray[i].Contains(".docx"))
                        {
                            
                            Document doc = new Document(ServerPath.PathLocation() + @"Obsolete\" + Path.GetFileName(fileArray[i]));

                            string watermarkText = "OBSOLETE";

                            // Create a watermark shape. This will be a WordArt shape.
                            // You are free to try other shape types as watermarks.
                            Aspose.Words.Drawing.Shape watermark = new Aspose.Words.Drawing.Shape(doc, ShapeType.TextPlainText);

                            // Set up the text of the watermark.
                            watermark.TextPath.Text = watermarkText;
                            watermark.TextPath.FontFamily = "Arial Black";
                            watermark.TextPath.Bold = true;
                            watermark.Width = 600;
                            watermark.Height = 100;

                            // Text will be directed from the bottom-left to the top-right corner.
                            watermark.Rotation = -40;

                            // Remove the following two lines if you need a solid black text.
                            watermark.Fill.Color = System.Drawing.Color.Red;
                            // Try LightGray to get more Word-style watermark
                            watermark.StrokeColor = System.Drawing.Color.Red;
                            //// Try LightGray to get more Word-style watermark
                            // Place the watermark in the page center.
                            watermark.RelativeHorizontalPosition = RelativeHorizontalPosition.Page;
                            watermark.RelativeVerticalPosition = RelativeVerticalPosition.Page;
                            watermark.WrapType = WrapType.None;
                            watermark.VerticalAlignment = VerticalAlignment.Center;
                            watermark.HorizontalAlignment = Aspose.Words.Drawing.HorizontalAlignment.Center;

                            // Create a new paragraph and append the watermark to this paragraph.
                            Paragraph watermarkPara = new Paragraph(doc);
                            watermarkPara.AppendChild(watermark);

                            // Insert the watermark into all headers of each document section.
                            foreach (Section sect in doc.Sections)
                            {
                                // There could be up to three different headers in each section, since we want
                                // the watermark to appear on all pages, insert into all headers.
                                insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HeaderPrimary);
                                insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HeaderFirst);
                                insertWatermarkIntoHeader(watermarkPara, sect, HeaderFooterType.HeaderEven);
                            }
                            doc.Protect(Aspose.Words.ProtectionType.ReadOnly, "@Paradice1");
                            doc.Save(ServerPath.PathLocation() + @"Obsolete\" + Path.GetFileName(fileArray[i]));
                        }
                        else if(fileArray[i].Contains("xls") || fileArray.Contains("xlsx"))
                        {
                            Workbook work = new Workbook(fileArray[i]);
                            // Get the first default sheet
                            Worksheet sheet = work.Worksheets[0];

                            // Add Watermark
                            Aspose.Cells.Drawing.Shape wordart = sheet.Shapes.AddTextEffect(MsoPresetTextEffect.TextEffect1,
                            "OBSOLETE", "Arial Black", 50, true, false
                            , 18, 8, 1, 1, 400, 1200);

                            // Get the fill format of the word art
                            FillFormat wordArtFormat = wordart.Fill;

                            wordArtFormat.SetOneColorGradient(Color.Red, 0.0, GradientStyleType.Horizontal, 2);

                            // Set the transparency
                            wordArtFormat.Transparency = 0.4;
                            
                            // Make the line invisible
                            LineFormat lineFormat = wordart.Line;

                            // Make the line invisible
                            wordart.HasLine = false;

                            // Save the file
                            work.Settings.WriteProtection.Author = "IMS Pulse Revision Master";
                            work.Settings.WriteProtection.Password = "@Paradice1";
                         

                            work.Save(ServerPath.PathLocation() + @"Obsolete\" + Path.GetFileName(fileArray[i]));
                        }
                
                    }
                }
            }   
        }

        private void btnOpenO_Click(object sender, EventArgs e)
        {
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);
                       
                for (int i = 0; i < fileArray.Length; i++)
                {
                  
                        if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                        {
                            Process.Start(ServerPath.PathLocation() + @"Obsolete\");
                        }
                    
                }
              
                   
        }

        private void barStaticItem1_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void barButtonItem4_ItemClick(object sender, ItemClickEventArgs e)
        {
            string[] fileArray = Directory.GetFiles(ServerPath.PathLocation() + "Safety Management" + @"\", "*", System.IO.SearchOption.AllDirectories);

            for (int i = 0; i < fileArray.Length; i++)
            {
                if (lstItem.SelectedItem.ToString().Contains(Path.GetFileName(fileArray[i])))
                {
                    if(fileArray[i].Contains("docx") || fileArray[i].Contains("doc"))
                    {
                        Document doc = new Document(fileArray[i]);
                        if(txtFind.Text == "" || txtReplace.Text == "")
                        {
                            MessageBox.Show("Please Enter Values", "Empty Values", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            int count = doc.Range.Replace(txtFind.Text, txtFind.Text);
                            MessageBox.Show("Target " + txtFind.Text + " found: " + count, "Search " + txtFind.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (count > 0)
                            {
                                doc.Range.Replace(txtFind.Text, txtReplace.Text);
                                doc.Save(fileArray[i]);
                                MessageBox.Show(txtReplace.Text + " has replaced " + txtFind.Text, "Replace Done0", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Nothing was found for " + txtFind.Text + ".", "Nothing found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                             
                    }
                    else
                    {
                        MessageBox.Show("This is not a Word Document");
                    }
                }
            }
        }

        private void txtReplace_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void txtName_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void RevisionSafety_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            SafetySection safetyCentre = new SafetySection();
            safetyCentre.Show();
        }

        private void lstItem_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void chkFocus_CheckedChanged(object sender, EventArgs e)
        {
            if(chkFocus.Checked == true)
            {
                btnObselete.Enabled = false;
  
                btnOpenO.Enabled = false;
                btnRevise.Enabled = false;
                chkObselete.Enabled = false;
            }
            else
            {
                btnObselete.Enabled = true;

                btnOpenO.Enabled = true;
                btnRevise.Enabled = true;
                chkObselete.Enabled = true;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Process.Start(ServerPath.PathLocation() + @"Safety Management\IMS Global");
        }

        private void toastNotificationsManager1_Activated(object sender, DevExpress.XtraBars.ToastNotifications.ToastNotificationEventArgs e)
        {

        }

        private void lstItem_DoubleClick(object sender, EventArgs e)
        {
            //flyoutPanel1.ShowBeakForm();
        }

        private void lstItem_MouseHover(object sender, EventArgs e)
        {
        
        }

        private void lstItem_Click_1(object sender, EventArgs e)
        {
            ExtractNum("Safety Management");
            Merge();
            Tracking();
            AutoRevise();
        }


        //private void BarManager1_ItemClick(object sender, ItemClickEventArgs e)
        //{
        //    XtraMessageBox.Show(e.Item.Caption + " item clicked");
        //}
    }
}