﻿namespace Management_System
{
    partial class RiskRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RiskRegister));
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cboCost = new System.Windows.Forms.ComboBox();
            this.cboReputation = new System.Windows.Forms.ComboBox();
            this.cboRegulation = new System.Windows.Forms.ComboBox();
            this.cboContractTerms = new System.Windows.Forms.ComboBox();
            this.cboHumanHealth = new System.Windows.Forms.ComboBox();
            this.cboLossContracts = new System.Windows.Forms.ComboBox();
            this.cboPrevious = new System.Windows.Forms.ComboBox();
            this.cboLike = new System.Windows.Forms.ComboBox();
            this.cboPro = new System.Windows.Forms.ComboBox();
            this.txtRisk = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.SuspendLayout();
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "View Entries";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 90D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(310, 654);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(164, 48);
            this.bunifuFlatButton2.TabIndex = 45;
            this.bunifuFlatButton2.Text = "View Entries";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.bunifuFlatButton2_Click);
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Add Risk";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(37, 654);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.DarkBlue;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.LightSkyBlue;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(171, 48);
            this.bunifuFlatButton1.TabIndex = 44;
            this.bunifuFlatButton1.Text = "Add Risk";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.cboCost);
            this.groupBox1.Controls.Add(this.cboReputation);
            this.groupBox1.Controls.Add(this.cboRegulation);
            this.groupBox1.Controls.Add(this.cboContractTerms);
            this.groupBox1.Controls.Add(this.cboHumanHealth);
            this.groupBox1.Controls.Add(this.cboLossContracts);
            this.groupBox1.Controls.Add(this.cboPrevious);
            this.groupBox1.Controls.Add(this.cboLike);
            this.groupBox1.Controls.Add(this.cboPro);
            this.groupBox1.Controls.Add(this.txtRisk);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Location = new System.Drawing.Point(37, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(437, 619);
            this.groupBox1.TabIndex = 43;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Make Risk Entry";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.label14.Location = new System.Drawing.Point(166, 585);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 18);
            this.label14.TabIndex = 45;
            this.label14.Text = "label14";
            this.c1ThemeController1.SetTheme(this.label14, "(default)");
            // 
            // cboCost
            // 
            this.cboCost.FormattingEnabled = true;
            this.cboCost.Location = new System.Drawing.Point(232, 538);
            this.cboCost.Name = "cboCost";
            this.cboCost.Size = new System.Drawing.Size(199, 21);
            this.cboCost.TabIndex = 43;
            this.cboCost.SelectedIndexChanged += new System.EventHandler(this.cboCost_SelectedIndexChanged);
            // 
            // cboReputation
            // 
            this.cboReputation.FormattingEnabled = true;
            this.cboReputation.Location = new System.Drawing.Point(232, 481);
            this.cboReputation.Name = "cboReputation";
            this.cboReputation.Size = new System.Drawing.Size(199, 21);
            this.cboReputation.TabIndex = 42;
            this.cboReputation.SelectedIndexChanged += new System.EventHandler(this.cboReputation_SelectedIndexChanged);
            // 
            // cboRegulation
            // 
            this.cboRegulation.FormattingEnabled = true;
            this.cboRegulation.Location = new System.Drawing.Point(232, 424);
            this.cboRegulation.Name = "cboRegulation";
            this.cboRegulation.Size = new System.Drawing.Size(199, 21);
            this.cboRegulation.TabIndex = 41;
            this.cboRegulation.SelectedIndexChanged += new System.EventHandler(this.cboRegulation_SelectedIndexChanged);
            // 
            // cboContractTerms
            // 
            this.cboContractTerms.FormattingEnabled = true;
            this.cboContractTerms.Location = new System.Drawing.Point(232, 363);
            this.cboContractTerms.Name = "cboContractTerms";
            this.cboContractTerms.Size = new System.Drawing.Size(199, 21);
            this.cboContractTerms.TabIndex = 40;
            this.cboContractTerms.SelectedIndexChanged += new System.EventHandler(this.cboContractTerms_SelectedIndexChanged);
            // 
            // cboHumanHealth
            // 
            this.cboHumanHealth.FormattingEnabled = true;
            this.cboHumanHealth.Location = new System.Drawing.Point(232, 295);
            this.cboHumanHealth.Name = "cboHumanHealth";
            this.cboHumanHealth.Size = new System.Drawing.Size(199, 21);
            this.cboHumanHealth.TabIndex = 39;
            this.cboHumanHealth.SelectedIndexChanged += new System.EventHandler(this.cboHumanHealth_SelectedIndexChanged);
            // 
            // cboLossContracts
            // 
            this.cboLossContracts.FormattingEnabled = true;
            this.cboLossContracts.Location = new System.Drawing.Point(232, 233);
            this.cboLossContracts.Name = "cboLossContracts";
            this.cboLossContracts.Size = new System.Drawing.Size(199, 21);
            this.cboLossContracts.TabIndex = 38;
            this.cboLossContracts.SelectedIndexChanged += new System.EventHandler(this.cboLossContracts_SelectedIndexChanged);
            // 
            // cboPrevious
            // 
            this.cboPrevious.FormattingEnabled = true;
            this.cboPrevious.Location = new System.Drawing.Point(232, 175);
            this.cboPrevious.Name = "cboPrevious";
            this.cboPrevious.Size = new System.Drawing.Size(199, 21);
            this.cboPrevious.TabIndex = 37;
            this.cboPrevious.SelectedIndexChanged += new System.EventHandler(this.cboPrevious_SelectedIndexChanged);
            // 
            // cboLike
            // 
            this.cboLike.FormattingEnabled = true;
            this.cboLike.Location = new System.Drawing.Point(232, 118);
            this.cboLike.Name = "cboLike";
            this.cboLike.Size = new System.Drawing.Size(199, 21);
            this.cboLike.TabIndex = 36;
            this.cboLike.SelectedIndexChanged += new System.EventHandler(this.cboLike_SelectedIndexChanged);
            // 
            // cboPro
            // 
            this.cboPro.FormattingEnabled = true;
            this.cboPro.Location = new System.Drawing.Point(232, 19);
            this.cboPro.Name = "cboPro";
            this.cboPro.Size = new System.Drawing.Size(199, 21);
            this.cboPro.TabIndex = 35;
            // 
            // txtRisk
            // 
            this.txtRisk.Location = new System.Drawing.Point(232, 67);
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.Size = new System.Drawing.Size(199, 20);
            this.txtRisk.TabIndex = 33;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(45, 546);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 30;
            this.label6.Text = "Est. Cost of Correction";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(46, 489);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Impact on Company Reputation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(44, 432);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Potential Violation of Regulation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Process";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Risk";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 371);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(156, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Inability to Meet Contract Terms";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(45, 126);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Likelihood";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(47, 303);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(155, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Potential Risk to Human Health";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Previous Occurence";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(45, 241);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Potential Loss of Contracts";
            // 
            // RiskRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 714);
            this.Controls.Add(this.bunifuFlatButton2);
            this.Controls.Add(this.bunifuFlatButton1);
            this.Controls.Add(this.groupBox1);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RiskRegister";
            this.Text = "Risk Register";
            this.c1ThemeController1.SetTheme(this, "MacBlue");
            this.Load += new System.EventHandler(this.RiskRegister_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRisk;
        private System.Windows.Forms.ComboBox cboCost;
        private System.Windows.Forms.ComboBox cboReputation;
        private System.Windows.Forms.ComboBox cboRegulation;
        private System.Windows.Forms.ComboBox cboContractTerms;
        private System.Windows.Forms.ComboBox cboHumanHealth;
        private System.Windows.Forms.ComboBox cboLossContracts;
        private System.Windows.Forms.ComboBox cboPrevious;
        private System.Windows.Forms.ComboBox cboLike;
        private System.Windows.Forms.ComboBox cboPro;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private System.Windows.Forms.Label label14;
    }
}