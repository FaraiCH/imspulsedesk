﻿using Aspose.Cells;
using DocumentManager;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XlApp = Microsoft.Office.Interop.Excel;
namespace Management_System
{
    public partial class RiskRegister : Form
    {
        string path = ServerPath.PathLocation() + @"Shared\RISK REGISTER.xlsx";

        public RiskRegister()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);

            InitializeComponent();
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            Advanced_Risk_Register register = new Advanced_Risk_Register();
            register.Show();
        }


        private void RiskRegister_Load(object sender, EventArgs e)
        {
            try
            {
                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";

                //try
                //{
                using (var client = new SftpClient(host, port, username, password))
                {

                    client.Connect();

                    if (client.IsConnected)
                    {

                        if (File.Exists(@"C:\Reader\Loader.xlsx"))
                        {


                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                            Workbook works = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                            Worksheet sheets = works.Worksheets[1];

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue);

                            }

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared");
                            }

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                            {
                                using (var fileStream = new FileStream(path, FileMode.Open))
                                {

                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                }
                            }
                            else
                            {
                                foreach (var item in client.ListDirectory(@"/var/www/html/imspulse/bunch-box/" + sheets.Cells["C1"].StringValue + @"/Shared/"))
                                {
                                    if (!item.IsDirectory)
                                    {
                                        using (Stream fileStreams = File.Create(ServerPath.PathLocation() + @"/Shared/" + item.Name))
                                        {
                                            client.DownloadFile(item.FullName, fileStreams);
                                        }

                                    }

                                }
                            }

                        }
                    }
                    else
                    {
                        MessageBox.Show("Unable to connect to the cloud. Please check your internet connection.");
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


            //EntryNo();
            label14.Visible = false;
         
            Workbook work = new Workbook(path);
            Worksheet sheet = work.Worksheets[4];

            int row = sheet.Cells.MaxDataRow;

            for (int i = 2; i < row; i++)
            {
                if (sheet.Cells["I" + i].Type != CellValueType.IsNull && sheet.Cells["I" + i].Value.ToString() != "All Processes")
                {
                    cboPro.Items.Add(sheet.Cells["I" + i].Value);
                }

                if (sheet.Cells["K" + i].Type != CellValueType.IsNull)
                {
                    cboLike.Items.Add(sheet.Cells["K" + i].Value);
                }

                if (sheet.Cells["L" + i].Type != CellValueType.IsNull)
                {
                    cboPrevious.Items.Add(sheet.Cells["L" + i].Value);
                }

                if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                {
                    cboLossContracts.Items.Add(sheet.Cells["M" + i].Value);
                }

                if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                {
                    cboHumanHealth.Items.Add(sheet.Cells["M" + i].Value);
                }
                if (sheet.Cells["M" + i].Type != CellValueType.IsNull)
                {
                    cboContractTerms.Items.Add(sheet.Cells["M" + i].Value);
                }
                if (sheet.Cells["N" + i].Type != CellValueType.IsNull)
                {
                    cboRegulation.Items.Add(sheet.Cells["N" + i].Value);
                }
                if (sheet.Cells["P" + i].Type != CellValueType.IsNull)
                {
                    cboReputation.Items.Add(sheet.Cells["P" + i].Value);
                }
                if (sheet.Cells["O" + i].Type != CellValueType.IsNull)
                {
                   cboCost.Items.Add(sheet.Cells["O" + i].Value);
                }
               

            }

        }

        public void CalcFullRating()
        {        
            float lossContracts, humanHealth, contractTerms, regulation, reputation, cost = 0;
            float rating = 1;

            switch (cboLossContracts.SelectedIndex)
            {
                case 0:
                    lossContracts = 1;
                    break;
                case 1:
                    lossContracts = 2;
                    break;
                case 2:
                    lossContracts = 3;
                    break;
                case 3:
                    lossContracts = 4;
                    break;
                case 4:
                    lossContracts = 5;
                    break;
                default:
                    lossContracts = 0;
                    break;

            }

            switch (cboHumanHealth.SelectedIndex)
            {
                case 0:
                    humanHealth = 1;
                    break;
                case 1:
                    humanHealth = 2;
                    break;
                case 2:
                    humanHealth = 3;
                    break;
                case 3:
                    humanHealth = 4;
                    break;
                case 4:
                    humanHealth = 5;
                    break;
                default:
                    humanHealth = 0;
                    break;

            }

            switch (cboContractTerms.SelectedIndex)
            {
                case 0:
                    contractTerms = 1;
                    break;
                case 1:
                    contractTerms = 2;
                    break;
                case 2:
                    contractTerms = 3;
                    break;
                case 3:
                    contractTerms = 4;
                    break;
                case 4:
                    contractTerms = 5;
                    break;
                default:
                    contractTerms = 0;
                    break;

            }

            switch (cboRegulation.SelectedIndex)
            {
                case 0:
                    regulation = 1;
                    break;
                case 1:
                    regulation = 2;
                    break;
                case 2:
                    regulation = 3;
                    break;
                case 3:
                    regulation = 4;
                    break;
                case 4:
                    regulation = 5;
                    break;
                default:
                    regulation = 0;
                    break;

            }

            switch (cboReputation.SelectedIndex)
            {
                case 0:
                    reputation = 1;
                    break;
                case 1:
                    reputation = 2;
                    break;
                case 2:
                    reputation = 3;
                    break;
                case 3:
                    reputation = 4;
                    break;
                case 4:
                    reputation = 5;
                    break;
                default:
                    reputation = 0;
                    break;

            }

            switch (cboCost.SelectedIndex)
            {
                case 0:
                    cost = 1;
                    break;
                case 1:
                    cost = 2;
                    break;
                case 2:
                    cost = 3;
                    break;
                case 3:
                    cost = 4;
                    break;
                case 4:
                    cost = 5;
                    break;
               

            }

            rating = (lossContracts + humanHealth + contractTerms + regulation + reputation + cost) / 6;

            if(rating > 3)
            {
               
                label14.Visible = true;
                label14.Text = "Immediate Action Needed";
                label14.BackColor = Color.DarkRed;
                label14.ForeColor = Color.White;
            }
            else if(rating < 2)
            {
                
                label14.Visible = true;
                label14.Text = "No Action Needed";
                label14.BackColor = Color.Green;
                label14.ForeColor = Color.Black;
            }
            else if (rating > 2 && rating < 3)
            {
               
                label14.Visible = true;
                label14.Text = "Action Might Be Needed";
                label14.BackColor = Color.Yellow;
                label14.ForeColor = Color.Black;
            }
         
           
        }

        public void InputCorrectiveAction()
        {
           

            Workbook work = new Workbook(path);
            Worksheet sheet = work.Worksheets[2];

            int rowCount = 0;
            int row = sheet.Cells.Rows.Count;
            
            
            for (int i = 1; i < row; i++)
            {
                if (sheet.Cells[i, 2].Type != CellValueType.IsNull)
                {
                    rowCount += 1;
                }

            }
            rowCount += 3;

            //MessageBox.Show(rowCount.ToString());
            ////Fill the Data Here:

            if (sheet.Cells["A" + rowCount].Type != CellValueType.IsNull)
            {
                sheet.Cells["A" + rowCount].PutValue(rowCount - 3);
            }

            sheet.Cells["B" + rowCount].PutValue(cboPro.SelectedItem.ToString());
            sheet.Cells["C" + rowCount].PutValue(txtRisk.Text);
            sheet.Cells["D" + rowCount].PutValue(cboLike.SelectedItem.ToString());
            sheet.Cells["E" + rowCount].PutValue(cboPrevious.SelectedItem.ToString());
            sheet.Cells["G" + rowCount].PutValue(cboLossContracts.SelectedItem.ToString());
            sheet.Cells["H" + rowCount].PutValue(cboHumanHealth.SelectedItem.ToString());
            sheet.Cells["I" + rowCount].PutValue(cboContractTerms.SelectedItem.ToString());
            sheet.Cells["J" + rowCount].PutValue(cboRegulation.SelectedItem.ToString());
            sheet.Cells["K" + rowCount].PutValue(cboReputation.SelectedItem.ToString());
            sheet.Cells["L" + rowCount].PutValue(cboCost.SelectedItem.ToString());
            sheet.Cells["Q" + rowCount].PutValue(DateTime.Now);

            work.CalculateFormula();

            work.Save(path);

            //CheckRating();
            MessageBox.Show("Changes Complete. Please view the Risk Register for detailed analysis", "Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      

        private void txtRating_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void cboCost_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboPrevious_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void cboLossContracts_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboHumanHealth_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboContractTerms_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboRegulation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboReputation_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalcFullRating();
        }

        private void cboLike_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {


                var host = "imspulse.com";
                var port = 22;
                var username = "farai";
                var password = "@Paradice1";



                using (var client = new SftpClient(host, port, username, password))
                {


                    if (File.Exists(@"C:\Reader\Loader.xlsx"))
                    {
                        Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                        Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

                        Worksheet sheet = work.Worksheets[1];

                        client.Connect();


                        if (client.IsConnected)
                        {
                            InputCorrectiveAction();
                            //MessageBox.Show("I'm connected to the client");  

                            string result = new DirectoryInfo(path).Parent.Name;

                            if (!client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/"))
                            {
                                client.CreateDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                            }


                            using (var fileStream = new FileStream(path, FileMode.Open))
                            {

                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                if (client.Exists(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/" + Path.GetFileName(path)))
                                {
                                    client.ChangeDirectory(@"/var/www/html/imspulse/bunch-box/" + sheet.Cells["A1"].StringValue + @"/Shared/");
                                    client.UploadFile(fileStream, Path.GetFileName(path));
                                    MessageBox.Show("NCR Entry Created");
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("We couldn't connect to the server. It could be an internet connection issue. Therefore, your NCR will not be updated on the server.");

                        }

                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
      
            //ResetEverything();
        }
    }
}
