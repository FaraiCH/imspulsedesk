﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentManager;
using System.IO;
using System.Diagnostics;
using Aspose.Cells;
using Aspose.Words;
using Aspose.Slides;
using Microsoft.VisualBasic.FileIO;
using System.Net;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList.Menu;
using TheRelease;
using MySql.Data.MySqlClient;

namespace Management_System
{
    public partial class SMSPolicy : UserControl
    {
        NetworkCredential networkCredential = new NetworkCredential("Cinfratec", "admincinfratec");
        public SMSPolicy()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Words.License().SetLicense(stream);

            Stream stream2 = new MemoryStream(Convert.FromBase64String(LData));

            stream2.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream2);


            Stream stream3 = new MemoryStream(Convert.FromBase64String(LData));

            stream3.Seek(0, SeekOrigin.Begin);
            new Aspose.Slides.License().SetLicense(stream3);

            InitializeComponent();
        }

        public void ResetAll()
        {
            //lstHR.Hide();
            //lstOperation.Hide();
            //lstSMS.Hide();
            //bunifuTileButton2.color = Color.DodgerBlue;
            //bunifuTileButton1.color = Color.Gray;
            //bunifuTileButton3.color = Color.Gray;
            //bunifuFlatButton1.Enabled = false;
            //bunifuFlatButton2.Enabled = false;
            //bunifuFlatButton3.Enabled = false;
        }

        private void PoliciesPro_Load(object sender, EventArgs e)
        {

            loadlist();
        }

        public void loadlist()
        {
            treeList1.Parent = this;
            treeList1.Dock = DockStyle.None;
            treeList2.Dock = DockStyle.None;
            //Specify the fields that arrange underlying data as a hierarchy.
            treeList1.KeyFieldName = "ID";
            treeList2.KeyFieldName = "ID";
            treeList3.KeyFieldName = "ID";
            treeList1.ParentFieldName = "RegionID";
            treeList2.ParentFieldName = "RegionID";
            treeList3.ParentFieldName = "RegionID";
            //Allow the treelist to create columns bound to the fields the KeyFieldName and ParentFieldName properties specify.
            treeList1.OptionsBehavior.PopulateServiceColumns = true;
            treeList2.OptionsBehavior.PopulateServiceColumns = true;
            treeList3.OptionsBehavior.PopulateServiceColumns = true;
            //Specify the data source.
            treeList1.DataSource = SalesDataGenerator.CreateDataPol(@"Safety Management\SMS");
            treeList2.DataSource = SalesDataGenerator.CreateDataHRPol(@"Safety Management\Human Resources");
            treeList3.DataSource = SalesDataGenerator.CreateDataOperationPol(@"Safety Management\Operations");
            //The treelist automatically creates columns for the public fields found in the data source. 
            //You do not need to call the TreeList.PopulateColumns method unless the treeList1.OptionsBehavior.AutoPopulateColumns option is disabled.

            //Change the row height.
            treeList1.RowHeight = 23;
            treeList2.RowHeight = 23;
            treeList3.RowHeight = 23;

            //Access the automatically created columns.
            TreeListColumn colRegion = treeList1.Columns["Document"];
            TreeListColumn colRegio2 = treeList2.Columns["Document"];
            TreeListColumn colRegio3 = treeList3.Columns["Document"];


            //Hide the key columns. An end-user can access them from the Customization Form.
            treeList1.Columns[treeList1.KeyFieldName].Visible = false;
            treeList2.Columns[treeList1.KeyFieldName].Visible = false;
            treeList3.Columns[treeList1.KeyFieldName].Visible = false;
            treeList1.Columns[treeList1.ParentFieldName].Visible = false;
            treeList2.Columns[treeList1.ParentFieldName].Visible = false;
            treeList3.Columns[treeList1.ParentFieldName].Visible = false;


            //Make the Region column read-only.
            colRegion.OptionsColumn.ReadOnly = true;
            colRegio2.OptionsColumn.ReadOnly = true;
            colRegio3.OptionsColumn.ReadOnly = true;

            //Sort data against the Region column
            colRegion.SortIndex = 0;
            colRegio2.SortIndex = 0;
            colRegio3.SortIndex = 0;

            //Do not stretch columns to the treelist width.



            //Calculate the optimal column widths after the treelist is shown.
            this.BeginInvoke(new MethodInvoker(delegate {
                treeList1.BestFitColumns();
            }));

            treeList1.StateImageList = imageCollection1;

            treeList2.StateImageList = imageCollection1;
            treeList3.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList1.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 7;

                }
                else
                {
                    node.StateImageIndex = 7;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

            foreach (TreeListNode node in treeList2.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 8;

                }
                else
                {
                    node.StateImageIndex = 8;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
            foreach (TreeListNode node in treeList3.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = 9;

                }
                else
                {
                    node.StateImageIndex = 9;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }
        }

        public void treelistDecrypt(TreeList treeList, int color)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;
            string primeFolder = @"Safety Management";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
            try
            {
                for (int i = 0; i < directory.Length; i++)
                {
                    if (selectedNodes[0].HasChildren == false)
                    {

                        if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                        {
                            // initialize a new XtraInputBoxArgs instance
                            XtraInputBoxArgs args = new XtraInputBoxArgs();
                            // set required Input Box options
                            args.Caption = "Password Protection";
                            args.Prompt = "Enter Password To Decrypt";
                            args.DefaultButtonIndex = 0;
                            // initialize a DateEdit editor with custom settings
                            TextEdit editor = new TextEdit();

                            args.Editor = editor;
                            // a default DateEdit value
                            args.DefaultResponse = "My Password";
                            // display an Input Box with the custom editor
                            var result = XtraInputBox.Show(args).ToString();

                            if (directory[i].Contains(".doc"))
                            {


                                Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };

                                Document docu = new Document(directory[i], getum12);
                                docu.Unprotect();
                                docu.Save(directory[i]);
                            }
                            if (directory[i].Contains(".xls"))
                            {
                                Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                                Workbook work = new Workbook(directory[i], getum);

                                work.Settings.Password = null;

                                work.Save(directory[i]);
                            }
                            if (directory[i].Contains(".ppt"))
                            {
                                Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                                using (Presentation presentation = new Presentation(directory[i], getpere))
                                {
                                    presentation.ProtectionManager.RemoveEncryption();

                                    presentation.Save(directory[i], Aspose.Slides.Export.SaveFormat.Pptx);
                                }
                            }


                        }
                    }

                }
                MessageBox.Show("Item Decrypted");

                treeList.RefreshDataSource();
                treeList.StateImageList = imageCollection1;

                foreach (TreeListNode node in treeList.Nodes)
                {


                    // first node
                    if (node.HasChildren)
                    {
                        node.StateImageIndex = color;

                    }
                    else
                    {
                        node.StateImageIndex = color;
                    }
                    foreach (TreeListNode mumba in node.Nodes)
                    {
                        if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                        {
                            mumba.StateImageIndex = 1;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                        {
                            mumba.StateImageIndex = 2;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                        {
                            mumba.StateImageIndex = 3;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                        {
                            mumba.StateImageIndex = 4;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                        {
                            mumba.StateImageIndex = 5;
                        }
                        else
                        {
                            mumba.StateImageIndex = 6;
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void treelistEncrypt(TreeList treeList, int color)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;
            string primeFolder = @"Safety Management";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

            for (int i = 0; i < directory.Length; i++)
            {
                if (selectedNodes[0].HasChildren == false)
                {

                    if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                    {
                        // initialize a new XtraInputBoxArgs instance
                        XtraInputBoxArgs args = new XtraInputBoxArgs();
                        // set required Input Box options
                        args.Caption = "Password Protection";
                        args.Prompt = "Enter Password To Encrypt";
                        args.DefaultButtonIndex = 0;
                        // initialize a DateEdit editor with custom settings
                        TextEdit editor = new TextEdit();

                        args.Editor = editor;
                        // a default DateEdit value
                        args.DefaultResponse = "My Password";
                        // display an Input Box with the custom editor
                        var result = XtraInputBox.Show(args).ToString();

                        if (directory[i].Contains(".doc"))
                        {

                            Aspose.Words.Saving.OoxmlSaveOptions opt = new Aspose.Words.Saving.OoxmlSaveOptions(Aspose.Words.SaveFormat.Docx);

                            opt.Compliance = Aspose.Words.Saving.OoxmlCompliance.Iso29500_2008_Transitional;

                            opt.Password = result;
                            Aspose.Words.LoadOptions getum12 = new Aspose.Words.LoadOptions { Password = result };
                            Document docu = new Document(directory[i], getum12);

                            docu.Save(directory[i], opt);
                        }
                        if (directory[i].Contains(".xls"))
                        {
                            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = result };
                            Workbook work = new Workbook(directory[i], getum);

                            work.Settings.Password = result;

                            work.Save(directory[i]);
                        }
                        if (directory[i].Contains(".ppt"))
                        {
                            Aspose.Slides.LoadOptions getpere = new Aspose.Slides.LoadOptions { Password = result };

                            using (Presentation presentation = new Presentation(directory[i], getpere))
                            {
                                presentation.ProtectionManager.Encrypt(result);

                                presentation.Save(directory[i], Aspose.Slides.Export.SaveFormat.Pptx);
                            }
                        }


                    }
                }

            }
            MessageBox.Show("Item Encrypted");

            treeList.RefreshDataSource();
            treeList.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = color;

                }
                else
                {
                    node.StateImageIndex = color;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }

        }


        public void treelistDoubleClick(string subfolder, TreeList treeList)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;
            string primeFolder = @"Safety Management" + subfolder;
            int duplicates = 0;
            string first = string.Empty;
            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[1];
            //Connect to Remote Mysql Database

            int idGroup = 0;

            int idUser = 0;
            var dbCon = DBConnection.Instance();
            dbCon.Server = "92.205.25.31";
            dbCon.DatabaseName = "imspulse";
            dbCon.UserName = "manny";
            dbCon.Password = "@Paradice1";

            if (dbCon.IsConnect())
            {
                //Get all users and compare that user to the login
                string query = "SELECT * FROM imspulse.backend_users;";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string idInfo = reader.GetString(0);
                    string loginInfo = reader.GetString(3);


                    if (sheet.Cells["C1"].StringValue == loginInfo)
                    {
                        idUser = int.Parse(idInfo);

                        break;
                    }

                }
                reader.Close();

            }

            if (dbCon.IsConnect())
            {
                //Get the user group and check to see if the user is on Trial
                string query = "SELECT * FROM imspulse.backend_users_groups;";
                var cmd = new MySqlCommand(query, dbCon.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int user_id = int.Parse(reader.GetString(0));
                    int user_group = int.Parse(reader.GetString(1));

                    //Check to see if User and Group are occupied with the details of the table

                    if ((user_id == idUser) && (user_group != 0))
                    {
                        //Put the User Group of the User in idGroup
                        idGroup = user_group;
                        break;
                    }
                    else
                    {
                        idGroup = 0;
                    }


                }
                reader.Close();


                if (idGroup == 8)
                {
                    //suppose col0 and col1 are defined as VARCHAR in the DB
                    string query2 = "select * from imspulse.backend_users_groups where user_group_id = " + idGroup + ";";
                    var cmd2 = new MySqlCommand(query2, dbCon.Connection);
                    var reader2 = cmd.ExecuteReader();
                    while (reader2.Read())
                    {

                        MessageBox.Show("This user is suspended", "Suspended");

                        break;

                    }
                    reader.Close();
                    Application.Exit();

                }

            }

            for (int i = 0; i < directory.Length; i++)
            {
                if (selectedNodes[0].HasChildren == false)
                {

                    if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                    {
                        duplicates++;

                        if (duplicates == 1)
                        {
                            Process.Start(directory[i]);
                            first = directory[i];
                        }

                        if (duplicates > 1)
                        {
                            DialogResult da = MessageBox.Show("You have a duplicate document of the exact same name , code and/or revision located at " + directory[i] + ". The first instance of this document, which will be treated as the original, is located at: " + first + ".  Do you want to delete or keep the duplicate? Please note that if you choose to keep it, IMS Pulse will continuously ask you to delete it, as the document would infridge on the integrity of the Quality Management System structure.", "Duplicate Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if (da == DialogResult.Yes)
                            {
                                FileSystem.DeleteFile(directory[i],
                      Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                      Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin,
                      Microsoft.VisualBasic.FileIO.UICancelOption.ThrowException);
                                MessageBox.Show("The Duplicate has been scheduled for removal. The next time you load the Quality Centre, the duplicate will be removed");
                                break;
                            }

                        }
                    }
                }

            }
        }
        public void treelistDeleteItem(TreeList treeList, int color)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;
            string primeFolder = @"Safety Management";

            string[] directory = Directory.GetFiles(ServerPath.PathLocation() + primeFolder + @"\", "*", System.IO.SearchOption.AllDirectories);

            for (int i = 0; i < directory.Length; i++)
            {
                if (selectedNodes[0].HasChildren == false)
                {

                    if (directory[i].Contains(selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString()))
                    {
                        FileSystem.DeleteFile(directory[i],
                        Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                        Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin,
                        Microsoft.VisualBasic.FileIO.UICancelOption.ThrowException);
                    }
                }

            }
            treeList.DeleteNode(treeList.FocusedNode);
            MessageBox.Show("Item Deleted");
            treeList.RefreshDataSource();
            treeList.StateImageList = imageCollection1;

            foreach (TreeListNode node in treeList.Nodes)
            {


                // first node
                if (node.HasChildren)
                {
                    node.StateImageIndex = color;

                }
                else
                {
                    node.StateImageIndex = color;
                }
                foreach (TreeListNode mumba in node.Nodes)
                {
                    if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                    {
                        mumba.StateImageIndex = 1;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                    {
                        mumba.StateImageIndex = 2;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                    {
                        mumba.StateImageIndex = 3;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                    {
                        mumba.StateImageIndex = 4;
                    }
                    else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                    {
                        mumba.StateImageIndex = 5;
                    }
                    else
                    {
                        mumba.StateImageIndex = 6;
                    }
                }

            }


        }

        public void treelistAddItem(string subfolder, TreeList treeList, int color)
        {
            TreeListMultiSelection selectedNodes = treeList.Selection;


            if (selectedNodes[0].ParentNode == null)
            {
                var fileContent = string.Empty;
                var filePath = string.Empty;


                xtraOpenFileDialog1.InitialDirectory = "c:\\";
                xtraOpenFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*|Word 2003 (*.doc)|*.doc|Word 2007 (*.docx)|*.docx|Excel 2003 (*.xls)|*.xls|Excel 2007 (*.xlsx)|*.xlsx|Powerpoint 2003 (*.ppt)|*.ppt|Powerpoint 2007 (*.pptx)|*.pptx";
                xtraOpenFileDialog1.FilterIndex = 2;
                xtraOpenFileDialog1.RestoreDirectory = true;

                if (xtraOpenFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = xtraOpenFileDialog1.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = xtraOpenFileDialog1.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                    }

                    File.Copy(filePath, ServerPath.PathLocation() + @"Safety Management\" + subfolder + @"\" + selectedNodes[0].GetDisplayText(treeList.Columns[2]).ToString() + @"\" + Path.GetFileName(filePath));
                }


                if (filePath != string.Empty)
                {

                    TreeListNode newNode = treeList.AppendNode(new object[] { "New first child" }, treeList.FocusedNode);
                    treeList.SetNodeIndex(newNode, 20001);
                    newNode.SetValue(2, Path.GetFileName(filePath));
                    treeList.OptionsBehavior.Editable = !treeList.OptionsBehavior.Editable;
                    treeList.RefreshDataSource();
                    MessageBox.Show("Document Added");
                    treeList.StateImageList = imageCollection1;
                }
                foreach (TreeListNode node in treeList.Nodes)
                {


                    // first node
                    if (node.HasChildren)
                    {
                        node.StateImageIndex = color;

                    }
                    else
                    {
                        node.StateImageIndex = color;
                    }
                    foreach (TreeListNode mumba in node.Nodes)
                    {
                        if (mumba.GetDisplayText(2).ToString().Contains(".doc"))
                        {
                            mumba.StateImageIndex = 1;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".xls"))
                        {
                            mumba.StateImageIndex = 2;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".ppt"))
                        {
                            mumba.StateImageIndex = 3;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".vsd"))
                        {
                            mumba.StateImageIndex = 4;
                        }
                        else if (mumba.GetDisplayText(2).ToString().Contains(".pdf"))
                        {
                            mumba.StateImageIndex = 5;
                        }
                        else
                        {
                            mumba.StateImageIndex = 6;
                        }
                    }

                }




            }
            else
            {
                MessageBox.Show("This is not a folder. Please select a folder and then a document to it.");
            }
        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
   
        }

        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }

        public void ValidateUser(ListView lstView)
        {
            DocumentMaster master = new DocumentMaster();
            string path = master.LoadSelected(@"Safety Management", lstView);

            if (isAdmin() == "admin")
            {
                if (master.LoadSelected(@"Safety Management", lstView).Contains("docx") || master.LoadSelected(@"Safety Management", lstView).Contains("doc"))
                {
                    Aspose.Words.LoadOptions getum1 = new Aspose.Words.LoadOptions { Password = "@Paradice1" };
                    Document doc = new Document(master.LoadSelected(@"Safety Management", lstView), getum1);
                    doc.Unprotect();

                    doc.Save(master.LoadSelected(@"Safety Management", lstView));
                }
                else if (master.LoadSelected(@"Safety Management", lstView).Contains(".xls") || master.LoadSelected(@"Safety Management", lstView).Contains(".xlsx"))
                {
                    Aspose.Cells.LoadOptions getum1 = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook workbook = new Workbook(path, getum1);
                    workbook.Unprotect("@Paradice1");
                    workbook.Settings.Password = null;
                    workbook.Settings.WriteProtection.Password = null;
                    workbook.Save(path);
                }
                List<string> fullpath = master.RunSelectedFile(@"Safety Management", lstView);
            }
            else
            {
                if (master.LoadSelected(@"Safety Management", lstView).Contains("docx") || master.LoadSelected(@"Safety Management", lstView).Contains("doc"))
                {
                    Aspose.Words.LoadOptions getum1 = new Aspose.Words.LoadOptions { Password = "@Paradice1" };
                    Document doc = new Document(master.LoadSelected(@"Safety Management", lstView), getum1);
                    doc.Protect(Aspose.Words.ProtectionType.ReadOnly, "@Paradice1");

                    doc.Save(master.LoadSelected(@"Safety Management", lstView));
                }
                else if (master.LoadSelected(@"Safety Management", lstView).Contains(".xls") || master.LoadSelected(@"Safety Management", lstView).Contains(".xlsx"))
                {
                    Aspose.Cells.LoadOptions getum1 = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
                    Workbook workbook = new Workbook(path, getum1);
                    workbook.Settings.WriteProtection.Author = "IMS Pulse";
                    workbook.Settings.WriteProtection.Password = "@Paradice1";

                    workbook.Save(path);
                }

                //else if (master.LoadSelected(@"Safety Management", lstView).Contains(".pptx") || master.LoadSelected(@"Safety Management", lstView).Contains(".ppt"))
                //{
                //    Aspose.Slides.LoadOptions getum1 = new Aspose.Slides.LoadOptions { Password = "@Paradice1" };
                //    Presentation work = new Presentation(path, getum1);

                //    work.ProtectionManager.SetWriteProtection("@Paradice1");

                   
                //}


                List<string> fullpath = master.RunSelectedFile(@"Safety Management", lstView);
            }
        }

        private void bbAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\SMS", treeList1, 7);
        }

        private void opAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\Operations", treeList3, 9);
        }

        private void huAddChild_ItemClick(object sender, EventArgs e)
        {
            treelistAddItem(@"\Human Resources", treeList2, 8);
        }

        private void bbDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList1, 7);

        }

        private void huDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList2, 8);

        }
        private void opDelete_ItemClick(object sender, EventArgs e)
        {

            treelistDeleteItem(treeList3, 9);

        }


        private void bbEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList1, 7);

        }

        private void huEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList2, 8);

        }
        private void opEncrypt_ItemClick(object sender, EventArgs e)
        {
            treelistEncrypt(treeList3, 9);

        }
        public void bbDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList1, 7);
        }
        public void huDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList2, 8);
        }
        public void opDecrypt_ItemClick(object sender, EventArgs e)
        {
            treelistDecrypt(treeList3, 9);
        }

        private void treeList1_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"\SMS", treeList1);
        }

        private void treeList2_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"\Human Resources", treeList2);
        }

        private void treeList3_DoubleClick(object sender, EventArgs e)
        {
            treelistDoubleClick(@"\Operations", treeList3);
        }

        private void treeList1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList1.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList1.Selection;



                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", bbAddChild_ItemClick));

                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", bbDelete_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", bbEncrypt_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", bbDecrypt_ItemClick));

                }
            }
        }

        private void treeList2_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList2.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList2.Selection;


                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", huAddChild_ItemClick));

                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", huDelete_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", huEncrypt_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", huDecrypt_ItemClick));

                }
            }
        }

        private void treeList3_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (isAdmin() == "admin")
            {
                if (e.Menu is TreeListNodeMenu)
                {
                    treeList3.FocusedNode = ((TreeListNodeMenu)e.Menu).Node;
                    TreeListMultiSelection selectedNodes = treeList3.Selection;


                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Add Document", opAddChild_ItemClick));

                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Delete Document", opDelete_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Encrypt Document", opEncrypt_ItemClick));
                    e.Menu.Items.Add(new DevExpress.Utils.Menu.DXMenuItem("Decrypt Document", opDecrypt_ItemClick));

                }
            }
        }
    }
}
