﻿namespace Management_System
{
    partial class SafetyCentre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SafetyCentre));
            this.mEMBERSHIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsDate = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem29 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
            this.tlsUserName = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.tlsSystemName = new System.Windows.Forms.ToolStripMenuItem();
            this.picBar = new System.Windows.Forms.PictureBox();
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonBottomToolBar1 = new C1.Win.C1Ribbon.RibbonBottomToolBar();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.ribHome = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonToolBar1 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonToolBar2 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonToolBar3 = new C1.Win.C1Ribbon.RibbonToolBar();
            this.ribbonButton2 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup4 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton4 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup5 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton5 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribEdit = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup8 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton8 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup9 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton9 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup10 = new C1.Win.C1Ribbon.RibbonGroup();
            this.rcmbThemes = new C1.Win.C1Ribbon.RibbonComboBox();
            this.ribSafety = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup12 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton11 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup11 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton10 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup13 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton12 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup14 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton13 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup3 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribRev = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup16 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton20 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup6 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton3 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribHelp = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup20 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton15 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonButton19 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup21 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton16 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup23 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton18 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonTopToolBar1 = new C1.Win.C1Ribbon.RibbonTopToolBar();
            this.c1ThemeController1 = new C1.Win.C1Themes.C1ThemeController();
            this.bunifuGradientPanel1 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.btnForm = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnProcess = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnPol = new Bunifu.Framework.UI.BunifuTileButton();
            this.btnProcedure = new Bunifu.Framework.UI.BunifuTileButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuGradientPanel3 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.processPro1 = new Management_System.SMSProcess();
            this.policiesPro1 = new Management_System.SMSPolicy();
            this.drpartmentsPro1 = new Management_System.SMSProcedure();
            this.departments1 = new Management_System.SMSForms();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).BeginInit();
            this.bunifuGradientPanel1.SuspendLayout();
            this.bunifuGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // mEMBERSHIPToolStripMenuItem
            // 
            this.mEMBERSHIPToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.mEMBERSHIPToolStripMenuItem.Name = "mEMBERSHIPToolStripMenuItem";
            this.mEMBERSHIPToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.mEMBERSHIPToolStripMenuItem.Text = "Membership:";
            // 
            // tlsDate
            // 
            this.tlsDate.Name = "tlsDate";
            this.tlsDate.Size = new System.Drawing.Size(55, 20);
            this.tlsDate.Text = "[DATE]";
            // 
            // toolStripMenuItem29
            // 
            this.toolStripMenuItem29.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripMenuItem29.Name = "toolStripMenuItem29";
            this.toolStripMenuItem29.Size = new System.Drawing.Size(100, 20);
            this.toolStripMenuItem29.Text = "[MEMBERSHIP]";
            // 
            // toolStripMenuItem28
            // 
            this.toolStripMenuItem28.Name = "toolStripMenuItem28";
            this.toolStripMenuItem28.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItem28.Text = "Date:";
            // 
            // toolStripMenuItem27
            // 
            this.toolStripMenuItem27.Name = "toolStripMenuItem27";
            this.toolStripMenuItem27.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem27.Text = "PC:";
            // 
            // tlsUserName
            // 
            this.tlsUserName.Name = "tlsUserName";
            this.tlsUserName.Size = new System.Drawing.Size(93, 20);
            this.tlsUserName.Text = "[USER_NAME]";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(45, 20);
            this.toolStripMenuItem5.Text = "User:";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem5,
            this.tlsUserName,
            this.toolStripMenuItem27,
            this.tlsSystemName,
            this.toolStripMenuItem28,
            this.toolStripMenuItem29,
            this.tlsDate,
            this.mEMBERSHIPToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 728);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1198, 24);
            this.menuStrip2.TabIndex = 15;
            this.menuStrip2.Text = "BottomStrip";
            // 
            // tlsSystemName
            // 
            this.tlsSystemName.Name = "tlsSystemName";
            this.tlsSystemName.Size = new System.Drawing.Size(103, 20);
            this.tlsSystemName.Text = "[SYTEM_NAME]";
            // 
            // picBar
            // 
            this.picBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picBar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.picBar.Location = new System.Drawing.Point(-15, 693);
            this.picBar.Name = "picBar";
            this.picBar.Size = new System.Drawing.Size(1234, 73);
            this.picBar.TabIndex = 12;
            this.picBar.TabStop = false;
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.AutoSizeElement = C1.Framework.AutoSizeElement.Width;
            this.c1Ribbon1.BottomToolBarHolder = this.ribbonBottomToolBar1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(1198, 146);
            this.c1Ribbon1.Tabs.Add(this.ribHome);
            this.c1Ribbon1.Tabs.Add(this.ribEdit);
            this.c1Ribbon1.Tabs.Add(this.ribSafety);
            this.c1Ribbon1.Tabs.Add(this.ribHelp);
            this.c1ThemeController1.SetTheme(this.c1Ribbon1, "Office2016White");
            this.c1Ribbon1.TopToolBarHolder = this.ribbonTopToolBar1;
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            // 
            // ribbonBottomToolBar1
            // 
            this.ribbonBottomToolBar1.Name = "ribbonBottomToolBar1";
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // ribHome
            // 
            this.ribHome.Groups.Add(this.ribbonGroup1);
            this.ribHome.Groups.Add(this.ribbonGroup2);
            this.ribHome.Groups.Add(this.ribbonGroup4);
            this.ribHome.Groups.Add(this.ribbonGroup5);
            this.ribHome.Name = "ribHome";
            this.ribHome.Text = "Home";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.ribbonButton1);
            this.ribbonGroup1.Items.Add(this.ribbonToolBar1);
            this.ribbonGroup1.Items.Add(this.ribbonToolBar2);
            this.ribbonGroup1.Name = "ribbonGroup1";
            this.ribbonGroup1.Text = "New";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.LargeImage")));
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "New Document";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // ribbonToolBar1
            // 
            this.ribbonToolBar1.Name = "ribbonToolBar1";
            // 
            // ribbonToolBar2
            // 
            this.ribbonToolBar2.Name = "ribbonToolBar2";
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.ribbonToolBar3);
            this.ribbonGroup2.Items.Add(this.ribbonButton2);
            this.ribbonGroup2.Name = "ribbonGroup2";
            this.ribbonGroup2.Text = "External";
            // 
            // ribbonToolBar3
            // 
            this.ribbonToolBar3.Name = "ribbonToolBar3";
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.LargeImage")));
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "Edit External Document";
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click);
            // 
            // ribbonGroup4
            // 
            this.ribbonGroup4.Items.Add(this.ribbonButton4);
            this.ribbonGroup4.Name = "ribbonGroup4";
            this.ribbonGroup4.Text = "Main Centre";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.LargeImage")));
            this.ribbonButton4.Name = "ribbonButton4";
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Text = "Go To Main Centre";
            this.ribbonButton4.Click += new System.EventHandler(this.ribbonButton4_Click_1);
            // 
            // ribbonGroup5
            // 
            this.ribbonGroup5.Items.Add(this.ribbonButton5);
            this.ribbonGroup5.Name = "ribbonGroup5";
            this.ribbonGroup5.Text = "Application";
            // 
            // ribbonButton5
            // 
            this.ribbonButton5.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.LargeImage")));
            this.ribbonButton5.Name = "ribbonButton5";
            this.ribbonButton5.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton5.SmallImage")));
            this.ribbonButton5.Text = "Exit Application";
            this.ribbonButton5.Click += new System.EventHandler(this.ribbonButton5_Click_1);
            // 
            // ribEdit
            // 
            this.ribEdit.Groups.Add(this.ribbonGroup8);
            this.ribEdit.Groups.Add(this.ribbonGroup9);
            this.ribEdit.Groups.Add(this.ribbonGroup10);
            this.ribEdit.Name = "ribEdit";
            this.ribEdit.Text = "Edit";
            // 
            // ribbonGroup8
            // 
            this.ribbonGroup8.Items.Add(this.ribbonButton8);
            this.ribbonGroup8.Name = "ribbonGroup8";
            this.ribbonGroup8.Text = "Document Location";
            // 
            // ribbonButton8
            // 
            this.ribbonButton8.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.LargeImage")));
            this.ribbonButton8.Name = "ribbonButton8";
            this.ribbonButton8.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton8.SmallImage")));
            this.ribbonButton8.Text = "Change Document Location";
            this.ribbonButton8.Click += new System.EventHandler(this.ribbonButton8_Click);
            // 
            // ribbonGroup9
            // 
            this.ribbonGroup9.Items.Add(this.ribbonButton9);
            this.ribbonGroup9.Name = "ribbonGroup9";
            this.ribbonGroup9.Text = "Document Code";
            // 
            // ribbonButton9
            // 
            this.ribbonButton9.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton9.LargeImage")));
            this.ribbonButton9.Name = "ribbonButton9";
            this.ribbonButton9.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton9.SmallImage")));
            this.ribbonButton9.Text = "Change Document Code";
            this.ribbonButton9.Click += new System.EventHandler(this.ribbonButton9_Click);
            // 
            // ribbonGroup10
            // 
            this.ribbonGroup10.Items.Add(this.rcmbThemes);
            this.ribbonGroup10.Name = "ribbonGroup10";
            this.ribbonGroup10.Text = "Themes [Preview]";
            // 
            // rcmbThemes
            // 
            this.rcmbThemes.Label = "Themes";
            this.rcmbThemes.Name = "rcmbThemes";
            this.rcmbThemes.ChangeCommitted += new System.EventHandler(this.rcmbThemes_ChangeCommitted);
            this.rcmbThemes.DropDown += new System.EventHandler(this.rcmbThemes_DropDown);
            // 
            // ribSafety
            // 
            this.ribSafety.Groups.Add(this.ribbonGroup12);
            this.ribSafety.Groups.Add(this.ribbonGroup11);
            this.ribSafety.Groups.Add(this.ribbonGroup13);
            this.ribSafety.Groups.Add(this.ribbonGroup14);
            this.ribSafety.Groups.Add(this.ribbonGroup3);
            this.ribSafety.Groups.Add(this.ribbonGroup16);
            this.ribSafety.Groups.Add(this.ribbonGroup6);
            this.ribSafety.Name = "ribSafety";
            this.ribSafety.Text = "Safety Management";
            // 
            // ribbonGroup12
            // 
            this.ribbonGroup12.Items.Add(this.ribbonButton11);
            this.ribbonGroup12.Name = "ribbonGroup12";
            this.ribbonGroup12.Text = " NCR and CAR";
            // 
            // ribbonButton11
            // 
            this.ribbonButton11.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton11.LargeImage")));
            this.ribbonButton11.Name = "ribbonButton11";
            this.ribbonButton11.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton11.SmallImage")));
            this.ribbonButton11.Text = "Create CAR/NCR";
            this.ribbonButton11.Click += new System.EventHandler(this.ribbonButton11_Click_1);
            // 
            // ribbonGroup11
            // 
            this.ribbonGroup11.Items.Add(this.ribbonButton10);
            this.ribbonGroup11.Name = "ribbonGroup11";
            this.ribbonGroup11.Text = "Advanced Corrective Action";
            // 
            // ribbonButton10
            // 
            this.ribbonButton10.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton10.LargeImage")));
            this.ribbonButton10.Name = "ribbonButton10";
            this.ribbonButton10.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton10.SmallImage")));
            this.ribbonButton10.Text = "Edit CAR/NCR";
            this.ribbonButton10.Click += new System.EventHandler(this.ribbonButton10_Click);
            // 
            // ribbonGroup13
            // 
            this.ribbonGroup13.Items.Add(this.ribbonButton12);
            this.ribbonGroup13.Name = "ribbonGroup13";
            this.ribbonGroup13.Text = "Risk";
            // 
            // ribbonButton12
            // 
            this.ribbonButton12.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton12.LargeImage")));
            this.ribbonButton12.Name = "ribbonButton12";
            this.ribbonButton12.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton12.SmallImage")));
            this.ribbonButton12.Text = "Create Risk Entry";
            this.ribbonButton12.Click += new System.EventHandler(this.ribbonButton12_Click_1);
            // 
            // ribbonGroup14
            // 
            this.ribbonGroup14.Items.Add(this.ribbonButton13);
            this.ribbonGroup14.Name = "ribbonGroup14";
            this.ribbonGroup14.Text = "Advanced Risk";
            // 
            // ribbonButton13
            // 
            this.ribbonButton13.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton13.LargeImage")));
            this.ribbonButton13.Name = "ribbonButton13";
            this.ribbonButton13.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton13.SmallImage")));
            this.ribbonButton13.Text = "Edit Risk Entry";
            this.ribbonButton13.Click += new System.EventHandler(this.ribbonButton13_Click);
            // 
            // ribbonGroup3
            // 
            this.ribbonGroup3.Items.Add(this.ribRev);
            this.ribbonGroup3.Name = "ribbonGroup3";
            this.ribbonGroup3.Text = "Revisions";
            // 
            // ribRev
            // 
            this.ribRev.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribRev.LargeImage")));
            this.ribRev.Name = "ribRev";
            this.ribRev.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribRev.SmallImage")));
            this.ribRev.Text = "Open Revision Master";
            this.ribRev.Click += new System.EventHandler(this.ribRev_Click);
            // 
            // ribbonGroup16
            // 
            this.ribbonGroup16.Items.Add(this.ribbonButton20);
            this.ribbonGroup16.Name = "ribbonGroup16";
            this.ribbonGroup16.Text = "Users [Admin Only]";
            // 
            // ribbonButton20
            // 
            this.ribbonButton20.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton20.LargeImage")));
            this.ribbonButton20.Name = "ribbonButton20";
            this.ribbonButton20.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton20.SmallImage")));
            this.ribbonButton20.Text = "View System Users";
            this.ribbonButton20.Click += new System.EventHandler(this.ribbonButton20_Click);
            // 
            // ribbonGroup6
            // 
            this.ribbonGroup6.Items.Add(this.ribbonButton3);
            this.ribbonGroup6.Name = "ribbonGroup6";
            this.ribbonGroup6.Text = "Safety";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.LargeImage")));
            this.ribbonButton3.Name = "ribbonButton3";
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "Open Safety Folder";
            this.ribbonButton3.Click += new System.EventHandler(this.ribbonButton3_Click_1);
            // 
            // ribHelp
            // 
            this.ribHelp.Groups.Add(this.ribbonGroup20);
            this.ribHelp.Groups.Add(this.ribbonGroup21);
            this.ribHelp.Groups.Add(this.ribbonGroup23);
            this.ribHelp.Name = "ribHelp";
            this.ribHelp.Text = "Help";
            // 
            // ribbonGroup20
            // 
            this.ribbonGroup20.Items.Add(this.ribbonButton15);
            this.ribbonGroup20.Items.Add(this.ribbonButton19);
            this.ribbonGroup20.Name = "ribbonGroup20";
            this.ribbonGroup20.Text = "Contact";
            // 
            // ribbonButton15
            // 
            this.ribbonButton15.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton15.LargeImage")));
            this.ribbonButton15.Name = "ribbonButton15";
            this.ribbonButton15.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton15.SmallImage")));
            this.ribbonButton15.Text = "Website";
            this.ribbonButton15.Click += new System.EventHandler(this.ribbonButton15_Click);
            // 
            // ribbonButton19
            // 
            this.ribbonButton19.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton19.LargeImage")));
            this.ribbonButton19.Name = "ribbonButton19";
            this.ribbonButton19.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton19.SmallImage")));
            this.ribbonButton19.Text = "Direct Email";
            this.ribbonButton19.Click += new System.EventHandler(this.ribbonButton19_Click);
            // 
            // ribbonGroup21
            // 
            this.ribbonGroup21.Items.Add(this.ribbonButton16);
            this.ribbonGroup21.Name = "ribbonGroup21";
            this.ribbonGroup21.Text = "Product";
            // 
            // ribbonButton16
            // 
            this.ribbonButton16.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton16.LargeImage")));
            this.ribbonButton16.Name = "ribbonButton16";
            this.ribbonButton16.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton16.SmallImage")));
            this.ribbonButton16.Text = "About Product";
            this.ribbonButton16.Click += new System.EventHandler(this.ribbonButton16_Click);
            // 
            // ribbonGroup23
            // 
            this.ribbonGroup23.Items.Add(this.ribbonButton18);
            this.ribbonGroup23.Name = "ribbonGroup23";
            this.ribbonGroup23.Text = "User Manual";
            // 
            // ribbonButton18
            // 
            this.ribbonButton18.LargeImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton18.LargeImage")));
            this.ribbonButton18.Name = "ribbonButton18";
            this.ribbonButton18.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton18.SmallImage")));
            this.ribbonButton18.Text = "Manual";
            // 
            // ribbonTopToolBar1
            // 
            this.ribbonTopToolBar1.Name = "ribbonTopToolBar1";
            // 
            // bunifuGradientPanel1
            // 
            this.bunifuGradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(246)))));
            this.bunifuGradientPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuGradientPanel1.Controls.Add(this.btnForm);
            this.bunifuGradientPanel1.Controls.Add(this.btnProcess);
            this.bunifuGradientPanel1.Controls.Add(this.btnPol);
            this.bunifuGradientPanel1.Controls.Add(this.btnProcedure);
            this.bunifuGradientPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.bunifuGradientPanel1.GradientBottomLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel1.GradientBottomRight = System.Drawing.Color.Transparent;
            this.bunifuGradientPanel1.GradientTopLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel1.GradientTopRight = System.Drawing.Color.WhiteSmoke;
            this.bunifuGradientPanel1.Location = new System.Drawing.Point(0, 144);
            this.bunifuGradientPanel1.Name = "bunifuGradientPanel1";
            this.bunifuGradientPanel1.Quality = 10;
            this.bunifuGradientPanel1.Size = new System.Drawing.Size(284, 543);
            this.bunifuGradientPanel1.TabIndex = 23;
            this.c1ThemeController1.SetTheme(this.bunifuGradientPanel1, "(default)");
            // 
            // btnForm
            // 
            this.btnForm.BackColor = System.Drawing.Color.SteelBlue;
            this.btnForm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnForm.color = System.Drawing.Color.SteelBlue;
            this.btnForm.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnForm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnForm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnForm.ForeColor = System.Drawing.Color.Transparent;
            this.btnForm.Image = ((System.Drawing.Image)(resources.GetObject("btnForm.Image")));
            this.btnForm.ImagePosition = 13;
            this.btnForm.ImageZoom = 30;
            this.btnForm.LabelPosition = 27;
            this.btnForm.LabelText = "FORMS";
            this.btnForm.Location = new System.Drawing.Point(72, 21);
            this.btnForm.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnForm.Name = "btnForm";
            this.btnForm.Size = new System.Drawing.Size(126, 96);
            this.btnForm.TabIndex = 0;
            this.btnForm.Click += new System.EventHandler(this.bunifuTileButton1_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.BackColor = System.Drawing.Color.SteelBlue;
            this.btnProcess.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnProcess.color = System.Drawing.Color.SteelBlue;
            this.btnProcess.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnProcess.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcess.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcess.ForeColor = System.Drawing.Color.Transparent;
            this.btnProcess.Image = ((System.Drawing.Image)(resources.GetObject("btnProcess.Image")));
            this.btnProcess.ImagePosition = 13;
            this.btnProcess.ImageZoom = 30;
            this.btnProcess.LabelPosition = 27;
            this.btnProcess.LabelText = "PROCESS FLOW";
            this.btnProcess.Location = new System.Drawing.Point(72, 404);
            this.btnProcess.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(126, 96);
            this.btnProcess.TabIndex = 3;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // btnPol
            // 
            this.btnPol.BackColor = System.Drawing.Color.SteelBlue;
            this.btnPol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnPol.color = System.Drawing.Color.SteelBlue;
            this.btnPol.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnPol.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPol.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPol.ForeColor = System.Drawing.Color.Transparent;
            this.btnPol.Image = ((System.Drawing.Image)(resources.GetObject("btnPol.Image")));
            this.btnPol.ImagePosition = 13;
            this.btnPol.ImageZoom = 30;
            this.btnPol.LabelPosition = 27;
            this.btnPol.LabelText = "POLICIES";
            this.btnPol.Location = new System.Drawing.Point(72, 276);
            this.btnPol.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPol.Name = "btnPol";
            this.btnPol.Size = new System.Drawing.Size(126, 96);
            this.btnPol.TabIndex = 2;
            this.btnPol.Click += new System.EventHandler(this.btnPol_Click);
            // 
            // btnProcedure
            // 
            this.btnProcedure.BackColor = System.Drawing.Color.SteelBlue;
            this.btnProcedure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.btnProcedure.color = System.Drawing.Color.SteelBlue;
            this.btnProcedure.colorActive = System.Drawing.Color.LightSteelBlue;
            this.btnProcedure.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcedure.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProcedure.ForeColor = System.Drawing.Color.Transparent;
            this.btnProcedure.Image = ((System.Drawing.Image)(resources.GetObject("btnProcedure.Image")));
            this.btnProcedure.ImagePosition = 13;
            this.btnProcedure.ImageZoom = 30;
            this.btnProcedure.LabelPosition = 27;
            this.btnProcedure.LabelText = "PROCEDURES";
            this.btnProcedure.Location = new System.Drawing.Point(72, 147);
            this.btnProcedure.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnProcedure.Name = "btnProcedure";
            this.btnProcedure.Size = new System.Drawing.Size(126, 96);
            this.btnProcedure.TabIndex = 1;
            this.btnProcedure.Click += new System.EventHandler(this.bunifuTileButton2_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(209, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(320, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "1. Choose one of the folders.";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(209, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(535, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "2. If applicable, choose the relevant departments.";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(209, 371);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(588, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "3. Pick a document to either preview or open in Office.";
            // 
            // bunifuGradientPanel3
            // 
            this.bunifuGradientPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bunifuGradientPanel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel3.BackgroundImage")));
            this.bunifuGradientPanel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bunifuGradientPanel3.Controls.Add(this.label3);
            this.bunifuGradientPanel3.Controls.Add(this.label2);
            this.bunifuGradientPanel3.Controls.Add(this.label1);
            this.bunifuGradientPanel3.Controls.Add(this.pictureBox1);
            this.bunifuGradientPanel3.Controls.Add(this.processPro1);
            this.bunifuGradientPanel3.Controls.Add(this.policiesPro1);
            this.bunifuGradientPanel3.Controls.Add(this.drpartmentsPro1);
            this.bunifuGradientPanel3.Controls.Add(this.departments1);
            this.bunifuGradientPanel3.GradientBottomLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel3.GradientTopLeft = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.GradientTopRight = System.Drawing.Color.Gainsboro;
            this.bunifuGradientPanel3.Location = new System.Drawing.Point(290, 144);
            this.bunifuGradientPanel3.Name = "bunifuGradientPanel3";
            this.bunifuGradientPanel3.Quality = 10;
            this.bunifuGradientPanel3.Size = new System.Drawing.Size(896, 543);
            this.bunifuGradientPanel3.TabIndex = 25;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(892, 543);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // processPro1
            // 
            this.processPro1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.processPro1.BackColor = System.Drawing.Color.Transparent;
            this.processPro1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("processPro1.BackgroundImage")));
            this.processPro1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.processPro1.Location = new System.Drawing.Point(3, 3);
            this.processPro1.Name = "processPro1";
            this.processPro1.Size = new System.Drawing.Size(1005, 535);
            this.processPro1.TabIndex = 7;
            // 
            // policiesPro1
            // 
            this.policiesPro1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.policiesPro1.BackColor = System.Drawing.Color.Transparent;
            this.policiesPro1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("policiesPro1.BackgroundImage")));
            this.policiesPro1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.policiesPro1.Location = new System.Drawing.Point(3, 3);
            this.policiesPro1.Name = "policiesPro1";
            this.policiesPro1.Size = new System.Drawing.Size(888, 539);
            this.policiesPro1.TabIndex = 6;
            // 
            // drpartmentsPro1
            // 
            this.drpartmentsPro1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.drpartmentsPro1.BackColor = System.Drawing.Color.Transparent;
            this.drpartmentsPro1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("drpartmentsPro1.BackgroundImage")));
            this.drpartmentsPro1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.drpartmentsPro1.Location = new System.Drawing.Point(3, 3);
            this.drpartmentsPro1.Name = "drpartmentsPro1";
            this.drpartmentsPro1.Size = new System.Drawing.Size(888, 535);
            this.drpartmentsPro1.TabIndex = 5;
            // 
            // departments1
            // 
            this.departments1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.departments1.BackColor = System.Drawing.Color.Transparent;
            this.departments1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("departments1.BackgroundImage")));
            this.departments1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.departments1.Location = new System.Drawing.Point(3, 3);
            this.departments1.Name = "departments1";
            this.departments1.Size = new System.Drawing.Size(888, 535);
            this.departments1.TabIndex = 3;
            // 
            // SafetyCentre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 752);
            this.Controls.Add(this.bunifuGradientPanel3);
            this.Controls.Add(this.bunifuGradientPanel1);
            this.Controls.Add(this.c1Ribbon1);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.picBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SafetyCentre";
            this.Text = "Safety Centre";
            this.c1ThemeController1.SetTheme(this, "Office2016White");
            this.VisualStyleHolder = C1.Win.C1Ribbon.VisualStyle.Custom;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SafetyCentre_FormClosed);
            this.Load += new System.EventHandler(this.SafetyCentre_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1ThemeController1)).EndInit();
            this.bunifuGradientPanel1.ResumeLayout(false);
            this.bunifuGradientPanel3.ResumeLayout(false);
            this.bunifuGradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem mEMBERSHIPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tlsDate;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem29;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
        private System.Windows.Forms.ToolStripMenuItem tlsUserName;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem tlsSystemName;
        private System.Windows.Forms.PictureBox picBar;
      
        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonBottomToolBar ribbonBottomToolBar1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab ribHome;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
        private C1.Win.C1Ribbon.RibbonTopToolBar ribbonTopToolBar1;
        private C1.Win.C1Themes.C1ThemeController c1ThemeController1;
        private C1.Win.C1Ribbon.RibbonTab ribEdit;
        private C1.Win.C1Ribbon.RibbonTab ribSafety;
        private C1.Win.C1Ribbon.RibbonTab ribHelp;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar1;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Ribbon.RibbonToolBar ribbonToolBar3;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup4;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton4;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup5;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton5;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup8;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup9;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup10;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton8;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton9;
        private C1.Win.C1Ribbon.RibbonComboBox rcmbThemes;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup11;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton10;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup20;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup21;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup23;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton15;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton19;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton16;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton18;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup16;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton20;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup12;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton11;
       
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup13;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton12;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup14;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton13;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel1;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel3;
        private Bunifu.Framework.UI.BunifuTileButton btnProcess;
        private Bunifu.Framework.UI.BunifuTileButton btnPol;
        private Bunifu.Framework.UI.BunifuTileButton btnProcedure;
        private Bunifu.Framework.UI.BunifuTileButton btnForm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private SMSForms departments1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private SMSProcedure drpartmentsPro1;
        private SMSPolicy policiesPro1;
        private SMSProcess processPro1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup3;
        private C1.Win.C1Ribbon.RibbonButton ribRev;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup6;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton3;
    }
}