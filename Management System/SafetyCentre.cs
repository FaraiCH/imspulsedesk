﻿using DocumentManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using C1.Win.C1Themes;

using System.Diagnostics;
using System.Data.OleDb;
using System.IO;
using Aspose.Cells;
using System.Net;

namespace Management_System
{
    public partial class SafetyCentre : C1.Win.C1Ribbon.C1RibbonForm
    {
        NetworkCredential networkCredential = new NetworkCredential("Cinfratec", "admincinfratec");

        MainForm main;

        public void RefreshDocument()
        {
            //lstItem.Hide();
            //lstItem.Show();
        }

        void ToggleSelectionFontStyle(FontStyle fontStyle)
        {
            //if (this.richTextBox1.SelectionFont == null)
            //{
            //    MessageBox.Show("Cannot change font style while selected text has more than one font.");
            //}
            //else
            //{
            //    this.richTextBox1.SelectionFont = new Font(this.richTextBox1.SelectionFont,
            //    this.richTextBox1.SelectionFont.Style ^ fontStyle);
            //}
        }

        private void UpdateFontGroupBasedOnCurrentTextSelection()
        {
            //Font font = this.richTextBox1.SelectionFont;
            //bool none = font == null;
            //ribbonToggleButton1.Pressed = none ? false : font.Bold;
            //ribbonToggleButton2.Pressed = none ? false : font.Italic;
            //ribbonToggleButton3.Pressed = none ? false : font.Underline;
        }

        public SafetyCentre()
        {
            string LData = "PExpY2Vuc2U+CjxEYXRhPgo8TGljZW5zZWRUbz5BdmVQb2ludDwvTGljZW5zZWRUbz4KPEVtYWlsVG8+aXRfYmlsbGluZ0BhdmVwb2ludC5jb208L0VtYWlsVG8+CjxMaWNlbnNlVHlwZT5EZXZlbG9wZXIgT0VNPC9MaWNlbnNlVHlwZT4KPExpY2Vuc2VOb3RlPkxpbWl0ZWQgdG8gMSBkZXZlbG9wZXIsIHVubGltaXRlZCBwaHlzaWNhbCBsb2NhdGlvbnM8L0xpY2Vuc2VOb3RlPgo8T3JkZXJJRD4xOTA1MjAwNzE1NDY8L09yZGVySUQ+CjxVc2VySUQ+MTU0ODI2PC9Vc2VySUQ+CjxPRU0+VGhpcyBpcyBhIHJlZGlzdHJpYnV0YWJsZSBsaWNlbnNlPC9PRU0+CjxQcm9kdWN0cz4KPFByb2R1Y3Q+QXNwb3NlLlRvdGFsIGZvciAuTkVUPC9Qcm9kdWN0Pgo8L1Byb2R1Y3RzPgo8RWRpdGlvblR5cGU+RW50ZXJwcmlzZTwvRWRpdGlvblR5cGU+CjxTZXJpYWxOdW1iZXI+Y2JmMzVkNWYtOWE2Ni00ZTI4LTg1ZGQtM2ExN2JiZTM0MTNhPC9TZXJpYWxOdW1iZXI+CjxTdWJzY3JpcHRpb25FeHBpcnk+MjAyMDA2MDQ8L1N1YnNjcmlwdGlvbkV4cGlyeT4KPExpY2Vuc2VWZXJzaW9uPjMuMDwvTGljZW5zZVZlcnNpb24+CjxMaWNlbnNlSW5zdHJ1Y3Rpb25zPmh0dHBzOi8vcHVyY2hhc2UuYXNwb3NlLmNvbS9wb2xpY2llcy91c2UtbGljZW5zZTwvTGljZW5zZUluc3RydWN0aW9ucz4KPC9EYXRhPgo8U2lnbmF0dXJlPnpqZDMrdWgzNTdiZHhqR3JWTTZCN3I2c250TkRBTlRXU2MyQi9RWS9hdmZxTnA0VHk5Z0kxR2V1NUdOaWVwRHArY1JrRFBMdjBDRTZ2MHNjYVZwK1JNTkF5SzdiUzdzeGZSL205Z0NtekFNUlptdUxQTm1laEtZVTNvOGJWVDJvWmRJeEY2dVRTMDhIclJxUnk5SWt6c3BxYmRrcEZFY0lGcHlLbDF2NlF2UT08L1NpZ25hdHVyZT4KPC9MaWNlbnNlPg==";

            Stream stream = new MemoryStream(Convert.FromBase64String(LData));

            stream.Seek(0, SeekOrigin.Begin);
            new Aspose.Cells.License().SetLicense(stream);
            main = new MainForm();
            InitializeComponent();
            // Keep ribbon controls state updated based on current text selection.
            this.UpdateFontGroupBasedOnCurrentTextSelection();
            //this.richTextBox1.SelectionChanged += delegate
            //{
            //    this.UpdateFontGroupBasedOnCurrentTextSelection();
            //};
        }

        //public void LoadRichTextBoxVisible(RichTextBox richTextBox)
        //{
        //    richTextBox1.Copy();
        //    richTextBox.Paste();
        //    richTextBox.WordWrap = true;
        //}

        public void loadNew()
        {
            //string filename = @"C:\Quality Management\NewDocuments.accdb";
            //OleDbCommand cmd = new OleDbCommand();
            //OleDbConnection con = new OleDbConnection(string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};", filename));
            //string sql = "Select DocumentPath FROM NewDocS";
            //con.Open();
            //cmd = new OleDbCommand(sql, con);
            //OleDbDataAdapter adaptor = new OleDbDataAdapter(cmd);
            //DataTable dataTable = new DataTable();
            //adaptor.Fill(dataTable);

            //for (int i = 0; i < dataTable.Rows.Count; i++)
            //{
            //    lstItem.Items.Add(Path.GetFileName(dataTable.Rows[i]["DocumentPath"].ToString()));
            //}

            //con.Close();
        }

        public void SchedularMaid()
        {
            // For Interval in Seconds 
            // This Scheduler will start at 15:10 and call after every 15 Seconds
            // IntervalInSeconds(start_hour, start_minute, seconds)
            MyScheduler.IntervalInSeconds(15, 9, 10,
            () => {
                MessageBox.Show("Work, baby, work");
            });
            // For Interval in Minutes 
            // This Scheduler will start at 22:00 and call after every 30 Minutes
            // IntervalInSeconds(start_hour, start_minute, minutes)
            //MyScheduler.IntervalInMinutes(22, 00, 30,
            //() => {
            //    Console.WriteLine("//here write the code that you want to schedule");
            //});
            //// For Interval in Hours 
            //// This Scheduler will start at 9:44 and call after every 1 Hour
            //// IntervalInSeconds(start_hour, start_minute, hours)
            //MyScheduler.IntervalInHours(9, 44, 1,
            //() => {
            //    Console.WriteLine("//here write the code that you want to schedule");
            //});
            //// For Interval in Seconds 
            //// This Scheduler will start at 17:22 and call after every 3 Days
            //// IntervalInSeconds(start_hour, start_minute, days)
            //MyScheduler.IntervalInDays(17, 22, 3,
            //() => {
            //    Console.WriteLine("//here write the code that you want to schedule");
            //});
            //Console.ReadLine();
        }

        public void Themes()
        {
            rcmbThemes.Items.Clear();
            string[] themes = C1ThemeController.GetThemes();
            foreach (string theme in themes)
            rcmbThemes.Items.Add(theme);

           
        }
        public void DueDates()
        {
            try
            {
                string path = ServerPath.PathLocation() + @"Shared\CAR LOG.xlsx";
                string Late = "";
                int todayIs = DateTime.Today.Day;
                Workbook work = new Workbook(path);
                Worksheet sheet = work.Worksheets[0];

                if (sheet.Cells[3, 0].Type != CellValueType.IsNull)
                {
                    for (int i = 3; i < sheet.Cells.Rows.Count; i++)
                    {

                        if (sheet.Cells[i, 1].Type != CellValueType.IsNull)
                        {
                            int dueDate = sheet.Cells[i, 9].DateTimeValue.Day;

                            if (todayIs < dueDate)
                            {
                                int answer = todayIs - dueDate;

                                if (answer < 5)
                                {
                                    //Late += string.Format("{ 0} \t {1}", dataTable.Rows[i]["F6"].ToString(), dataTable.Rows[i]["F15"].ToString()) + Environment.NewLine;
                                    Late = "Some CAR Entries are approaching their due dates. Please open the CAR/NCR for more details.";

                                }
                                MessageBox.Show(Late, "Due dates are approaching", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }


                        }
                        break;
                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        public void ReadProces()
        {
            string isChecked = File.ReadAllText(@"C:\Management\Management System\ID_46664\Mula.txt");

            if (isChecked.Contains("Checked"))
            {
                DueDates();
            }
        }

        public void Log()
        {
            string filenameUser = ServerPath.PathLocation() + @"Management\Management System\ID_98889\UserLog.txt";

            using (StreamWriter w = File.AppendText(filenameUser))
            {
                w.WriteLine("Date:  " + DateTime.Now.ToShortDateString() + "\t\t  " + "User Name:  " + Environment.UserName.ToString() + "\t\t  " + "Management Centre: " + this.Text + "\t\t\t  " + "PC:  " + Environment.MachineName.ToString() + Environment.NewLine);
            }
        }

        public void RichImage()
        {
            Image image = Image.FromFile(@"C:\Safety Management\IMS Global\Safety-100.jpg");
            Clipboard.SetImage(image);
            //richTextBox1.Paste();
            //richTextBox1.SelectAll();
            //richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
            //richTextBox1.DeselectAll();
            
            Clipboard.Clear();
        }


        public void ValidateUser()
        {
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);

            Worksheet sheet = work.Worksheets[0];


            if (sheet.Cells["B1"].Value != null)
            {
                string password = sheet.Cells["B1"].Value.ToString();

                if (password == passwordBase[0] || password == passwordBase[1] || password == passwordBase[2] ||
                    password == passwordBase[3] || password == passwordBase[4])
                {
                    label1.Text = "Admin";
                    MessageBox.Show("You have been logged in as the Admin. You have full priveledges. Enjoy!", "Logged in as 'Admin'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                label1.Text = "Admin";
                MessageBox.Show("You have been logged in as the Admin. You have full priveledges. Enjoy!", "Logged in as 'Admin'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);


            }
            else
            {
                label1.Text = "User";
                MessageBox.Show("You have been logged into the system as a User. You do not have administrative access to edit any documents. " +
                    "You do not have permission to make CAR/NCR entries, make Risk entries. You may only read. To have admin rights, exit the application and log in with the correct password.", "Logged in as 'User'", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
        }

        private void SafetyCentre_Load(object sender, EventArgs e)
        {
            ribRev.Text = "Open Revision Master" + "\u2122";
            processPro1.Hide();
            drpartmentsPro1.Hide();
            departments1.Hide();
            policiesPro1.Hide();
            //RichImage();
  
           
            //groupBox2.Visible = false;
            //SchedularMaid();
            tlsDate.Text = DateTime.Now.ToShortDateString();
            tlsUserName.Text = Environment.UserName.ToString();
            tlsSystemName.Text = Environment.MachineName.ToString();
            Themes();

            //DocumentExtensions extensions = new DocumentExtensions();
            //extensions.GetWorkSheetName(@"C:\Users\Farai\source\repos\Management System\Management System\Safety Management\IMS Global\4.1 General Requirements, Context of the Organisation\IMS-PO-REG-001 Context of Organisation.xlsx");

            //lstItem.Items.Add("============= Word Documentation ============");
            //lstItem.Items.Add("");
            //LoadDocs();
            //lstItem.Items.Add("");
            //lstItem.Items.Add("============= Excel Documentation ============");
            //lstItem.Items.Add("");
            //LoadExcel();
            //lstItem.Items.Add("");
            //lstItem.Items.Add("=============== NEW Documentation ================");
            //lstItem.Items.Add("");

            //using (new NetworkConnection(@"\\SERVER-PC\ISO 9001", networkCredential))
            //{
            //    string[] theFolders = Directory.GetDirectories(@"\\SERVER-PC\ISO 9001");
            //    string hold = "";

            //    for (int i = 0; i < theFolders.Length; i++)
            //    {
            //        hold += theFolders[i] + "  \n";
            //    }
            //    if (hold != null)
            //    {
               
                        loadNew();
                        ReadProces();
                        Log();                   
                

            //    }
            //}
        
        }



        private void SafetyCentre_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void lstItem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        private void txtSearch_OnTextChange(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tlsMainCentre_Click(object sender, EventArgs e)
        {
            MainForm mainForm = new MainForm();
            this.Hide();
            mainForm.Show();
        }

        private void ribbonTab1_Select(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            main.Show();
            this.Hide();

        }

        private void rcmbTheme_SelectedItemChanged(object sender, EventArgs e)
        {
            

       
        }

        private void rcmbThemes_DropDown(object sender, EventArgs e)
        {
            Themes();
        }

        private void rcmbThemes_ChangeCommitted(object sender, EventArgs e)
        {
           
            C1Theme theme = C1ThemeController.GetThemeByName(rcmbThemes.Text, false);
            if (theme != null)
            C1ThemeController.ApplyThemeToControlTree(this, theme);
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            NewDocument newDocument = new NewDocument();
            newDocument.ShowDialog();
          
        }

        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            EditDocument editDocument = new EditDocument();

            editDocument.ShowDialog();
            
        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Cannot Do That", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         
        }

        private void ribbonButton4_Click(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            main.Show();
            this.Hide();
        }

        private void ribbonButton5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ribbonButton4_Click_1(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            main.Show();
            this.Hide();
        }

        private void ribbonButton5_Click_1(object sender, EventArgs e)
        {
            main.MovePanelToFront();
            main.Show();
            this.Hide();          
        }

        private void ribbonButton10_Click(object sender, EventArgs e)
        {
            EditCorrective();
        }

        private void ribbonButton11_Click(object sender, EventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();
            nCRandCAR.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            AdvancedCorrective advancedCAR = new AdvancedCorrective();
            advancedCAR.ShowDialog();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void ribbonButton11_Click_1(object sender, EventArgs e)
        {
            NCRandCAR nCRandCAR = new NCRandCAR();
            nCRandCAR.ShowDialog();
        }


        public string isAdmin()
        {
            string status = "";
            string[] passwordBase = { "@Paradice1", "@Merlin!4u", "$MeganMeckal.JR", "$ErenYeagerFounder", "DjEddie@3000" };

            //Proper way to open a protected worksheet with password
            Aspose.Cells.LoadOptions getum = new Aspose.Cells.LoadOptions { Password = "@Paradice1" };
            Workbook work = new Workbook(@"C:\Reader\Loader.xlsx", getum);
            Worksheet sheet = work.Worksheets[0];

            if (sheet.Cells["B1"].Value != null)
            {
                status = "admin";
            }
            else if (sheet.Cells["E1"].Value != null)
            {

                status = "admin";



            }
            else
            {
                status = "user";
            }
            work.Settings.Password = "@Paradice1";
            work.Save(@"C:\Reader\Loader.xlsx");
            return status;
        }

        public void ValidateUsers()
        {

            if (isAdmin() == "admin")
            {
                UserLog userLog = new UserLog();
                userLog.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void EditRisk()
        {
            if (isAdmin() == "admin")
            {
                Advanced_Risk_Register register = new Advanced_Risk_Register();
                register.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void EditCorrective()
        {
            if (isAdmin() == "admin")
            {
                AdvancedCorrective advancedCorrective = new AdvancedCorrective();
                advancedCorrective.ShowDialog();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use the NCR/CAR feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ribbonButton20_Click(object sender, EventArgs e)
        {
            ValidateUsers();
        }

        private void ribbonButton6_Click(object sender, EventArgs e)
        {
            //if (richTextBox1.CanUndo)
            //{
            //    richTextBox1.Undo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to undo.", "Cannot Undo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void ribbonButton7_Click(object sender, EventArgs e)
        {
            //if (richTextBox1.CanRedo)
            //{
            //    richTextBox1.Redo();
            //}
            //else
            //{
            //    MessageBox.Show("There is nothing to redo.", "Cannot Redo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }

        private void ribbonButton8_Click(object sender, EventArgs e)
        {
            LocationChanger locationChanger = new LocationChanger();
            locationChanger.ShowDialog();
        }

        private void ribbonButton9_Click(object sender, EventArgs e)
        {
            DocumentCode documentCode = new DocumentCode();
            documentCode.ShowDialog();
        }

        private void ribbonButton16_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void ribbonButton15_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.imsglobal.co.za");
        }

        private void ribbonButton17_Click(object sender, EventArgs e)
        {

        }

        private void ribbonButton12_Click(object sender, EventArgs e)
        {
            Charts charts = new Charts();
            charts.ShowDialog();
        }

        private void ribbonToggleButton1_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Bold);
        }

        private void ribbonToggleButton2_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Italic);
        }

        private void ribbonToggleButton3_Click(object sender, EventArgs e)
        {
            ToggleSelectionFontStyle(FontStyle.Underline);
        }

        private void ribbonButton19_Click(object sender, EventArgs e)
        {
            Email email = new Email();
            email.ShowDialog();
        }

        private void ribbonButton12_Click_1(object sender, EventArgs e)
        {
            RiskRegister riskRegister = new RiskRegister();
            riskRegister.ShowDialog();
        }

        private void ribbonButton13_Click(object sender, EventArgs e)
        {
            EditRisk();
        }

        private void lstItem_DoubleClick(object sender, EventArgs e)
        {


        }

        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            if (departments1.Visible == true)
            {
                departments1.Hide();
                btnForm.color = Color.SteelBlue;
                departments1.ResetAll();
                pictureBox1.Show();
                label1.Show();
                label2.Show();
                label3.Show();

            }
            else
            {
                btnForm.color = Color.LightSteelBlue;
                departments1.Show();
                drpartmentsPro1.Hide();
                btnProcedure.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                policiesPro1.Hide();
                processPro1.Hide();
                pictureBox1.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            if (drpartmentsPro1.Visible == true)
            {
                drpartmentsPro1.Hide();
                btnProcedure.color = Color.SteelBlue;
                drpartmentsPro1.ResetAll();
                pictureBox1.Show();
                label1.Show();
                label2.Show();
                label3.Show();

            }
            else
            {
                btnProcedure.color = Color.LightSteelBlue;
                btnForm.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                drpartmentsPro1.Show();
                departments1.Hide();
                policiesPro1.Hide();
                processPro1.Hide();
                pictureBox1.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void btnPol_Click(object sender, EventArgs e)
        {
            if(policiesPro1.Visible == true)
            {
                policiesPro1.Hide();

                btnPol.color = Color.SteelBlue;
                policiesPro1.ResetAll();
                pictureBox1.Show();
                label1.Show();
                label2.Show();
                label3.Show();

            }
            else
            {
                btnProcedure.color = Color.SteelBlue;
                btnForm.color = Color.SteelBlue;
                btnProcess.color = Color.SteelBlue;
                btnPol.color = Color.LightSteelBlue;
                policiesPro1.Show();
                drpartmentsPro1.Hide();
                departments1.Hide();
                processPro1.Hide();
                pictureBox1.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (processPro1.Visible == true)
            {
                processPro1.Hide();

                btnProcess.color = Color.SteelBlue;
                processPro1.ResetAll();
                pictureBox1.Show();
                label1.Show();
                label2.Show();
                label3.Show();
            }
            else
            {
                processPro1.Show();
                btnProcedure.color = Color.SteelBlue;
                btnForm.color = Color.SteelBlue;
                btnPol.color = Color.SteelBlue;
                btnProcess.color = Color.LightSteelBlue;
                policiesPro1.Hide();
                drpartmentsPro1.Hide();
                departments1.Hide();
                pictureBox1.Hide();
                label1.Hide();
                label2.Hide();
                label3.Hide();

            }


        }
       
        private void ribRev_Click(object sender, EventArgs e)
        {
            if (isAdmin() == "admin")
            {
                RevisionSafety revisionSafety = new RevisionSafety();
                revisionSafety.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
          
        }

        private void ribbonButton3_Click_1(object sender, EventArgs e)
        {
            if (isAdmin() == "admin")
            {
                Process.Start(ServerPath.PathLocation() + @"Safety Management\IMS Global");
            }
            else
            {
                MessageBox.Show("You do not have administrative permission to use this feature.", "No Permission as User", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
    }
}
